#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <omp.h>
#include "sdGeneric.h"
#include "stShapeSpaceKalman.h"
#include "ppBgExtractionAndFilter.h"
#include "wdIgorMeanAngle.h"



int main(int argc, char *argv[])
{

	//-----------------------Main Parameter Setting--------------------
	// Segment of frame for processing
	int startFrame = 8000, endFrame = 10000;

	// Parameters
	int snoutCenterX = 389;
	int snoutCenterY = 145;
	int snoutTipX = 417;
	int snoutTipY = 328;
	char address[256];

	float bwThreshold = 0.2;
	int razorSize = 8;

	int distanceFromSnout = 20;
	int bandWidth = 8;
	int cMass = 14;
	float hisgamma = 2.5;

	int interestedHeight = 0;


	FILE * fp;
	fp = fopen("tmp/parameters", "r");

	fscanf(fp, "Video Address:%s\n", address);
	fscanf(fp, "Starting Frame Index:%d\n", &startFrame);
	fscanf(fp, "End Frame Index:%d\n", &endFrame);
	fscanf(fp, "Snout Center X:%d\n", &snoutCenterX);
	fscanf(fp, "Snout Center Y:%d\n", &snoutCenterY);
	fscanf(fp, "Snout Tip X:%d\n", &snoutTipX);
	fscanf(fp, "Snout Tip Y:%d\n", &snoutTipY);
	fscanf(fp, "Razor Size:%d\n", &razorSize);
	fscanf(fp, "Black/White Threshold:%f\n", &bwThreshold);
	fscanf(fp, "cMass T:%d\n", &cMass);
	fscanf(fp, "Histogram Gamma:%f\n", &hisgamma);
	fscanf(fp, "Minimum Distance:%d\n", &distanceFromSnout);
	fscanf(fp, "Bandwidth:%d\n", &bandWidth);
	fscanf(fp, "Interested Height:%d", &interestedHeight);

	fclose(fp);

	/*
	printf("Please enter the index of starting frame: \n");
	scanf("%d", &startFrame);
	printf("Please enter the index of end frame: \n");
	scanf("%d", &endFrame);
	printf("Please enter the X of snout center: \n");
	scanf("%d", &snoutCenterX);
	printf("Please enter the Y of snout center: \n");
	scanf("%d", &snoutCenterY);
	printf("Please enter the X of snout tip: \n");
	scanf("%d", &snoutTipX);
	printf("Please enter the Y of snout tip \n");
	scanf("%d", &snoutTipY);
	*/


	// Read Video File
	CvCapture* capture[2];

	for (int i = 0; i < 2; i++) {
		capture[i] = cvCaptureFromFile(address);
	}

	//--------------------------The End ---------------------------------


	uchar *frameData;
	IplImage *frame = 0;
	int boundaryX[10000];
	int boundaryY[10000];


	int frameNumbers = cvGetCaptureProperty(capture[1], CV_CAP_PROP_FRAME_COUNT);

	if (endFrame > frameNumbers) {
		endFrame = frameNumbers;
	}

	int nRows = cvGetCaptureProperty(capture[1], CV_CAP_PROP_FRAME_HEIGHT);
	int nCols = cvGetCaptureProperty(capture[1], CV_CAP_PROP_FRAME_WIDTH);
	//int step = 2;
	int step = (frameNumbers - startFrame) / 1000;
	int pixs = nRows*nCols;

	IplImage *greyframe = cvCreateImage(
		cvSize(nCols, nRows), IPL_DEPTH_8U, 1);


	FILE *fileint;
	

	// Removed for fast GUI!
	/*

	fileint = fopen("tmp/ppBg", "w");
	uchar *ppBg;

	ppBg = (uchar*)calloc(nRows*nCols, sizeof(uchar));
	memset(ppBg, 0, nRows*nCols);

	// Initialize background image
	printf("Extracting Background Image...\n");
	for (int index = startFrame; index < frameNumbers; index += step) {
		cvSetCaptureProperty(capture[1], CV_CAP_PROP_POS_FRAMES, index);
		frame = cvQueryFrame(capture[1]);
		cvCvtColor(frame, greyframe, CV_BGR2GRAY);
		frameData = (uchar *)greyframe->imageData;
		for (int i = 0; i < pixs; i++) {
			if (frameData[i] > ppBg[i]) {
				ppBg[i] = frameData[i];
			}
		}
	}

	for (int i = 0; i < pixs; i++) {
		fprintf(fileint, "%d\n", ppBg[i]);
	}

	fclose(fileint);

	*/

	cvSetCaptureProperty(capture[1], CV_CAP_PROP_POS_FRAMES, startFrame);
	frame = cvQueryFrame(capture[1]);

	IplImage *demopic1 = cvCreateImage(
		cvSize(nCols, nRows), IPL_DEPTH_8U, 3);
	demopic1 = cvCloneImage(frame);

	cvCvtColor(frame, greyframe, CV_BGR2GRAY);

	// Calculate Generic Boundaries
	int boundaryCount = sdGeneric(greyframe, boundaryX, boundaryY);

	// Create Snout Ring
	IplImage *RingMask = cvCreateImage(
		cvSize(nCols, nRows), IPL_DEPTH_8U, 1);

	IplImage *distanceMask = cvCreateImage(
		cvSize(nCols, nRows), IPL_DEPTH_32F, 1);
	cvSetZero(RingMask);
	

	initializeFromParametersSnoutTemplate(snoutCenterX, snoutCenterY,
		snoutTipX, snoutTipY, nRows, nCols,
		boundaryX, boundaryY, boundaryCount, RingMask, distanceMask,
		distanceFromSnout, bandWidth, demopic1);

	uchar *ringData;
	ringData = (uchar *)RingMask->imageData;
	fileint = fopen("tmp/RingMask", "w");


	// Ear Problem....
	for (int i = 0; i < interestedHeight*nCols; i++) {
		ringData[i] = 0;
	}


	for (int i = 0; i < pixs; i++) {
		fprintf(fileint, "%d\n", ringData[i]);
	}
	fclose(fileint);

	float *distanceData;
	distanceData = (uchar *)distanceMask->imageData;
	fileint = fopen("tmp/distanceMask", "w");
	for (int i = 0; i < pixs; i++) {
		fprintf(fileint, "%f\n", distanceData[i]);
	}
	fclose(fileint);

	// Iterate through all video frames
	cvSetCaptureProperty(capture[1], CV_CAP_PROP_POS_FRAMES, startFrame);

	// Aquire bwshape data
	frame = cvQueryFrame(capture[1]);
	cvCvtColor(frame, greyframe, CV_BGR2GRAY);

	IplConvKernel *erodeDisk = cvCreateStructuringElementEx(razorSize * 2 - 1,
		razorSize * 2 - 1, 0, 0, CV_SHAPE_ELLIPSE, NULL);

	IplImage *bwShape = cvCloneImage(greyframe);
	stExtractEnvGrayValuesWithMorphology(bwShape, bwThreshold, erodeDisk);

	uchar *bwShapeData;
	bwShapeData = (uchar *)bwShape->imageData;

	fileint = fopen("tmp/bwShape", "w");
	for (int i = 0; i < pixs; i++) {
		fprintf(fileint, "%d\n", bwShapeData[i]);
	}
	fclose(fileint);

	cvSetCaptureProperty(capture[1], CV_CAP_PROP_POS_FRAMES, startFrame);
	frame = cvQueryFrame(capture[1]);
	cvCvtColor(frame, greyframe, CV_BGR2GRAY);

	
	// Removed for fast GUI!
	/*

	// Tracking Trail for GUI...

	ppExtractionAndFilter(greyframe, pixs, ppBg, bwShapeData);

	IplImage *demopic2 = cvCreateImage(
		cvSize(nCols, nRows), IPL_DEPTH_8U, 3);

	cvCvtColor(greyframe, demopic2, CV_GRAY2BGR);

	int sourceX[20], sourceY[20], sinkX[20], sinkY[20];

	int nptsSharedPaths = cMass;

	int selectedWKCount = wdIgorMeanAngleProcess(greyframe, RingMask,
		nCols, nRows, pixs, snoutCenterX, snoutCenterY,
		distanceMask, sourceX, sourceY, sinkX, sinkY, 0, nptsSharedPaths);

	CvScalar colormap[20];

	colormap[0] = cvScalar(0, 0, 255, 0);
	colormap[1] = cvScalar(0, 128, 255, 0);
	colormap[2] = cvScalar(0, 255, 255, 0);
	colormap[3] = cvScalar(0, 255, 128, 0);
	colormap[4] = cvScalar(0, 255, 0, 0);
	colormap[5] = cvScalar(128, 255, 0, 0);
	colormap[6] = cvScalar(255, 0, 0, 0);
	colormap[7] = cvScalar(255, 128, 0, 0);
	colormap[8] = cvScalar(255, 0, 0, 0);
	colormap[9] = cvScalar(127, 0, 255, 0);
	colormap[10] = cvScalar(255, 0, 255, 0);
	colormap[11] = cvScalar(255, 0, 127, 0);
	colormap[12] = cvScalar(153, 153, 255, 0);
	colormap[13] = cvScalar(76, 0, 153, 0);
	colormap[14] = cvScalar(255, 204, 255, 0);
	colormap[15] = cvScalar(102, 169, 255, 0);
	colormap[16] = cvScalar(214, 0, 255, 0);
	colormap[17] = cvScalar(0, 125, 255, 0);
	colormap[18] = cvScalar(255, 20, 123, 0);
	colormap[19] = cvScalar(255, 0, 120, 0);


	for (int i = 0; i < selectedWKCount; i++) {
		cvLine(demopic2, cvPoint(sourceX[i],sourceY[i]), cvPoint(sinkX[i], sinkY[i]),
		colormap[i], 2, 8, 0);
	}

	int p[3];
	p[0] = CV_IMWRITE_JPEG_QUALITY;
	p[1] = 95;
	p[2] = 0;

	cvSaveImage("tmp/demo2.jpg", demopic2, p);

	*/
}