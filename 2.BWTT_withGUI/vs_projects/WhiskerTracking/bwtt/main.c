#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include <omp.h>
#include "sdGeneric.h"
#include "stShapeSpaceKalman.h"
#include "ppBgExtractionAndFilter.h"
#include "wdIgorMeanAngle.h"



int main(int argc, char *argv[])
{

	//-----------------------Main Parameter Setting--------------------
	// Segment of frame for processing
	int startFrame = 8000, endFrame = 10000;

	// Parameters
	int snoutCenterX = 389;
	int snoutCenterY = 145;
	int snoutTipX = 417;
	int snoutTipY = 328;
	char address[256];

	float bwThreshold = 0.2;
	int razorSize = 8;

	int distanceFromSnout = 20;
	int bandWidth = 8;
	int cMass = 14;
	float hisgamma = 2.5;
	int interestedHeight = 0;
	int numofThreads = 1;

	FILE * fp;
	fp = fopen("tmp/parameters", "r");

	fscanf(fp, "Video Address:%s\n", address);
	fscanf(fp, "Starting Frame Index:%d\n", &startFrame);
	fscanf(fp, "End Frame Index:%d\n", &endFrame);
	fscanf(fp, "Snout Center X:%d\n", &snoutCenterX);
	fscanf(fp, "Snout Center Y:%d\n", &snoutCenterY);
	fscanf(fp, "Snout Tip X:%d\n", &snoutTipX);
	fscanf(fp, "Snout Tip Y:%d\n", &snoutTipY);
	fscanf(fp, "Razor Size:%d\n", &razorSize);
	fscanf(fp, "Black/White Threshold:%f\n", &bwThreshold);
	fscanf(fp, "cMass T:%d\n", &cMass);
	fscanf(fp, "Histogram Gamma:%f\n", &hisgamma);
	fscanf(fp, "Minimum Distance:%d\n", &distanceFromSnout);
	fscanf(fp, "Bandwidth:%d\n", &bandWidth);
	fscanf(fp, "Interested Height:%d\n", &interestedHeight);
	fscanf(fp, "Num of threads:%d", &numofThreads);

	fclose(fp);

	/*
	printf("Please enter the index of starting frame: \n");
	scanf("%d", &startFrame);
	printf("Please enter the index of end frame: \n");
	scanf("%d", &endFrame);
	printf("Please enter the X of snout center: \n");
	scanf("%d", &snoutCenterX);
	printf("Please enter the Y of snout center: \n");
	scanf("%d", &snoutCenterY);
	printf("Please enter the X of snout tip: \n");
	scanf("%d", &snoutTipX);
	printf("Please enter the Y of snout tip \n");
	scanf("%d", &snoutTipY);
	*/


	// Read Video File
	CvCapture* capture[2];

	for (int i = 0; i < 1; i++) {
		capture[i] = cvCaptureFromFile(address);
	}

	//--------------------------The End ---------------------------------


	uchar *frameData;
	IplImage *frame = 0;


	FILE *file1;
	file1 = fopen("output/source", "w");


	int frameNumbers = cvGetCaptureProperty(capture[0], CV_CAP_PROP_FRAME_COUNT);

	if (endFrame > frameNumbers) {
		endFrame = frameNumbers;
	}

	int nRows = cvGetCaptureProperty(capture[0], CV_CAP_PROP_FRAME_HEIGHT);
	int nCols = cvGetCaptureProperty(capture[0], CV_CAP_PROP_FRAME_WIDTH);

	int pixs = nRows*nCols;

	IplImage *greyframe = cvCreateImage(
		cvSize(nCols, nRows), IPL_DEPTH_8U, 1);

	// Read Background Image
	uchar *ppBg;

	ppBg = (uchar*)calloc(nRows*nCols, sizeof(uchar));
	//memset(ppBg, 0, nRows*nCols);
	
	FILE *fileint;
	fileint = fopen("tmp/ppBg", "r");

	for (int i = 0; i < pixs; i++) {
		int temp;
		fscanf(fileint, "%d\n", &temp);
		ppBg[i] = (uchar)temp;
	}

	fclose(fileint);

	
	// Read Ring Mask
	IplImage *RingMask = cvCreateImage(
		cvSize(nCols, nRows), IPL_DEPTH_8U, 1);

	fileint = fopen("tmp/RingMask", "r");
	uchar *ringData;
	ringData = (uchar *)RingMask->imageData;

	for (int i = 0; i < pixs; i++) {
		int temp;
		fscanf(fileint, "%d\n", &temp);
		ringData[i] = (uchar)temp;
	}

	fclose(fileint);

	// Read Distance Mask
	IplImage *distanceMask = cvCreateImage(
		cvSize(nCols, nRows), IPL_DEPTH_32F, 1);
	float *distanceData;
	distanceData = (uchar *)distanceMask->imageData;

	fileint = fopen("tmp/distanceMask", "r");

	for (int i = 0; i < pixs; i++) {
		float temp;
		fscanf(fileint, "%f\n", &temp);
		distanceData[i] = temp;
	}

	fclose(fileint);



	// Read BWShape
	IplImage *bwShape = cvCreateImage(
		cvSize(nCols, nRows), IPL_DEPTH_8U, 1);

	fileint = fopen("tmp/bwShape", "r");
	uchar *bwShapeData;
	bwShapeData = (uchar *)bwShape->imageData;

	for (int i = 0; i < pixs; i++) {
		int temp;
		fscanf(fileint, "%d\n", &temp);
		bwShapeData[i] = (uchar)temp;
	}
	fclose(fileint);

	/* // Use this for multiple reading!
	for (int i = 0; i < 1; i++) {
		cvSetCaptureProperty(capture[i], CV_CAP_PROP_POS_FRAMES, startFrame + 1000 * i);
	}
	*/

	cvSetCaptureProperty(capture[0], CV_CAP_PROP_POS_FRAMES, startFrame);

	int sourceX[10000], sourceY[10000], sinkX[10000], sinkY[10000];
	int selectedWKCount[1000];

	IplImage *ppgreyFrames[40];
	for (int ffi = 0; ffi < 40; ffi++) {
		ppgreyFrames[ffi] = cvCreateImage(
			cvSize(nCols, nRows), IPL_DEPTH_8U, 1);
	}

	int fi = 0;

	//clock_t start = clock(), diff;
	
	//omp_set_dynamic(0);
	omp_set_num_threads(numofThreads);
	//int threadNum = omp_get_max_threads();
	//double start_time = omp_get_wtime();

	int nptsSharedPaths = cMass;

	int threadNum = omp_get_max_threads();

	int segments = (endFrame - startFrame + 1)/ threadNum;

	int progressp = 0;

	for (int segcount = 0; segcount < segments; segcount++) {

		//printf("Processing Segment %d\n", segcount);

		if (100 * segcount / segments > progressp) {
			progressp = 100 * segcount / segments;
			printf("\r %d %% Finished: %d Frames Processed", progressp, segcount*threadNum);
		}

		for (int ffi = 0; ffi < threadNum; ffi++) {
			frame = cvQueryFrame(capture[0]);
			cvCvtColor(frame, ppgreyFrames[ffi], CV_BGR2GRAY);
		}


		#pragma omp parallel for private(fi, frame)
		for (fi = 0; fi < threadNum; fi++) {
			
			// Extract Whisker Images

			//frame = cvQueryFrame(capture[fi]);
			//cvCvtColor(frame, ppgreyFrames[fi], CV_BGR2GRAY);

			ppExtractionAndFilter(ppgreyFrames[fi], pixs, ppBg, bwShapeData);

			// Calculate Mean Angle
			selectedWKCount[fi] = wdIgorMeanAngleProcess(ppgreyFrames[fi], RingMask,
				nCols, nRows, pixs, snoutCenterX, snoutCenterY,
				distanceMask, sourceX, sourceY, sinkX, sinkY, fi, nptsSharedPaths);

		}

		for (int ffi = 0; ffi < threadNum; ffi++) {
			fprintf(file1, "---\n");
			long tempbase = ffi * 20;
			for (int i = 0; i < selectedWKCount[ffi]; i++) {

				fprintf(file1, "%d\t%d\t%d\t%d\n",
					sourceY[tempbase + i], sourceX[tempbase + i],
					sinkY[tempbase + i], sinkX[tempbase + i]);
			}
		}

	}


	//double time = omp_get_wtime() - start_time;

	//printf("Processed 10000 frames \n");
	//printf("Time taken %.16g seconds\n", time);

	printf("\n Done!");

	// release the image
	cvReleaseImage(&greyframe);
	cvReleaseImage(&RingMask);
	cvReleaseImage(&bwShape);


	for (int i = 0; i < 1; i++) {
		cvReleaseCapture(&capture[i]);
	}

	cvReleaseImage(&distanceMask);

	for (int ffi = 0; ffi < 40; ffi++) {
		cvReleaseImage(&ppgreyFrames[ffi]);
	}

	free(ppBg);
	fclose(file1);

	getchar();

	return 0;

}