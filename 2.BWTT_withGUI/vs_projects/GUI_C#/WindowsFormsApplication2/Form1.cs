﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using AForge.Video;
using AForge.Video.FFMPEG;
using AForge.Imaging;
using AForge.Video.DirectShow;
using AForge.Video.Kinect;
using AForge.Video.VFW;
using AForge.Video.Ximea;

namespace WindowsFormsApplication2
{

    // BlackWhite Threshold;
    // BlackWhite Morphology;
    // Razor size
    // ;;;;;;;;;;;;; 
    //Gamma
    // Min T
    // Max T
    // BlackWhite Threshold;
    // BlackWhite Morphology;
    // Razor Size
    // bandwidth
    // peakT
    //theataT
    //cmassT
    public partial class Form1 : Form
    {

        public long frameCount;
        VideoFileReader reader = new VideoFileReader();

        int snoutCenterX, snoutCenterY, snoutTipX, snoutTipY;
        bool ready1 = false;
        bool ready2 = false;

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (openFileDialog1.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                string strfilename = openFileDialog1.InitialDirectory + openFileDialog1.FileName;
                textBox1.Text = strfilename;
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string targetPath = @"tmp";
            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            System.IO.StreamWriter file = new System.IO.StreamWriter(@"tmp\parameters");
            file.WriteLine("Video Address:"+textBox1.Text);
            file.WriteLine("Starting Frame Index:" + numericUpDown1.Value);
            file.WriteLine("End Frame Index:" + numericUpDown2.Value);
            file.WriteLine("Snout Center X:" + snoutCenterX);
            file.WriteLine("Snout Center Y:" + snoutCenterY);
            file.WriteLine("Snout Tip X:" + snoutTipX);
            file.WriteLine("Snout Tip Y:" + snoutTipY);
            file.WriteLine("Razor Size:" + numericUpDown3.Value);
            file.WriteLine("Black/White Threshold:" + numericUpDown4.Value);
            file.WriteLine("cMass T:" + numericUpDown5.Value);
            file.WriteLine("Histogram Gamma:" + numericUpDown6.Value);
            file.WriteLine("Minimum Distance:" + numericUpDown7.Value);
            file.WriteLine("Bandwidth:" + numericUpDown8.Value);
            file.Close();
            button4.Enabled = true;

            /*
            string fileName = "parameters";
            string targetPath = @"core\preprocessing";

            string destFile = System.IO.Path.Combine(targetPath, fileName);

            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            System.IO.File.Copy(fileName, destFile, true);

            targetPath = @"core\tracking";
            destFile = System.IO.Path.Combine(targetPath, fileName);
            

            if (!System.IO.Directory.Exists(targetPath))
            {
                System.IO.Directory.CreateDirectory(targetPath);
            }

            System.IO.File.Copy(fileName, destFile, true);

            */

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Process process = Process.Start(@"core\preprocessing\preprocessing.exe");
            int id = process.Id;
            Process tempProc = Process.GetProcessById(id);
            this.Visible = false;
            tempProc.WaitForExit();
            this.Visible = true;
            button5.Enabled = true;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Process process = Process.Start(@"core\tracking\tracking.exe");
            int id = process.Id;
            Process tempProc = Process.GetProcessById(id);
            this.Visible = false;
            tempProc.WaitForExit();
            this.Visible = true;
        }


        private void button2_Click(object sender, EventArgs e)
        {
            reader.Open(textBox1.Text);
            Bitmap videoFrame = reader.ReadVideoFrame();

            pictureBox1.Image = videoFrame;
            frameCount = reader.FrameCount;
            numericUpDown1.Maximum = frameCount - 1;
            numericUpDown1.Minimum = 0;
            numericUpDown1.Value = 1000;
            numericUpDown2.Maximum = frameCount - 1;
            numericUpDown2.Minimum = 0;
            numericUpDown2.Value = frameCount - 1;
            reader.Close();
            ParametersBox.Enabled = true;
        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {
            reader.Open(textBox1.Text);
            Bitmap videoFrame = reader.ReadVideoFrame();
            for(int i = 0; i < numericUpDown1.Value; i++) {
                videoFrame.Dispose();
                videoFrame = reader.ReadVideoFrame();
            }
            pictureBox1.Image = videoFrame;
            reader.Close();
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            MouseEventArgs me = (MouseEventArgs)e;
            switch (me.Button)
            {

                case MouseButtons.Left:
                    // Left click
                    label5.Text = "(" + me.X + "," + me.Y + ")";
                    snoutCenterX = me.X;
                    snoutCenterY = me.Y;
                    ready1 = true;
                    break;

                case MouseButtons.Right:
                    // Right click
                    label6.Text = "(" + me.X + "," + me.Y + ")";
                    snoutTipX = me.X;
                    snoutTipY = me.Y;
                    ready2 = true;
                    break;
            }

            button3.Enabled = ready1 & ready2;
        }
    }
}