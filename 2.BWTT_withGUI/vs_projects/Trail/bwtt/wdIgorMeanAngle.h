/**
* @file wdIgorMeanAngle.h
* @Funtions for Extracting Whisker Segments
* @author Yang Ma, Erasmus MC
*/

/*
% This function attempts to find the snout contour using a pre-defined contour template
% Inputs:
%           -(snoutCenterX, snoutCenterY) is the user selected snout center
%           -(noseTipX, noseTipY) is the user selected nose tip
%           -sdContour is a vector of points extracted from previous section
% Outputs:
%           -snoutContour is the estiamted contour of snout
*/

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>
#include "createWhiskerTree.h"


int wdIgorMeanAngleProcess(IplImage *greyFrame, IplImage *RingMask, int cols, int rows, int pixs,
	int snoutCenterX, int snoutCenterY, IplImage *distanceMask,
	int *sourceX, int *sourceY, int *sinkX, int *sinkY, int ffi);
void polyfit(uchar *lutFit, int * sumCount, int *iSource, int *iSink,
	int numSources, int numSinks, int *selectedWTreeX, int *selectedWTreeY,
	float *selectedWTreeRho, int tokensIndex);
void QuickSumSort(unsigned long left, unsigned long right, int *sumCount, unsigned long * finalIndex);


int wdIgorMeanAngleProcess(IplImage *greyFrame, IplImage *RingMask, int cols, int rows, int pixs,
	int snoutCenterX, int snoutCenterY, IplImage *distanceMask,
	int *sourceX, int *sourceY, int *sinkX, int *sinkY, int ffi) {

	// Parameter Setting
	int peakT = 40;
	float thetaT = 2;
	float secThresh = thetaT;
	float bandWidth = 9;
	int nptsSharedPaths = 8; //cMassT

	// Get Whisker image in rings
	uchar *frameData;
	frameData = (uchar *)greyFrame->imageData;



	uchar *RingMaskData;
	RingMaskData = (uchar *)RingMask->imageData;
	for (int i = 0; i < pixs; i++) {
		frameData[i] = frameData[i] * RingMaskData[i];
	}



	// Double the image size
	IplImage *duoImage = cvCreateImage(
		cvSize(2*cols, 2*rows), IPL_DEPTH_8U, 1);
	cvResize(greyFrame, duoImage, CV_INTER_CUBIC);
	snoutCenterX *= 2;
	snoutCenterY *= 2;

	// STEP1 --> set parameters and CREATE WHIKSER TREE
	int trBinary = 1;
	if (peakT - 10 > 1) {
		trBinary = peakT - 10;
	}
	//float trBinary = max(peakT - 10, 1)/255.0f;


	int selectedWTreeX[1000], selectedWTreeY[1000];
	float selectedWTreeRho[1000];

	int tokensIndex = createWhiskerTree(duoImage, snoutCenterX, snoutCenterY,
		trBinary, 2*cols, 2*rows, selectedWTreeX, selectedWTreeY, selectedWTreeRho);

	// STEP2 --> FIND SOURCES AND SINKS

	float *disData = (float *)distanceMask->imageData;

	int idxSinks[1000], idxSources[1000];
	int sinkCount = 0, sourceCount = 0;
	for (int i = 0; i < tokensIndex; i++) {
		int coord = selectedWTreeX[i] / 2 + (selectedWTreeY[i] / 2) * cols;
		float distemp = disData[coord];

		//float temp = disData[selectedWTree[i].X / 2 + selectedWTree[i].Y*cols / 2];
		if (distemp < secThresh) {
			idxSinks[sinkCount++] = i;
		}
		if (distemp > bandWidth - secThresh) {
			idxSources[sourceCount++] = i;
		}
	}

	// STEP3 --> POLYFIT


	uchar *lutFit;
	int *sumCount;
	unsigned long limit = (unsigned long)sinkCount * (unsigned long)sourceCount
		* (unsigned long)tokensIndex;
	unsigned long size1 = (unsigned long)sinkCount * (unsigned long)sourceCount *
		(unsigned long)sizeof(int);
	unsigned long sizel = (unsigned long)sinkCount * (unsigned long)sourceCount *
			(unsigned long)sizeof(unsigned long);
	unsigned long size2 = limit * (unsigned long)sizeof(uchar);

	sumCount = (int*)malloc(size1);
	lutFit = (uchar*)malloc(size2);

	//memory allocated using malloc
	if (lutFit == NULL || sumCount == NULL)
	{
		printf("Error! memory not allocated.");
		exit(0);
	}

	int *sumCountTemp;
	sumCountTemp = (int*)malloc(size1);

	polyfit(lutFit, sumCount, idxSources, idxSinks,
		sourceCount, sinkCount, selectedWTreeX,
		selectedWTreeY, selectedWTreeRho, tokensIndex);



	// STEP 4 --> Sort & Select
	unsigned long *finalIndex;
	finalIndex = (unsigned long*)malloc(sizel);
	unsigned long totalOptions = (unsigned long)sinkCount * (unsigned long)sourceCount;
	for(unsigned long i = 0; i < totalOptions; i++) {
		finalIndex[i] = i;
		sumCountTemp[i] = sumCount[i];
	}


	QuickSumSort(0, totalOptions - 1, sumCountTemp, finalIndex);


	unsigned long resultIndex[100];
	int rstCount = 0;


	for (unsigned long i = 0; i < totalOptions; i++) {
		//int tempsum = 0;
		unsigned long tempbase = (unsigned long)finalIndex[i] * (unsigned long)tokensIndex;

		/*for (int j = 0; j < tokensIndex; j++) {
			if (lutFit[tempbase + j] == 1) {
				tempsum++;
			}
		}
		*/
		if (sumCount[finalIndex[i]] >= nptsSharedPaths) {
			resultIndex[rstCount++] = finalIndex[i];
			for (int j = 0; j < tokensIndex; j++) {
				if (lutFit[tempbase + j] == 1) {
					unsigned long k = j;
					for (unsigned long opindex = 0; opindex < totalOptions; opindex++) {
						if (lutFit[k] == 1) {
							lutFit[k] = 0;
							sumCount[opindex]--;
						}
						k += tokensIndex;
					}
				}
			}
		}
	}


	//float *disData = (float *)distanceMask->imageData;
	long tmpbase = ffi * 20;
	for (int i = 0; i < rstCount; i++) {
		int tempso = resultIndex[i] % sourceCount;
		int tempsi = resultIndex[i] / sourceCount;
		sourceX[tmpbase + i] = selectedWTreeX[idxSources[tempso]] / 2;
		sourceY[tmpbase + i] = selectedWTreeY[idxSources[tempso]] / 2;
		sinkX[tmpbase + i] = selectedWTreeX[idxSinks[tempsi]] / 2;
		sinkY[tmpbase + i] = selectedWTreeY[idxSinks[tempsi]] / 2;
	}



	free(lutFit);
	free(sumCount);
	free(finalIndex);
	cvReleaseImage(&duoImage);

	return rstCount;

}

void polyfit(uchar *lutFit, int * sumCount, int *iSource, int *iSink,
	 int numSources, int numSinks, int *selectedWTreeX, int *selectedWTreeY,
	 float *selectedWTreeRho, int tokensIndex) {

	float clusteringThreshold = 1.5;

	for (int i = 0; i < numSources; i++) {
		for (int j = 0; j < numSinks; j++) {

			unsigned int so = iSource[i];
			unsigned int si = iSink[j];
			unsigned long base = i + j*numSources;

			double px = (selectedWTreeX[si] - selectedWTreeX[so]) /
				(selectedWTreeRho[si] - selectedWTreeRho[so]);
			double bx = selectedWTreeX[so] - px*selectedWTreeRho[so];

			double py = (selectedWTreeY[si] - selectedWTreeY[so]) /
				(selectedWTreeRho[si] - selectedWTreeRho[so]);
			double by = selectedWTreeY[so] - py*selectedWTreeRho[so];

			sumCount[base] = 0;

			for (int k = 0; k < tokensIndex; k++) {
				double errorx = px*selectedWTreeRho[k] + bx - selectedWTreeX[k];
				double errory = py*selectedWTreeRho[k] + by - selectedWTreeY[k];
				double err = errorx*errorx + errory*errory;
				if (err < clusteringThreshold && selectedWTreeRho[k] < selectedWTreeRho[si]
					&& selectedWTreeRho[k] >selectedWTreeRho[so]) {
					unsigned long temp = (unsigned long)base*(unsigned long)tokensIndex + k;
					lutFit[temp] = 1;
					sumCount[base]++;
				}
			}

		}
	}

}

void QuickSumSort(unsigned long left, unsigned long right, int *sumCount, unsigned long * finalIndex) {

	unsigned long i = left, j = right;
	int key = sumCount[left];
	unsigned long tempidx = finalIndex[left];

	if (left >= right) {
		return;
	}

	while (i < j) {

		while (i<j && sumCount[j] < key)
			j--;
		finalIndex[i] = finalIndex[j];
		sumCount[i++] = sumCount[j];

		while (i<j && sumCount[i]  > key)
			i++;
		if (i < j) {
			finalIndex[j] = finalIndex[i];
			sumCount[j--] = sumCount[i];
		}
	}
	finalIndex[i] = tempidx;
	sumCount[i] = key;
	QuickSumSort(left, i - 1, sumCount, finalIndex);
	QuickSumSort(i + 1, right, sumCount, finalIndex);
}
