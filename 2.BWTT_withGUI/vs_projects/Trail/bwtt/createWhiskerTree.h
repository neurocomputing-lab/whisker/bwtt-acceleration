/**
* @file wdIgorMeanAngle.h
* @Funtions for Extracting Whisker Segments
* @author Yang Ma, Erasmus MC
*/

/*
% This function attempts to find the snout contour using a pre-defined contour template
% Inputs:
%           -(snoutCenterX, snoutCenterY) is the user selected snout center
%           -(noseTipX, noseTipY) is the user selected nose tip
%           -sdContour is a vector of points extracted from previous section
% Outputs:
%           -snoutContour is the estiamted contour of snout
*/


#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>


int createWhiskerTree(IplImage *duoImage, int snoutCenterX,
	int snoutCenterY, float trBinary, int cols, int rows, int *selectedWTreeX, int *selectedWTreeY,
	float *selectedWTreeRho);
void QuickSortTree(int left, int right, int *whiskerTreeX, int *whiskerTreeY,
	float *whiskerTreeRho, float *whiskerTreeTheta, int *whiskerTreeI);
int findLocalTokens(int localTokensN,
	int trInnerRing, int start, int end, int rhoRange, int *idx, int *whiskerTreeI);
int groupConnectedTokens(int *idx, int tokenCount, int tokensIndex,
	int *whiskerTreeX, int *whiskerTreeY, float *whiskerTreeRho,
	int *selectedWTreeX, int *selectedWTreeY, float *selectedWTreeRho);

int createWhiskerTree(IplImage *duoImage, int snoutCenterX,
	int snoutCenterY, float trBinary, int cols, int rows, int *selectedWTreeX, int *selectedWTreeY,
	float *selectedWTreeRho) {

	int minimumRho = 20;
	int localTokensN = 6;
	int peakT = 30;
	int trInnerRing = peakT;
	int tokensIndex = 0;


	uchar *duoData = (uchar *)duoImage->imageData;

	// [Eliminated] Initialize Conv kernel
	int whiskerTreeX[10000], whiskerTreeY[10000], whiskerTreeI[10000];
	float whiskerTreeRho[10000], whiskerTreeTheta[10000];


	// Calcualte rhos
	int wtCount = 0;
	int maxRho = 0;
	for (int i = 0; i < rows; i++) {
		for (int j = 0; j < cols; j++) {
			int tintens = duoData[i*cols + j];
			if (tintens > trBinary) {
				double tempX = j - snoutCenterX;
				double tempY = i - snoutCenterY;
				float tempRho = sqrt(tempX*tempX + tempY*tempY);
				if (tempRho > maxRho) {
					maxRho = (int)tempRho;
				}
				whiskerTreeRho[wtCount] = tempRho;
				whiskerTreeTheta[wtCount] = atan2(tempY, tempX);
				whiskerTreeX[wtCount] = j;
				whiskerTreeY[wtCount] = i;
				whiskerTreeI[wtCount] = tintens;
				wtCount++;
			}
		}
	}

	//Sort According to Rho
	QuickSortTree(0, wtCount - 1, whiskerTreeX, whiskerTreeY,
		whiskerTreeRho, whiskerTreeTheta, whiskerTreeI);

	int wtStart = 0;
	// Search through all radis
	for (int i = minimumRho; i < maxRho; i++) {
		int prev = wtStart;
		for (int j = wtStart; j < wtCount; j++) {
			//Rule Out Facial Hair
			float compRho = whiskerTreeRho[j] - (float)i;
			if (compRho < 1 && compRho > -1) {
				whiskerTreeX[wtStart] = whiskerTreeX[j];
				whiskerTreeY[wtStart] = whiskerTreeY[j];
				whiskerTreeRho[wtStart] = whiskerTreeRho[j];
				whiskerTreeTheta[wtStart] = whiskerTreeTheta[j];
				whiskerTreeI[wtStart++] = whiskerTreeI[j];
			}
		}
		if (wtStart > prev) {

			int idx[1000];
			// Quick sort by theta (convinent for small array)
			QuickSortTree(prev, wtStart - 1, whiskerTreeX, whiskerTreeY,
				whiskerTreeTheta, whiskerTreeRho, whiskerTreeI);

			int tokenCount = findLocalTokens(localTokensN,
				trInnerRing, prev, wtStart, i, idx, whiskerTreeI);

			if (tokenCount > 0) {
				tokensIndex = groupConnectedTokens(idx, tokenCount, tokensIndex,
					whiskerTreeX, whiskerTreeY, whiskerTreeRho,
					selectedWTreeX, selectedWTreeY, selectedWTreeRho);
			}
		}
	}


	return tokensIndex;
}

void QuickSortTree(int left, int right, int *whiskerTreeX, int *whiskerTreeY,
	float *whiskerTreeRho, float *whiskerTreeTheta, int *whiskerTreeI) {

	int i = left, j = right;
	float key = whiskerTreeRho[left];
	int tempX = whiskerTreeX[left], tempY = whiskerTreeY[left], tempI = whiskerTreeI[left];
	float tempTheta = whiskerTreeTheta[left];

	if (left >= right) {
		return;
	}

	while (i < j) {

		while (i<j && whiskerTreeRho[j] > key)
			j--;

		whiskerTreeY[i] = whiskerTreeY[j];
		whiskerTreeTheta[i] = whiskerTreeTheta[j];
		whiskerTreeI[i] = whiskerTreeI[j];
		whiskerTreeRho[i] = whiskerTreeRho[j];
		whiskerTreeX[i++] = whiskerTreeX[j];

		while (i<j && whiskerTreeRho[i] < key)
			i++;
		if (i < j) {
			whiskerTreeY[j] = whiskerTreeY[i];
			whiskerTreeTheta[j] = whiskerTreeTheta[i];
			whiskerTreeI[j] = whiskerTreeI[i];
			whiskerTreeRho[j] = whiskerTreeRho[i];
			whiskerTreeX[j--] = whiskerTreeX[i];
		}
	}

	whiskerTreeX[i] = tempX;
	whiskerTreeY[i] = tempY;
	whiskerTreeRho[i] = key;
	whiskerTreeTheta[i] = tempTheta;
	whiskerTreeI[i] = tempI;

	QuickSortTree(left, i - 1, whiskerTreeX, whiskerTreeY,
		whiskerTreeRho, whiskerTreeTheta, whiskerTreeI);
	QuickSortTree(i + 1, right, whiskerTreeX, whiskerTreeY,
		whiskerTreeRho, whiskerTreeTheta, whiskerTreeI);
}


int findLocalTokens(int localTokensN,
	int trInnerRing, int start, int end, int rhoRange, int *idx, int *whiskerTreeI) {

	int rhoThresh = 1000;
	int trOuterRing = 0;
	float Thresh;

	if (rhoRange < rhoThresh)
		Thresh = trInnerRing;
	else
		Thresh = trOuterRing;

	int maxIntens = 0;
	int maxCount = 0;

	//Neighboring case
	if (end - start > localTokensN / 2) {
		int L = end - start;
		int f[1000], maxVec[1000], rightVec[1000], leftVec[1000];
		for (int i = start; i < end; i++) {
			f[i - start] = whiskerTreeI[i];
			maxVec[i - start] = f[i - start];
		}

		for (int i = 0; i < localTokensN / 2; i++) {
			for (int j = 0; j < L; j++) {
				if (j <= i) {
					rightVec[j] = 0;
				}
				else {
					rightVec[j] = f[j - i - 1];
				}

				if (j >= L - i - 1) {
					leftVec[j] = 0;
				}
				else {
					leftVec[j] = f[i + j + 1];
				}
				int tempVec = rightVec[j]>leftVec[j] ? rightVec[j] : leftVec[j];
				if (maxVec[j] < tempVec) {
					maxVec[j] = tempVec;
				}
				//maxVec[j] = max(maxVec[j], max(rightVec[j], leftVec[j]));
			}
		}

		for (int i = start; i < end; i++) {
			if (whiskerTreeI[i] == maxVec[i - start] &&
				maxVec[i - start] > Thresh) {
				idx[maxCount++] = i;
			}
		}
	}
	else { // Simple case
		for (int i = start; i < end; i++) {
			if (whiskerTreeI[i] > maxIntens) {
				maxIntens = whiskerTreeI[i];
			}
		}

		if (maxIntens <= Thresh) {
			return -1;
		}

		for (int i = start; i < end; i++) {
			if (whiskerTreeI[i] == maxIntens) {
				idx[maxCount++] = i;
			}
		}
	}
	return maxCount;

}

int groupConnectedTokens(int *idx, int tokenCount, int tokensIndex,
	int *whiskerTreeX, int *whiskerTreeY, float *whiskerTreeRho,
	int *selectedWTreeX, int *selectedWTreeY, float *selectedWTreeRho) {

	int currentX = whiskerTreeX[idx[0]], currentY = whiskerTreeY[idx[0]];
	int xOfToken[50], yOfToken[50];
	float rhoOfToken[50];

	int tempCount = 0;

	for (int i = 0; i < tokenCount; i++) {
		int X = whiskerTreeX[idx[i]];
		int Y = whiskerTreeY[idx[i]];
		if (abs(currentX - X) > 1 || abs(currentY - Y) > 1) {
			if (i > 0) {
				int averageX = 0, averageY = 0;
				float averageRho = 0;
				if (tempCount > 2) {
					int maxXofToken = 0, maxYofToken = 0;
					int minXofToken = INT_MAX, minYofToken = INT_MAX;
					for (int i = 0; i < tempCount; i++) {
						if (maxXofToken < xOfToken[i])
							maxXofToken = xOfToken[i];
						if (maxYofToken < yOfToken[i])
							maxYofToken = yOfToken[i];
						if (minXofToken > xOfToken[i])
							minXofToken = xOfToken[i];
						if (minYofToken < yOfToken[i])
							minYofToken = yOfToken[i];
					}
					if (maxXofToken - minXofToken + 1 == tempCount || maxYofToken - minYofToken + 1 == tempCount) {
						selectedWTreeX[tokensIndex] = xOfToken[0];
						selectedWTreeY[tokensIndex] = yOfToken[0];
						selectedWTreeRho[tokensIndex++] = rhoOfToken[0];
						selectedWTreeX[tokensIndex] = xOfToken[tempCount - 1];
						selectedWTreeY[tokensIndex] = yOfToken[tempCount - 1];
						selectedWTreeRho[tokensIndex++] = rhoOfToken[tempCount - 1];
					}
					else {
						for (int i = 0; i < tempCount; i++) {
							averageX += xOfToken[i];
							averageY += yOfToken[i];
							averageRho += rhoOfToken[i];
						}

						selectedWTreeX[tokensIndex] = averageX / tempCount;
						selectedWTreeY[tokensIndex] = averageY / tempCount;
						selectedWTreeRho[tokensIndex++] = averageRho / tempCount;
					}
				}
				else {

					// Calculate average;
					for (int i = 0; i < tempCount; i++) {
						averageX += xOfToken[i];
						averageY += yOfToken[i];
						averageRho += rhoOfToken[i];
					}
					selectedWTreeX[tokensIndex] = averageX / tempCount;
					selectedWTreeY[tokensIndex] = averageY / tempCount;
					selectedWTreeRho[tokensIndex++] = averageRho / tempCount;
				}
				tempCount = 0;
			}
		}
		xOfToken[tempCount] = X;
		yOfToken[tempCount] = Y;
		rhoOfToken[tempCount] = whiskerTreeRho[idx[i]];
		currentX = X;
		currentY = Y;
		tempCount++;
	}

	if (tempCount > 0) {
		int averageX = 0, averageY = 0;
		float averageRho = 0;
		// Calculate average;
		for (int i = 0; i < tempCount; i++) {
			averageX += xOfToken[i];
			averageY += yOfToken[i];
			averageRho += rhoOfToken[i];
		}
		selectedWTreeX[tokensIndex] = averageX / tempCount;
		selectedWTreeY[tokensIndex] = averageY / tempCount;
		selectedWTreeRho[tokensIndex++] = averageRho / tempCount;
	}

	return tokensIndex;
}
