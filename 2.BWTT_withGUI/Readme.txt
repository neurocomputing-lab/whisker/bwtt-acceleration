This folder includes all the codes and resulting windows software of the OMP version of C-based BWTT.

Folder vs_projects includes four projects used in the software development.

1. "GUI_C#" is the vs project using C# to create GUI.
2. "Pre_processing" includes the preprocessing codes needed as the first step of processing.
3. "Trail" includes the codes for trail run on a single frame.
4. "WhikserTracking" includes the whisker tracking codes implemented with OpenMP multihreading.

If any change in made to the source code, the .exe genereated via "release" from step 2-4 have to to be put inside the folder "\BWTT_withGUI\BWTT_win\core" for the GUI to invoke.


Folder BWTT_win contains the final software of the C-based BWTT.