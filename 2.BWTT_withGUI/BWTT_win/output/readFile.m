fid = fopen('source','r');
% tline = fgets(fid);
A=textscan(fid,'%s','delimiter','\n');
B = A{1};
fclose(fid);

% Set the frame size you want
FrameSize = 2000;
result = cell(FrameSize,1);

idx = 0;
for i = 1:numel(B)
   if strcmp(B{i}, '---')
       idx = idx + 1;
   else
       C = textscan(B{i},'%d');
       C{1} = reshape(C{1}, [2, 2]);
       result{idx} = [ result{idx} C];
       
   end
end

