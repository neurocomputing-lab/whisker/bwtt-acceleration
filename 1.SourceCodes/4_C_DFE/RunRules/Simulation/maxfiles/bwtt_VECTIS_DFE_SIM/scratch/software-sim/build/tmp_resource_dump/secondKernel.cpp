#include "stdsimheader.h"
#include "secondKernel.h"

namespace maxcompilersim {

secondKernel::secondKernel(const std::string &instance_name) : 
  ManagerBlockSync(instance_name),
  KernelManagerBlockSync(instance_name, 66, 2, 0, 0, "",1)
, c_hw_fix_1_0_uns_bits((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x1l))))
, c_hw_fix_33_0_uns_bits((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000000l))))
, c_hw_fix_33_0_uns_bits_1((HWOffsetFix<33,0,UNSIGNED>(varint_u<33>(0x000000001l))))
, c_hw_fix_32_0_uns_bits((HWOffsetFix<32,0,UNSIGNED>(varint_u<32>(0x00000000l))))
, c_hw_fix_1_0_uns_bits_1((HWOffsetFix<1,0,UNSIGNED>(varint_u<1>(0x0l))))
, c_hw_flt_8_24_undef((HWFloat<8,24>()))
, c_hw_fix_10_0_uns_undef((HWOffsetFix<10,0,UNSIGNED>()))
, c_hw_fix_1_0_uns_undef((HWOffsetFix<1,0,UNSIGNED>()))
, c_hw_fix_32_0_sgn_undef((HWOffsetFix<32,0,TWOSCOMPLEMENT>()))
, c_hw_flt_8_24_bits((HWFloat<8,24>(varint_u<32>(0x3f99999al))))
, c_hw_fix_16_0_uns_bits((HWOffsetFix<16,0,UNSIGNED>(varint_u<16>(0x0000l))))
, c_hw_fix_16_0_uns_bits_1((HWOffsetFix<16,0,UNSIGNED>(varint_u<16>(0x0001l))))
, c_hw_fix_16_0_uns_undef((HWOffsetFix<16,0,UNSIGNED>()))
, c_hw_fix_49_0_uns_bits((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x1000000000000l))))
, c_hw_fix_49_0_uns_bits_1((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000000l))))
, c_hw_fix_49_0_uns_bits_2((HWOffsetFix<49,0,UNSIGNED>(varint_u<49>(0x0000000000001l))))
{
  { // Node ID: 51 (NodeInputMappedReg)
    registerMappedRegister("io_output_force_disabled", Data(1));
  }
  { // Node ID: 82 (NodeConstantRawBits)
    id82out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 0 (NodeInputMappedReg)
    registerMappedRegister("tokensIndex", Data(32));
  }
  { // Node ID: 81 (NodeConstantRawBits)
    id81out_value = (c_hw_fix_32_0_uns_bits);
  }
  { // Node ID: 5 (NodeInputMappedReg)
    registerMappedRegister("io_px_force_disabled", Data(1));
  }
  { // Node ID: 8 (NodeInput)
     m_px =  registerInput("px",0,5);
  }
  { // Node ID: 57 (NodeMappedRom)
    registerMappedMemory("mappedRomRho", 32, 1000);
  }
  { // Node ID: 9 (NodeInputMappedReg)
    registerMappedRegister("io_bx_force_disabled", Data(1));
  }
  { // Node ID: 12 (NodeInput)
     m_bx =  registerInput("bx",1,5);
  }
  { // Node ID: 55 (NodeMappedRom)
    registerMappedMemory("mappedRomX", 32, 1000);
  }
  { // Node ID: 13 (NodeInputMappedReg)
    registerMappedRegister("io_py_force_disabled", Data(1));
  }
  { // Node ID: 16 (NodeInput)
     m_py =  registerInput("py",2,5);
  }
  { // Node ID: 17 (NodeInputMappedReg)
    registerMappedRegister("io_by_force_disabled", Data(1));
  }
  { // Node ID: 20 (NodeInput)
     m_by =  registerInput("by",3,5);
  }
  { // Node ID: 56 (NodeMappedRom)
    registerMappedMemory("mappedRomY", 32, 1000);
  }
  { // Node ID: 80 (NodeConstantRawBits)
    id80out_value = (c_hw_flt_8_24_bits);
  }
  { // Node ID: 21 (NodeInputMappedReg)
    registerMappedRegister("io_siRho_force_disabled", Data(1));
  }
  { // Node ID: 24 (NodeInput)
     m_siRho =  registerInput("siRho",4,5);
  }
  { // Node ID: 25 (NodeInputMappedReg)
    registerMappedRegister("io_soRho_force_disabled", Data(1));
  }
  { // Node ID: 28 (NodeInput)
     m_soRho =  registerInput("soRho",5,5);
  }
  { // Node ID: 48 (NodeConstantRawBits)
    id48out_value = (c_hw_fix_16_0_uns_bits);
  }
  { // Node ID: 47 (NodeConstantRawBits)
    id47out_value = (c_hw_fix_16_0_uns_bits_1);
  }
  { // Node ID: 54 (NodeOutput)
    m_output = registerOutput("output",0 );
  }
  { // Node ID: 62 (NodeConstantRawBits)
    id62out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 79 (NodeConstantRawBits)
    id79out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 59 (NodeConstantRawBits)
    id59out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 63 (NodeOutputMappedReg)
    registerMappedRegister("current_run_cycle_count", Data(48), true);
  }
  { // Node ID: 78 (NodeConstantRawBits)
    id78out_value = (c_hw_fix_1_0_uns_bits);
  }
  { // Node ID: 65 (NodeConstantRawBits)
    id65out_value = (c_hw_fix_49_0_uns_bits);
  }
  { // Node ID: 68 (NodeInputMappedReg)
    registerMappedRegister("run_cycle_count", Data(48));
  }
}

void secondKernel::resetComputation() {
  resetComputationAfterFlush();
}

void secondKernel::resetComputationAfterFlush() {
  { // Node ID: 51 (NodeInputMappedReg)
    id51out_io_output_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_output_force_disabled");
  }
  { // Node ID: 0 (NodeInputMappedReg)
    id0out_tokensIndex = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("tokensIndex");
  }
  { // Node ID: 2 (NodeCounterV1)
    const HWOffsetFix<32,0,UNSIGNED> &id2in_max = id0out_tokensIndex;

    (id2st_count) = (c_hw_fix_33_0_uns_bits);
  }
  { // Node ID: 5 (NodeInputMappedReg)
    id5out_io_px_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_px_force_disabled");
  }
  { // Node ID: 8 (NodeInput)

    (id8st_read_next_cycle) = (c_hw_fix_1_0_uns_bits_1);
    (id8st_last_read_value) = (c_hw_flt_8_24_undef);
  }
  { // Node ID: 70 (NodeFIFO)

    for(int i=0; i<5; i++)
    {
      id70out_output[i] = (c_hw_fix_10_0_uns_undef);
    }
  }
  { // Node ID: 71 (NodeFIFO)

    for(int i=0; i<9; i++)
    {
      id71out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 9 (NodeInputMappedReg)
    id9out_io_bx_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_bx_force_disabled");
  }
  { // Node ID: 12 (NodeInput)

    (id12st_read_next_cycle) = (c_hw_fix_1_0_uns_bits_1);
    (id12st_last_read_value) = (c_hw_flt_8_24_undef);
  }
  { // Node ID: 77 (NodeFIFO)

    for(int i=0; i<15; i++)
    {
      id77out_output[i] = (c_hw_fix_10_0_uns_undef);
    }
  }
  { // Node ID: 13 (NodeInputMappedReg)
    id13out_io_py_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_py_force_disabled");
  }
  { // Node ID: 16 (NodeInput)

    (id16st_read_next_cycle) = (c_hw_fix_1_0_uns_bits_1);
    (id16st_last_read_value) = (c_hw_flt_8_24_undef);
  }
  { // Node ID: 17 (NodeInputMappedReg)
    id17out_io_by_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_by_force_disabled");
  }
  { // Node ID: 20 (NodeInput)

    (id20st_read_next_cycle) = (c_hw_fix_1_0_uns_bits_1);
    (id20st_last_read_value) = (c_hw_flt_8_24_undef);
  }
  { // Node ID: 21 (NodeInputMappedReg)
    id21out_io_siRho_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_siRho_force_disabled");
  }
  { // Node ID: 24 (NodeInput)

    (id24st_read_next_cycle) = (c_hw_fix_1_0_uns_bits_1);
    (id24st_last_read_value) = (c_hw_flt_8_24_undef);
  }
  { // Node ID: 75 (NodeFIFO)

    for(int i=0; i<53; i++)
    {
      id75out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 25 (NodeInputMappedReg)
    id25out_io_soRho_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_soRho_force_disabled");
  }
  { // Node ID: 28 (NodeInput)

    (id28st_read_next_cycle) = (c_hw_fix_1_0_uns_bits_1);
    (id28st_last_read_value) = (c_hw_flt_8_24_undef);
  }
  { // Node ID: 76 (NodeFIFO)

    for(int i=0; i<54; i++)
    {
      id76out_output[i] = (c_hw_fix_1_0_uns_undef);
    }
  }
  { // Node ID: 60 (NodeCounterV1)

    (id60st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 66 (NodeCounterV1)

    (id66st_count) = (c_hw_fix_49_0_uns_bits_1);
  }
  { // Node ID: 68 (NodeInputMappedReg)
    id68out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void secondKernel::updateState() {
  { // Node ID: 51 (NodeInputMappedReg)
    id51out_io_output_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_output_force_disabled");
  }
  { // Node ID: 0 (NodeInputMappedReg)
    id0out_tokensIndex = getMappedRegValue<HWOffsetFix<32,0,UNSIGNED> >("tokensIndex");
  }
  { // Node ID: 5 (NodeInputMappedReg)
    id5out_io_px_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_px_force_disabled");
  }
  { // Node ID: 9 (NodeInputMappedReg)
    id9out_io_bx_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_bx_force_disabled");
  }
  { // Node ID: 13 (NodeInputMappedReg)
    id13out_io_py_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_py_force_disabled");
  }
  { // Node ID: 17 (NodeInputMappedReg)
    id17out_io_by_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_by_force_disabled");
  }
  { // Node ID: 21 (NodeInputMappedReg)
    id21out_io_siRho_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_siRho_force_disabled");
  }
  { // Node ID: 25 (NodeInputMappedReg)
    id25out_io_soRho_force_disabled = getMappedRegValue<HWOffsetFix<1,0,UNSIGNED> >("io_soRho_force_disabled");
  }
  { // Node ID: 68 (NodeInputMappedReg)
    id68out_run_cycle_count = getMappedRegValue<HWOffsetFix<48,0,UNSIGNED> >("run_cycle_count");
  }
}

void secondKernel::preExecute() {
  { // Node ID: 8 (NodeInput)
    if(((needsToReadInput(m_px))&(((getFlushLevel())<((4l)+(5)))|(!(isFlushingActive()))))) {
      (id8st_last_read_value) = (readInput<HWFloat<8,24> >(m_px));
    }
    id8out_data = (id8st_last_read_value);
  }
  { // Node ID: 12 (NodeInput)
    if(((needsToReadInput(m_bx))&(((getFlushLevel())<((12l)+(5)))|(!(isFlushingActive()))))) {
      (id12st_last_read_value) = (readInput<HWFloat<8,24> >(m_bx));
    }
    id12out_data = (id12st_last_read_value);
  }
  { // Node ID: 16 (NodeInput)
    if(((needsToReadInput(m_py))&(((getFlushLevel())<((4l)+(5)))|(!(isFlushingActive()))))) {
      (id16st_last_read_value) = (readInput<HWFloat<8,24> >(m_py));
    }
    id16out_data = (id16st_last_read_value);
  }
  { // Node ID: 20 (NodeInput)
    if(((needsToReadInput(m_by))&(((getFlushLevel())<((12l)+(5)))|(!(isFlushingActive()))))) {
      (id20st_last_read_value) = (readInput<HWFloat<8,24> >(m_by));
    }
    id20out_data = (id20st_last_read_value);
  }
  { // Node ID: 24 (NodeInput)
    if(((needsToReadInput(m_siRho))&(((getFlushLevel())<((4l)+(5)))|(!(isFlushingActive()))))) {
      (id24st_last_read_value) = (readInput<HWFloat<8,24> >(m_siRho));
    }
    id24out_data = (id24st_last_read_value);
  }
  { // Node ID: 28 (NodeInput)
    if(((needsToReadInput(m_soRho))&(((getFlushLevel())<((4l)+(5)))|(!(isFlushingActive()))))) {
      (id28st_last_read_value) = (readInput<HWFloat<8,24> >(m_soRho));
    }
    id28out_data = (id28st_last_read_value);
  }
}

void secondKernel::runComputationCycle() {
  if (m_mappedElementsChanged) {
    m_mappedElementsChanged = false;
    updateState();
    std::cout << "secondKernel: Mapped Elements Changed: Reloaded" << std::endl;
  }
  preExecute();
  execute0();
}

int secondKernel::getFlushLevelStart() {
  return ((1l)+(3l));
}

}
