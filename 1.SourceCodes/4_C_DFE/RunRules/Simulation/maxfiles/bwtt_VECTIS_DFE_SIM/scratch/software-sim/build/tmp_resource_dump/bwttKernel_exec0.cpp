#include "stdsimheader.h"
//#define BOOST_NO_STD_LOCALE
//#include <boost/format.hpp>

//#include "bwttKernel.h"

namespace maxcompilersim {

void bwttKernel::execute0() {
  { // Node ID: 23 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id24out_result;

  { // Node ID: 24 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id24in_a = id23out_io_px_force_disabled;

    id24out_result = (not_fixed(id24in_a));
  }
  { // Node ID: 3 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id4out_result;

  { // Node ID: 4 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id4in_a = id3out_io_so_force_disabled;

    id4out_result = (not_fixed(id4in_a));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 5 (NodeInput)
    const HWOffsetFix<1,0,UNSIGNED> &id5in_enable = id4out_result;

    (id5st_read_next_cycle) = ((id5in_enable.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    queueReadRequest(m_so, id5st_read_next_cycle.getValueAsBool());
  }
  HWOffsetFix<10,0,UNSIGNED> id6out_o;

  { // Node ID: 6 (NodeCast)
    const HWOffsetFix<32,0,TWOSCOMPLEMENT> &id6in_i = id5out_data;

    id6out_o = (cast_fixed2fixed<10,0,UNSIGNED,TONEAR>(id6in_i));
  }
  { // Node ID: 0 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id1out_result;

  { // Node ID: 1 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id1in_a = id0out_io_si_force_disabled;

    id1out_result = (not_fixed(id1in_a));
  }
  if ( (getFillLevel() >= (4l)))
  { // Node ID: 2 (NodeInput)
    const HWOffsetFix<1,0,UNSIGNED> &id2in_enable = id1out_result;

    (id2st_read_next_cycle) = ((id2in_enable.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    queueReadRequest(m_si, id2st_read_next_cycle.getValueAsBool());
  }
  HWOffsetFix<10,0,UNSIGNED> id9out_o;

  { // Node ID: 9 (NodeCast)
    const HWOffsetFix<32,0,TWOSCOMPLEMENT> &id9in_i = id2out_data;

    id9out_o = (cast_fixed2fixed<10,0,UNSIGNED,TONEAR>(id9in_i));
  }
  { // Node ID: 52 (NodeMappedRom)
    const HWOffsetFix<10,0,UNSIGNED> &id52in_addra = id6out_o;
    const HWOffsetFix<10,0,UNSIGNED> &id52in_addrb = id9out_o;

    long id52x_1;
    HWOffsetFix<32,0,TWOSCOMPLEMENT> id52x_2;
    long id52x_3;
    HWOffsetFix<32,0,TWOSCOMPLEMENT> id52x_4;

    (id52x_1) = (id52in_addra.getValueAsLong());
    switch(((long)((id52x_1)<(1000l)))) {
      case 0l:
        id52x_2 = (c_hw_fix_32_0_sgn_undef);
        break;
      case 1l:
        id52x_2 = (getMappedMemValue< HWOffsetFix<32,0,TWOSCOMPLEMENT> > ("mappedRomX", id52x_1) );
        break;
      default:
        id52x_2 = (c_hw_fix_32_0_sgn_undef);
        break;
    }
    id52out_dataa[(getCycle()+2)%3] = (id52x_2);
    (id52x_3) = (id52in_addrb.getValueAsLong());
    switch(((long)((id52x_3)<(1000l)))) {
      case 0l:
        id52x_4 = (c_hw_fix_32_0_sgn_undef);
        break;
      case 1l:
        id52x_4 = (getMappedMemValue< HWOffsetFix<32,0,TWOSCOMPLEMENT> > ("mappedRomX", id52x_3) );
        break;
      default:
        id52x_4 = (c_hw_fix_32_0_sgn_undef);
        break;
    }
    id52out_datab[(getCycle()+2)%3] = (id52x_4);
  }
  { // Node ID: 10 (NodeCast)
    const HWOffsetFix<32,0,TWOSCOMPLEMENT> &id10in_i = id52out_datab[getCycle()%3];

    id10out_o[(getCycle()+6)%7] = (cast_fixed2float<8,24>(id10in_i));
  }
  { // Node ID: 7 (NodeCast)
    const HWOffsetFix<32,0,TWOSCOMPLEMENT> &id7in_i = id52out_dataa[getCycle()%3];

    id7out_o[(getCycle()+6)%7] = (cast_fixed2float<8,24>(id7in_i));
  }
  { // Node ID: 12 (NodeSub)
    const HWFloat<8,24> &id12in_a = id10out_o[getCycle()%7];
    const HWFloat<8,24> &id12in_b = id7out_o[getCycle()%7];

    id12out_result[(getCycle()+12)%13] = (sub_float(id12in_a,id12in_b));
  }
  { // Node ID: 67 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id67in_input = id6out_o;

    id67out_output[(getCycle()+6)%7] = id67in_input;
  }
  { // Node ID: 68 (NodeFIFO)
    const HWOffsetFix<10,0,UNSIGNED> &id68in_input = id9out_o;

    id68out_output[(getCycle()+6)%7] = id68in_input;
  }
  { // Node ID: 54 (NodeMappedRom)
    const HWOffsetFix<10,0,UNSIGNED> &id54in_addra = id67out_output[getCycle()%7];
    const HWOffsetFix<10,0,UNSIGNED> &id54in_addrb = id68out_output[getCycle()%7];

    long id54x_1;
    HWFloat<8,24> id54x_2;
    long id54x_3;
    HWFloat<8,24> id54x_4;

    (id54x_1) = (id54in_addra.getValueAsLong());
    switch(((long)((id54x_1)<(1000l)))) {
      case 0l:
        id54x_2 = (c_hw_flt_8_24_undef);
        break;
      case 1l:
        id54x_2 = (getMappedMemValue< HWFloat<8,24> > ("mappedRomRho", id54x_1) );
        break;
      default:
        id54x_2 = (c_hw_flt_8_24_undef);
        break;
    }
    id54out_dataa[(getCycle()+2)%3] = (id54x_2);
    (id54x_3) = (id54in_addrb.getValueAsLong());
    switch(((long)((id54x_3)<(1000l)))) {
      case 0l:
        id54x_4 = (c_hw_flt_8_24_undef);
        break;
      case 1l:
        id54x_4 = (getMappedMemValue< HWFloat<8,24> > ("mappedRomRho", id54x_3) );
        break;
      default:
        id54x_4 = (c_hw_flt_8_24_undef);
        break;
    }
    id54out_datab[(getCycle()+2)%3] = (id54x_4);
  }
  { // Node ID: 13 (NodeSub)
    const HWFloat<8,24> &id13in_a = id54out_datab[getCycle()%3];
    const HWFloat<8,24> &id13in_b = id54out_dataa[getCycle()%3];

    id13out_result[(getCycle()+12)%13] = (sub_float(id13in_a,id13in_b));
  }
  { // Node ID: 14 (NodeDiv)
    const HWFloat<8,24> &id14in_a = id12out_result[getCycle()%13];
    const HWFloat<8,24> &id14in_b = id13out_result[getCycle()%13];

    id14out_result[(getCycle()+28)%29] = (div_float(id14in_a,id14in_b));
  }
  if ( (getFillLevel() >= (57l)) && (getFlushLevel() < (57l)|| !isFlushingActive() ))
  { // Node ID: 26 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id26in_output_control = id24out_result;
    const HWFloat<8,24> &id26in_data = id14out_result[getCycle()%29];

    bool id26x_1;

    (id26x_1) = ((id26in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(57l))&(isFlushingActive()))));
    if((id26x_1)) {
      writeOutput(m_px, id26in_data);
    }
  }
  { // Node ID: 28 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id29out_result;

  { // Node ID: 29 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id29in_a = id28out_io_bx_force_disabled;

    id29out_result = (not_fixed(id29in_a));
  }
  { // Node ID: 70 (NodeFIFO)
    const HWFloat<8,24> &id70in_input = id7out_o[getCycle()%7];

    id70out_output[(getCycle()+48)%49] = id70in_input;
  }
  { // Node ID: 69 (NodeFIFO)
    const HWFloat<8,24> &id69in_input = id54out_dataa[getCycle()%3];

    id69out_output[(getCycle()+40)%41] = id69in_input;
  }
  { // Node ID: 15 (NodeMul)
    const HWFloat<8,24> &id15in_a = id14out_result[getCycle()%29];
    const HWFloat<8,24> &id15in_b = id69out_output[getCycle()%41];

    id15out_result[(getCycle()+8)%9] = (mul_float(id15in_a,id15in_b));
  }
  { // Node ID: 16 (NodeSub)
    const HWFloat<8,24> &id16in_a = id70out_output[getCycle()%49];
    const HWFloat<8,24> &id16in_b = id15out_result[getCycle()%9];

    id16out_result[(getCycle()+12)%13] = (sub_float(id16in_a,id16in_b));
  }
  if ( (getFillLevel() >= (77l)) && (getFlushLevel() < (77l)|| !isFlushingActive() ))
  { // Node ID: 31 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id31in_output_control = id29out_result;
    const HWFloat<8,24> &id31in_data = id16out_result[getCycle()%13];

    bool id31x_1;

    (id31x_1) = ((id31in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(77l))&(isFlushingActive()))));
    if((id31x_1)) {
      writeOutput(m_bx, id31in_data);
    }
  }
  { // Node ID: 33 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id34out_result;

  { // Node ID: 34 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id34in_a = id33out_io_py_force_disabled;

    id34out_result = (not_fixed(id34in_a));
  }
  { // Node ID: 53 (NodeMappedRom)
    const HWOffsetFix<10,0,UNSIGNED> &id53in_addra = id6out_o;
    const HWOffsetFix<10,0,UNSIGNED> &id53in_addrb = id9out_o;

    long id53x_1;
    HWOffsetFix<32,0,TWOSCOMPLEMENT> id53x_2;
    long id53x_3;
    HWOffsetFix<32,0,TWOSCOMPLEMENT> id53x_4;

    (id53x_1) = (id53in_addra.getValueAsLong());
    switch(((long)((id53x_1)<(1000l)))) {
      case 0l:
        id53x_2 = (c_hw_fix_32_0_sgn_undef);
        break;
      case 1l:
        id53x_2 = (getMappedMemValue< HWOffsetFix<32,0,TWOSCOMPLEMENT> > ("mappedRomY", id53x_1) );
        break;
      default:
        id53x_2 = (c_hw_fix_32_0_sgn_undef);
        break;
    }
    id53out_dataa[(getCycle()+2)%3] = (id53x_2);
    (id53x_3) = (id53in_addrb.getValueAsLong());
    switch(((long)((id53x_3)<(1000l)))) {
      case 0l:
        id53x_4 = (c_hw_fix_32_0_sgn_undef);
        break;
      case 1l:
        id53x_4 = (getMappedMemValue< HWOffsetFix<32,0,TWOSCOMPLEMENT> > ("mappedRomY", id53x_3) );
        break;
      default:
        id53x_4 = (c_hw_fix_32_0_sgn_undef);
        break;
    }
    id53out_datab[(getCycle()+2)%3] = (id53x_4);
  }
  { // Node ID: 11 (NodeCast)
    const HWOffsetFix<32,0,TWOSCOMPLEMENT> &id11in_i = id53out_datab[getCycle()%3];

    id11out_o[(getCycle()+6)%7] = (cast_fixed2float<8,24>(id11in_i));
  }
  { // Node ID: 8 (NodeCast)
    const HWOffsetFix<32,0,TWOSCOMPLEMENT> &id8in_i = id53out_dataa[getCycle()%3];

    id8out_o[(getCycle()+6)%7] = (cast_fixed2float<8,24>(id8in_i));
  }
  { // Node ID: 17 (NodeSub)
    const HWFloat<8,24> &id17in_a = id11out_o[getCycle()%7];
    const HWFloat<8,24> &id17in_b = id8out_o[getCycle()%7];

    id17out_result[(getCycle()+12)%13] = (sub_float(id17in_a,id17in_b));
  }
  { // Node ID: 19 (NodeDiv)
    const HWFloat<8,24> &id19in_a = id17out_result[getCycle()%13];
    const HWFloat<8,24> &id19in_b = id13out_result[getCycle()%13];

    id19out_result[(getCycle()+28)%29] = (div_float(id19in_a,id19in_b));
  }
  if ( (getFillLevel() >= (57l)) && (getFlushLevel() < (57l)|| !isFlushingActive() ))
  { // Node ID: 36 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id36in_output_control = id34out_result;
    const HWFloat<8,24> &id36in_data = id19out_result[getCycle()%29];

    bool id36x_1;

    (id36x_1) = ((id36in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(57l))&(isFlushingActive()))));
    if((id36x_1)) {
      writeOutput(m_py, id36in_data);
    }
  }
  { // Node ID: 38 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id39out_result;

  { // Node ID: 39 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id39in_a = id38out_io_by_force_disabled;

    id39out_result = (not_fixed(id39in_a));
  }
  { // Node ID: 72 (NodeFIFO)
    const HWFloat<8,24> &id72in_input = id8out_o[getCycle()%7];

    id72out_output[(getCycle()+48)%49] = id72in_input;
  }
  { // Node ID: 20 (NodeMul)
    const HWFloat<8,24> &id20in_a = id19out_result[getCycle()%29];
    const HWFloat<8,24> &id20in_b = id69out_output[getCycle()%41];

    id20out_result[(getCycle()+8)%9] = (mul_float(id20in_a,id20in_b));
  }
  { // Node ID: 21 (NodeSub)
    const HWFloat<8,24> &id21in_a = id72out_output[getCycle()%49];
    const HWFloat<8,24> &id21in_b = id20out_result[getCycle()%9];

    id21out_result[(getCycle()+12)%13] = (sub_float(id21in_a,id21in_b));
  }
  if ( (getFillLevel() >= (77l)) && (getFlushLevel() < (77l)|| !isFlushingActive() ))
  { // Node ID: 41 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id41in_output_control = id39out_result;
    const HWFloat<8,24> &id41in_data = id21out_result[getCycle()%13];

    bool id41x_1;

    (id41x_1) = ((id41in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(77l))&(isFlushingActive()))));
    if((id41x_1)) {
      writeOutput(m_by, id41in_data);
    }
  }
  { // Node ID: 43 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id44out_result;

  { // Node ID: 44 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id44in_a = id43out_io_siRho_force_disabled;

    id44out_result = (not_fixed(id44in_a));
  }
  if ( (getFillLevel() >= (17l)) && (getFlushLevel() < (17l)|| !isFlushingActive() ))
  { // Node ID: 46 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id46in_output_control = id44out_result;
    const HWFloat<8,24> &id46in_data = id54out_datab[getCycle()%3];

    bool id46x_1;

    (id46x_1) = ((id46in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(17l))&(isFlushingActive()))));
    if((id46x_1)) {
      writeOutput(m_siRho, id46in_data);
    }
  }
  { // Node ID: 48 (NodeInputMappedReg)
  }
  HWOffsetFix<1,0,UNSIGNED> id49out_result;

  { // Node ID: 49 (NodeNot)
    const HWOffsetFix<1,0,UNSIGNED> &id49in_a = id48out_io_soRho_force_disabled;

    id49out_result = (not_fixed(id49in_a));
  }
  if ( (getFillLevel() >= (57l)) && (getFlushLevel() < (57l)|| !isFlushingActive() ))
  { // Node ID: 51 (NodeOutput)
    const HWOffsetFix<1,0,UNSIGNED> &id51in_output_control = id49out_result;
    const HWFloat<8,24> &id51in_data = id69out_output[getCycle()%41];

    bool id51x_1;

    (id51x_1) = ((id51in_output_control.getValueAsBool())&(!(((getFlushLevel())>=(57l))&(isFlushingActive()))));
    if((id51x_1)) {
      writeOutput(m_soRho, id51in_data);
    }
  }
  { // Node ID: 59 (NodeConstantRawBits)
  }
  { // Node ID: 75 (NodeConstantRawBits)
  }
  { // Node ID: 56 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (3l)))
  { // Node ID: 57 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id57in_enable = id75out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id57in_max = id56out_value;

    HWOffsetFix<49,0,UNSIGNED> id57x_1;
    HWOffsetFix<1,0,UNSIGNED> id57x_2;
    HWOffsetFix<1,0,UNSIGNED> id57x_3;
    HWOffsetFix<49,0,UNSIGNED> id57x_4t_1e_1;

    id57out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id57st_count)));
    (id57x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id57st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id57x_2) = (gte_fixed((id57x_1),id57in_max));
    (id57x_3) = (and_fixed((id57x_2),id57in_enable));
    id57out_wrap = (id57x_3);
    if((id57in_enable.getValueAsBool())) {
      if(((id57x_3).getValueAsBool())) {
        (id57st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id57x_4t_1e_1) = (id57x_1);
        (id57st_count) = (id57x_4t_1e_1);
      }
    }
    else {
    }
  }
  HWOffsetFix<48,0,UNSIGNED> id58out_output;

  { // Node ID: 58 (NodeStreamOffset)
    const HWOffsetFix<48,0,UNSIGNED> &id58in_input = id57out_count;

    id58out_output = id58in_input;
  }
  if ( (getFillLevel() >= (4l)) && (getFlushLevel() < (4l)|| !isFlushingActive() ))
  { // Node ID: 60 (NodeOutputMappedReg)
    const HWOffsetFix<1,0,UNSIGNED> &id60in_load = id59out_value;
    const HWOffsetFix<48,0,UNSIGNED> &id60in_data = id58out_output;

    bool id60x_1;

    (id60x_1) = ((id60in_load.getValueAsBool())&(!(((getFlushLevel())>=(4l))&(isFlushingActive()))));
    if((id60x_1)) {
      setMappedRegValue("current_run_cycle_count", id60in_data);
    }
  }
  { // Node ID: 74 (NodeConstantRawBits)
  }
  { // Node ID: 62 (NodeConstantRawBits)
  }
  if ( (getFillLevel() >= (0l)))
  { // Node ID: 63 (NodeCounterV1)
    const HWOffsetFix<1,0,UNSIGNED> &id63in_enable = id74out_value;
    const HWOffsetFix<49,0,UNSIGNED> &id63in_max = id62out_value;

    HWOffsetFix<49,0,UNSIGNED> id63x_1;
    HWOffsetFix<1,0,UNSIGNED> id63x_2;
    HWOffsetFix<1,0,UNSIGNED> id63x_3;
    HWOffsetFix<49,0,UNSIGNED> id63x_4t_1e_1;

    id63out_count = (cast_fixed2fixed<48,0,UNSIGNED,TRUNCATE>((id63st_count)));
    (id63x_1) = (add_fixed<49,0,UNSIGNED,TRUNCATE>((id63st_count),(c_hw_fix_49_0_uns_bits_2)));
    (id63x_2) = (gte_fixed((id63x_1),id63in_max));
    (id63x_3) = (and_fixed((id63x_2),id63in_enable));
    id63out_wrap = (id63x_3);
    if((id63in_enable.getValueAsBool())) {
      if(((id63x_3).getValueAsBool())) {
        (id63st_count) = (c_hw_fix_49_0_uns_bits_1);
      }
      else {
        (id63x_4t_1e_1) = (id63x_1);
        (id63st_count) = (id63x_4t_1e_1);
      }
    }
    else {
    }
  }
  { // Node ID: 65 (NodeInputMappedReg)
  }
  { // Node ID: 66 (NodeEq)
    const HWOffsetFix<48,0,UNSIGNED> &id66in_a = id63out_count;
    const HWOffsetFix<48,0,UNSIGNED> &id66in_b = id65out_run_cycle_count;

    id66out_result[(getCycle()+1)%2] = (eq_fixed(id66in_a,id66in_b));
  }
  if ( (getFillLevel() >= (1l)))
  { // Node ID: 64 (NodeFlush)
    const HWOffsetFix<1,0,UNSIGNED> &id64in_start = id66out_result[getCycle()%2];

    if((id64in_start.getValueAsBool())) {
      startFlushing();
    }
  }
};

};
