/**\file */
#ifndef SLIC_DECLARATIONS_bwtt_H
#define SLIC_DECLARATIONS_bwtt_H
#include "MaxSLiCInterface.h"
#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

#define bwtt_PCIE_ALIGNMENT (16)


/*----------------------------------------------------------------------------*/
/*---------------------------- Interface default -----------------------------*/
/*----------------------------------------------------------------------------*/




/**
 * \brief Basic static function for the interface 'default'.
 * 
 * \param [in] param_sinkCount Interface Parameter "sinkCount".
 * \param [in] param_sourceCount Interface Parameter "sourceCount".
 * \param [in] param_tokensIndex Interface Parameter "tokensIndex".
 * \param [in] instream_si The stream should be of size ((param_sourceCount * param_sinkCount) * 4) bytes.
 * \param [in] instream_so The stream should be of size ((param_sourceCount * param_sinkCount) * 4) bytes.
 * \param [out] outstream_output The stream should be of size (((param_sinkCount * param_sourceCount) * param_tokensIndex) * 2) bytes.
 * \param [in] inmem_bwttKernel_mappedRomRho Mapped ROM inmem_bwttKernel_mappedRomRho, should be of size (1000 * sizeof(double)).
 * \param [in] inmem_bwttKernel_mappedRomX Mapped ROM inmem_bwttKernel_mappedRomX, should be of size (1000 * sizeof(uint64_t)).
 * \param [in] inmem_bwttKernel_mappedRomY Mapped ROM inmem_bwttKernel_mappedRomY, should be of size (1000 * sizeof(uint64_t)).
 * \param [in] inmem_secondKernel_mappedRomRho Mapped ROM inmem_secondKernel_mappedRomRho, should be of size (1000 * sizeof(double)).
 * \param [in] inmem_secondKernel_mappedRomX Mapped ROM inmem_secondKernel_mappedRomX, should be of size (1000 * sizeof(uint64_t)).
 * \param [in] inmem_secondKernel_mappedRomY Mapped ROM inmem_secondKernel_mappedRomY, should be of size (1000 * sizeof(uint64_t)).
 */
void bwtt(
	int64_t param_sinkCount,
	int64_t param_sourceCount,
	int64_t param_tokensIndex,
	const uint32_t *instream_si,
	const uint32_t *instream_so,
	uint16_t *outstream_output,
	const double *inmem_bwttKernel_mappedRomRho,
	const uint64_t *inmem_bwttKernel_mappedRomX,
	const uint64_t *inmem_bwttKernel_mappedRomY,
	const double *inmem_secondKernel_mappedRomRho,
	const uint64_t *inmem_secondKernel_mappedRomX,
	const uint64_t *inmem_secondKernel_mappedRomY);

/**
 * \brief Basic static non-blocking function for the interface 'default'.
 * 
 * Schedule to run on an engine and return immediately.
 * The status of the run can be checked either by ::max_wait or ::max_nowait;
 * note that one of these *must* be called, so that associated memory can be released.
 * 
 * 
 * \param [in] param_sinkCount Interface Parameter "sinkCount".
 * \param [in] param_sourceCount Interface Parameter "sourceCount".
 * \param [in] param_tokensIndex Interface Parameter "tokensIndex".
 * \param [in] instream_si The stream should be of size ((param_sourceCount * param_sinkCount) * 4) bytes.
 * \param [in] instream_so The stream should be of size ((param_sourceCount * param_sinkCount) * 4) bytes.
 * \param [out] outstream_output The stream should be of size (((param_sinkCount * param_sourceCount) * param_tokensIndex) * 2) bytes.
 * \param [in] inmem_bwttKernel_mappedRomRho Mapped ROM inmem_bwttKernel_mappedRomRho, should be of size (1000 * sizeof(double)).
 * \param [in] inmem_bwttKernel_mappedRomX Mapped ROM inmem_bwttKernel_mappedRomX, should be of size (1000 * sizeof(uint64_t)).
 * \param [in] inmem_bwttKernel_mappedRomY Mapped ROM inmem_bwttKernel_mappedRomY, should be of size (1000 * sizeof(uint64_t)).
 * \param [in] inmem_secondKernel_mappedRomRho Mapped ROM inmem_secondKernel_mappedRomRho, should be of size (1000 * sizeof(double)).
 * \param [in] inmem_secondKernel_mappedRomX Mapped ROM inmem_secondKernel_mappedRomX, should be of size (1000 * sizeof(uint64_t)).
 * \param [in] inmem_secondKernel_mappedRomY Mapped ROM inmem_secondKernel_mappedRomY, should be of size (1000 * sizeof(uint64_t)).
 * \return A handle on the execution status, or NULL in case of error.
 */
max_run_t *bwtt_nonblock(
	int64_t param_sinkCount,
	int64_t param_sourceCount,
	int64_t param_tokensIndex,
	const uint32_t *instream_si,
	const uint32_t *instream_so,
	uint16_t *outstream_output,
	const double *inmem_bwttKernel_mappedRomRho,
	const uint64_t *inmem_bwttKernel_mappedRomX,
	const uint64_t *inmem_bwttKernel_mappedRomY,
	const double *inmem_secondKernel_mappedRomRho,
	const uint64_t *inmem_secondKernel_mappedRomX,
	const uint64_t *inmem_secondKernel_mappedRomY);

/**
 * \brief Advanced static interface, structure for the engine interface 'default'
 * 
 */
typedef struct { 
	int64_t param_sinkCount; /**<  [in] Interface Parameter "sinkCount". */
	int64_t param_sourceCount; /**<  [in] Interface Parameter "sourceCount". */
	int64_t param_tokensIndex; /**<  [in] Interface Parameter "tokensIndex". */
	const uint32_t *instream_si; /**<  [in] The stream should be of size ((param_sourceCount * param_sinkCount) * 4) bytes. */
	const uint32_t *instream_so; /**<  [in] The stream should be of size ((param_sourceCount * param_sinkCount) * 4) bytes. */
	uint16_t *outstream_output; /**<  [out] The stream should be of size (((param_sinkCount * param_sourceCount) * param_tokensIndex) * 2) bytes. */
	const double *inmem_bwttKernel_mappedRomRho; /**<  [in] Mapped ROM inmem_bwttKernel_mappedRomRho, should be of size (1000 * sizeof(double)). */
	const uint64_t *inmem_bwttKernel_mappedRomX; /**<  [in] Mapped ROM inmem_bwttKernel_mappedRomX, should be of size (1000 * sizeof(uint64_t)). */
	const uint64_t *inmem_bwttKernel_mappedRomY; /**<  [in] Mapped ROM inmem_bwttKernel_mappedRomY, should be of size (1000 * sizeof(uint64_t)). */
	const double *inmem_secondKernel_mappedRomRho; /**<  [in] Mapped ROM inmem_secondKernel_mappedRomRho, should be of size (1000 * sizeof(double)). */
	const uint64_t *inmem_secondKernel_mappedRomX; /**<  [in] Mapped ROM inmem_secondKernel_mappedRomX, should be of size (1000 * sizeof(uint64_t)). */
	const uint64_t *inmem_secondKernel_mappedRomY; /**<  [in] Mapped ROM inmem_secondKernel_mappedRomY, should be of size (1000 * sizeof(uint64_t)). */
} bwtt_actions_t;

/**
 * \brief Advanced static function for the interface 'default'.
 * 
 * \param [in] engine The engine on which the actions will be executed.
 * \param [in,out] interface_actions Actions to be executed.
 */
void bwtt_run(
	max_engine_t *engine,
	bwtt_actions_t *interface_actions);

/**
 * \brief Advanced static non-blocking function for the interface 'default'.
 *
 * Schedule the actions to run on the engine and return immediately.
 * The status of the run can be checked either by ::max_wait or ::max_nowait;
 * note that one of these *must* be called, so that associated memory can be released.
 *
 * 
 * \param [in] engine The engine on which the actions will be executed.
 * \param [in] interface_actions Actions to be executed.
 * \return A handle on the execution status of the actions, or NULL in case of error.
 */
max_run_t *bwtt_run_nonblock(
	max_engine_t *engine,
	bwtt_actions_t *interface_actions);

/**
 * \brief Group run advanced static function for the interface 'default'.
 * 
 * \param [in] group Group to use.
 * \param [in,out] interface_actions Actions to run.
 *
 * Run the actions on the first device available in the group.
 */
void bwtt_run_group(max_group_t *group, bwtt_actions_t *interface_actions);

/**
 * \brief Group run advanced static non-blocking function for the interface 'default'.
 * 
 *
 * Schedule the actions to run on the first device available in the group and return immediately.
 * The status of the run must be checked with ::max_wait. 
 * Note that use of ::max_nowait is prohibited with non-blocking running on groups:
 * see the ::max_run_group_nonblock documentation for more explanation.
 *
 * \param [in] group Group to use.
 * \param [in] interface_actions Actions to run.
 * \return A handle on the execution status of the actions, or NULL in case of error.
 */
max_run_t *bwtt_run_group_nonblock(max_group_t *group, bwtt_actions_t *interface_actions);

/**
 * \brief Array run advanced static function for the interface 'default'.
 * 
 * \param [in] engarray The array of devices to use.
 * \param [in,out] interface_actions The array of actions to run.
 *
 * Run the array of actions on the array of engines.  The length of interface_actions
 * must match the size of engarray.
 */
void bwtt_run_array(max_engarray_t *engarray, bwtt_actions_t *interface_actions[]);

/**
 * \brief Array run advanced static non-blocking function for the interface 'default'.
 * 
 *
 * Schedule to run the array of actions on the array of engines, and return immediately.
 * The length of interface_actions must match the size of engarray.
 * The status of the run can be checked either by ::max_wait or ::max_nowait;
 * note that one of these *must* be called, so that associated memory can be released.
 *
 * \param [in] engarray The array of devices to use.
 * \param [in] interface_actions The array of actions to run.
 * \return A handle on the execution status of the actions, or NULL in case of error.
 */
max_run_t *bwtt_run_array_nonblock(max_engarray_t *engarray, bwtt_actions_t *interface_actions[]);

/**
 * \brief Converts a static-interface action struct into a dynamic-interface max_actions_t struct.
 *
 * Note that this is an internal utility function used by other functions in the static interface.
 *
 * \param [in] maxfile The maxfile to use.
 * \param [in] interface_actions The interface-specific actions to run.
 * \return The dynamic-interface actions to run, or NULL in case of error.
 */
max_actions_t* bwtt_convert(max_file_t *maxfile, bwtt_actions_t *interface_actions);

/**
 * \brief Initialise a maxfile.
 */
max_file_t* bwtt_init(void);

/* Error handling functions */
int bwtt_has_errors(void);
const char* bwtt_get_errors(void);
void bwtt_clear_errors(void);
/* Free statically allocated maxfile data */
void bwtt_free(void);
/* returns: -1 = error running command; 0 = no error reported */
int bwtt_simulator_start(void);
/* returns: -1 = error running command; 0 = no error reported */
int bwtt_simulator_stop(void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* SLIC_DECLARATIONS_bwtt_H */

