/**
* @file sdGeneric.h
* @Funtions for Calculating Snout Data
* @author Yang Ma, Erasmus MC
*/

/*
% This function attempts to extract whiskers and snout outline without bgSubtraction
% Inputs:
%           -GRAYshape is the gray scale image of the buffer
%           -bwThreshold selects the inital threshold for whisker
%           detection
%           -bwMorphology is the threshold for setting the importance of
%           detected features, usually 5 / 255
% -razorSize measures the width of selected shaving features and
%           increases the width of snout
% Outputs:
%           -bwBoundary is the countour of the environment
*/


#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>

int sdGeneric(IplImage *greyFrame, int *boundaryX, int *boundaryY);
void stExtractEnvGrayValuesWithMorphology(IplImage *greyFrame, double bwThreshold, IplConvKernel *erodeDisk);
int bwBoundary(IplImage *binaryFrame, int *boundaryX, int *boundaryY);

int kernel_data2[25] = {	0, 0, 1, 0, 0,
							0, 1, 1, 1, 0,
							1, 1, 1, 1, 1,
							0, 1, 1, 1, 0,
							0, 0, 1, 0, 0 };

int kernel_data4[49] = {	0, 0, 1, 1, 1, 0, 0,
							0, 1, 1, 1, 1, 1, 0,
							1, 1, 1, 1, 1, 1, 1,
							1, 1, 1, 1, 1, 1, 1,
							1, 1, 1, 1, 1, 1, 1,
							0, 1, 1, 1, 1, 1, 0,
							0, 0, 1, 1, 1, 0, 0 };

//IplConvKernel *disk4 = cvCreateStructuringElementEx(7, 7, 0, 0, *kernel_data4);

int kernel_data5[81] = {	0, 0, 1, 1, 1, 1, 1, 0, 0,
							0, 1, 1, 1, 1, 1, 1, 1, 0,
							1, 1, 1, 1, 1, 1, 1, 1, 1,
							1, 1, 1, 1, 1, 1, 1, 1, 1,
							1, 1, 1, 1, 1, 1, 1, 1, 1,
							1, 1, 1, 1, 1, 1, 1, 1, 1,
							1, 1, 1, 1, 1, 1, 1, 1, 1,
							0, 1, 1, 1, 1, 1, 1, 1, 0,
							0, 0, 1, 1, 1, 1, 1, 0, 0 };


int kernel_data8[225] = {	0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0,
							0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
							0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
							0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
							1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
							1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
							1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
							1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
							1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
							1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
							1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
							0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0,
							0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0,
							0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0,
							0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0 };

//IplConvKernel *disk8 = cvCreateStructuringElementEx(15, 15, 0, 0, *kernel_data8);


int sdGeneric(IplImage *greyFrame, int *boundaryX, int *boundaryY) {
	// Modify to read from user file later!
	double bwThreshold = 0.3;

	IplConvKernel *erodeDisk = cvCreateStructuringElementEx(9, 9, 0, 0, CV_SHAPE_CUSTOM, kernel_data5);
	stExtractEnvGrayValuesWithMorphology(greyFrame, bwThreshold, erodeDisk);

	int boundaryCount = bwBoundary(greyFrame, boundaryX, boundaryY);

	return boundaryCount;
}

void stExtractEnvGrayValuesWithMorphology(IplImage *greyFrame, double bwThreshold, IplConvKernel *erodeDisk) {

	IplConvKernel *disk2 = cvCreateStructuringElementEx(5, 5, 0, 0, CV_SHAPE_CUSTOM, kernel_data2);

	// Transfer the grey image to binary image
	cvThreshold(greyFrame, greyFrame, bwThreshold * 255, 1,CV_THRESH_BINARY);
	// Dilating using disk 2 kernel
	cvDilate(greyFrame, greyFrame, disk2, 1);
	//Enrode using disk 5 kernel
	cvErode(greyFrame, greyFrame, erodeDisk, 1);

}

int bwBoundary(IplImage *binaryFrame, int *boundaryX, int *boundaryY) {

	int rows = binaryFrame->height;
	int cols = binaryFrame->width;

	uchar *frameData;
	frameData = (uchar *)binaryFrame->imageData;

	int index = 0;
	int lastLine = (rows - 1)*cols;

	// Check the image horizontal boarder cases
	for (int i = 0; i < cols; i++) {
		if (frameData[i] == 1) {
			boundaryX[index] = i;
			boundaryY[index] = 0;
			index++;
		}
		if (frameData[i + lastLine] == 1) {
			boundaryX[index] = i;
			boundaryY[index] = rows - 1;
			index++;
		}
	}

	// Check the image vertical boarder cases
	for (int j = 0; j < rows; j++) {
		int temp = j*cols;
		if (frameData[temp] == 1) {
			boundaryX[index] = 0;
			boundaryY[index] = temp;
			index++;
		}
		if (frameData[temp + cols - 1] == 1) {
			boundaryX[index] = cols - 1;
			boundaryY[index] = temp;
			index++;
		}
	}

	// Iterate through rest of image
	for (int i = 1; i < rows - 1; i++) {
		for (int j = 1; j < cols - 1; j++) {

			if (frameData[i*cols + j] == 0)
				continue;

			int sum = 0;

			sum += frameData[i*cols + j - 1] + frameData[i*cols + j + 1] +
				frameData[(i - 1)*cols + j - 1] + frameData[(i - 1)*cols + j + 1] + frameData[(i - 1)*cols + j] +
				frameData[(i + 1)*cols + j - 1] + frameData[(i + 1)*cols + j + 1] + frameData[(i + 1)*cols + j];

			if (sum < 8) {
				boundaryX[index] = j;
				boundaryY[index] = i;
				index++;
			}
		}
	}

	return index;
}
