
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



function [GRAYwhiskersImage, bwShape] = ...
	stExtractWhiskersGrayValuesWithMorphologyAndShave( ...
	GRAYshape,bwThreshold, bwMorphology, razorSize, BWmask)

% Author: Perkon Igor    -    perkon@sissa.it

%<\inDOC>
% This function attempts to extract whiskers and snout outline without bgSubtraction
% Inputs:
%           - GRAYshape is the gray scale image of the buffer
%           - bwThreshold selects the inital threshold for whisker
%           detection
%           - bwMorphology is the threshold for setting the importance of
%           detected features, usually 5/255
%           - razorSize measures the width of selected shaving features and
%           increases the width of snout
% Outputs:
%           - GRAYwhiskersImage is the gray scale image of whiskers only
%           - bwShape is the BW shape of the snout, without whiskers
%
%<\outDOC>

%%
whiskerThreshold = 0.05;                           % threshold for segmentatin whiksers

% BWwhiskersMask first dilates the whiskers, than erodes in order to
% compensate
GRAYshapgpu = gpuArray(GRAYshape);
Binaryshapegpu = gpuArray(im2bw((GRAYshape),bwThreshold));
BWtemp = imdilate(Binaryshapegpu,strel('disk',2));
BWwhiskersMask = imerode(BWtemp ,strel('disk',4)); %10
BWwhiskersMask = imdilate(BWwhiskersMask  ,strel('disk',8)); %10

J = (imdilate(GRAYshapgpu,strel('disk',3))-GRAYshapgpu);

J = gather(J);

I = immultiply(J, im2bw(J, bwMorphology));


sortedIntensities = sort(max(I));


% MITCH: 08/01/2012
% the following code was here, and Goren was working with
% videos with a size smaller than 400 x 400, so it raised. i
% fixed it by guessing what the right thing to do is, Igor
% may want to review.
%
% ORIGINAL CODE (in sdGeneric):
% if (double(sortedIntensities(400))/255) <= 0
%     BWwhiskersImageDiff = I;
% else
%     BWwhiskersImageDiff = imadjust(I,[0 double(sortedIntensities(400))/255],[]);
% end;
adj = double(sortedIntensities(min(400, length(sortedIntensities)))) / 255;
if adj <= 0
    BWwhiskersImageDiff = I;
else
    BWwhiskersImageDiff = imadjust(I, [0 adj], []);
end;


% noise reduction process adopted by masking and
% thresholding objects
BinarywhiskersImageDiff = gpuArray(im2bw(BWwhiskersImageDiff, whiskerThreshold));
whiskersMask = imdilate(BinarywhiskersImageDiff,strel('disk',5));

BWwhiskersMask = gather(BWwhiskersMask);
whiskersMask = gather(whiskersMask);
BWmask2 = immultiply(BWwhiskersMask, whiskersMask);%bwareaopen(im2bw(BWwhiskersImageDiff, whiskerThreshold),10));
GRAYwhiskersImage = immultiply(BWwhiskersImageDiff, BWmask2);

%%
se = strel('disk',round(razorSize));
bwShape = imerode((BWtemp+imcomplement(BWmask))>0,se); %previouslyBWtemp
bwShape = gather(bwShape);
bwShape = immultiply(bwShape,BWmask);

clear GRAYshapgpu;
clear Binaryshapegpu;
clear BWtemp;
clear BWwhiskersMask;
clear BinarywhiskersImageDiff;





