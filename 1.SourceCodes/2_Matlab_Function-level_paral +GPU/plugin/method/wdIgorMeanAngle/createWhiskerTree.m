
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function whiskerTree = createWhiskerTree(snoutTemplatePP, potentialDensityArray, whiskersGrayImage, X, extractionMethod, method, param)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Function createWhiskerTree
%
% Inputs:   snoutTemplatePP, pp-form of snout
%           potentialDensityArray, density of lines to be scanned
%           whiskersGrayImage, the gray shape image of whiskers
%           X, is the current shape space position
%           extraction method, is the method used for building potential
%           lines
%           method, is the method used for scanning this equipotential
%           lines
%
% Outputs:  whiskerTree, is a cell array where the syntax is
%           whiskerTree{numberOfPotentialLine} = [x,y,c,n] matrix of max
%           coordinates, intesnity and normalized aproximated archlength,
%           calcaulated as index/numel(x), but if the method is
%           'ORIENTEDRADIALDISTANCE' then the output is [x,y,c,theta, rho]
%
% ExtractionMethod
%
% 1) 'EQUIPOTENTIALLINES'
%
% The function does the following operations: it creates a set of potential
% lines using the snoutTemplatePP and the potentialDensityArray. For each
% line it downsamples the line and creates a trace for improfile, which
% returns our interpolated profile density. All local maxima in these trace
% are extracted, with their coordinates and normalized approximated arch-
% lenght stored in the appropriate row of the whiskerTree cell-array.
%
% When the input shape space is 4-dimensional, the first two coordinates
% are subtracted and added in order to achieve affine compensation. We get
% centeredSnoutTemplate, which is our base scanning line.
%
% At the first scan, we sample the contour at a number of predefined function
% breaks.
% These points, representing a kind of linear polygon where improfile scans the
% image, constitute the approximation of the contour and are given by
% numberOfLinearSegments parameter.
%
% Due to phase error in successive allignements (expansion, contraction,
% etc) we found out that sometimes the arc-lenght value is too different
% for succesive frames and that whiksers at extrema are not well aprximated
%
%
% 2) ORIENTED DISTANCE
% The basic idea of this method is to use a circle approximation for
% whikser crossings. The circle is going to be centered on the shape space
% center and  offset is going to be calculated from the shape space
% data. In order to provide the idea of continuity, we take the GRAYimage
% and we convolve with an averaging filter in order to make a kind of
% interpolation and in order to select pixels surrounding whiskers. In this
% way we have also the "hole information". Than, in a dumb way we sort
% elements according to rho. Elements within a certain theta value are
% supposed to belong to the same potential. This threshold may be of course
% made dependent of the target distance, since pixels close to the snout
% will require a small threshold, while pixels away an other threshold.
% CHECK THE ABOVE IDEA! The so made data is scanned for local maxima.
% Once the elements are sorted according to rho, they are gruuped into
% rings of approximately the same rho. These rings are then sorted
% according to theta and the results is an imageprofile path that is
% scanned for tokens.
%
% This method has been furtherly modified to start with a fixed rho, in
% order to have image size independence and with a code used for connecting
% and averaging features that are lying together.
%
%
%-------------------------
% Additional speed improvement can be gained by creating an iterative
% improfile version, where only "productive" segments are searched and
% therefore not the all arch is scanned
%
%
% <\outDOC>

% 4.4.2010
if ~strcmp(class(whiskersGrayImage),'double')
    whiskersGrayImage = double(whiskersGrayImage);
    whiskersGrayImage = whiskersGrayImage./max(max(whiskersGrayImage));
end
whiskerTree = cell(1,numel(potentialDensityArray));

switch extractionMethod
    
    case 'EQUIPOTENTIALLINES',
        
        
        numberOfLinearSegments = 20;
        
        if nargin < 5
            method = 'maxOnly';
        end
        
        if (numel(X) == 4)
            centeredSnoutTemplate = fncmb(snoutTemplatePP,'-',[X(1), X(2)]);
        end
        
        
        for i=1:numel(potentialDensityArray),
            snoutTemplateWithPotential = (fncmb(fncmb(centeredSnoutTemplate, potentialDensityArray(i)),'+',[X(1), X(2)]));
            tSampling = 0:snoutTemplateWithPotential.breaks(end)/numberOfLinearSegments:snoutTemplateWithPotential.breaks(end);
            segmentCoordinates = fnval(snoutTemplateWithPotential, tSampling);
            [x,y,intensityProfile] = improfile(whiskersGrayImage, segmentCoordinates(1,:),segmentCoordinates(2,:), 'bicubic');
            
            tokens = findLocalTokens(intensityProfile,4,method);
            
            if strcmp(method,'maxOnly')
                idx = tokens(2,:);
                whiskerTree{i} = [x(idx), y(idx), intensityProfile(idx), (idx./numel(x))'];
            end
        end
        
    case 'DILATERINGS',
        whiskersSmoothedImage = imfilter(whiskersGrayImage, fspecial('average'));
        whiskersSmoothedMask = im2bw(whiskersSmoothedImage, param.whiskersSmoothingT);
        %figure(12), imshow(whiskersSmoothedMask)
        idx = (whiskersSmoothedMask);
        [x, y] = find(whiskersSmoothedMask);
        [theta, rho] = cart2pol(x - X(2), y - X(1));
        %[theta, rho] = cart2pol(x - X(2), y - 120);
        minimumRho = 30; % HARCODED in order to avoid data dependence from min(rho);
        whiskersFeatures = [rho, theta, double(whiskersGrayImage(idx)), x, y];
        [rhoSorted,ib] = sortrows(whiskersFeatures);
        
        
        %figure(3)
        %imshow(whiskersGrayImage)
        %hold on
        startingMask = whiskersGrayImage == 0;
        for i=1:numel(potentialDensityArray),
            DELTA = 1;
            startingMaskE = imdilate(startingMask,strel('square',3));
            ringDiff = (startingMask ~= startingMaskE);
            bwB = cell2mat(bwboundaries(ringDiff.*whiskersSmoothedMask));
            startingMask = startingMaskE;
            
            if numel(bwB),
                [c,ia,ib] = intersect(bwB, rhoSorted(:,[4 5]),'rows');
                rhoThetaSortedFeatures = rhoSorted(ib,:);
                
                
                
                
                %polar(rhoThetaSortedFeatures(:,2), rhoThetaSortedFeatures(:,1),'bx');
                %plot(rhoThetaSortedFeatures(:,5), rhoThetaSortedFeatures(:,4),'bx');
                
                intensityProfile = rhoThetaSortedFeatures(:,3);
                if isfield(param, 'rhoThresh')
                    tokens = findLocalTokens(intensityProfile,param.localTokensN,method,rhoThetaSortedFeatures(:,1),param.thresh1, param.thresh2);
                else
                    tokens = findLocalTokens(intensityProfile,param.localTokensN,method,rhoThetaSortedFeatures(:,1),param.thresh1, param.thresh2, param.rhoThresh);
                end
                if ~isempty(tokens)
                    if strcmp(method,'maxOnly')
                        idx = tokens(2,:);
                        % ORIGINAL UNGROUPED SET OF TOKENS
                        %whiskerTree{i} = [rhoThetaSortedFeatures(idx,5), rhoThetaSortedFeatures(idx,4), rhoThetaSortedFeatures(idx,3), rhoThetaSortedFeatures(idx,2)];
                        %plot(rhoThetaSortedFeatures(idx,5), rhoThetaSortedFeatures(idx,4),'mx');
                        whiskerTree{i} = groupConnectedTokens(idx, rhoThetaSortedFeatures);
                        
                        %plot(rhoThetaSortedFeatures(idx,5), rhoThetaSortedFeatures(idx,4),'gx');
                        %plot(whiskerTree{i}(:,2), whiskerTree{i}(:,1),'yo');
                    end
                end
            end
        end
        
        
    case 'ORIENTEDRADIALDISTANCE',
        whiskersGrayImageGpu = gpuArray(whiskersGrayImage);
        whiskersSmoothedImageGpu = imfilter(whiskersGrayImageGpu, fspecial('average'));
        whiskersSmoothedImage = gather(whiskersSmoothedImageGpu);
        
        whiskersSmoothedMask = im2bw(whiskersSmoothedImage, param.whiskersSmoothingT);
        %figure(12), imshow(whiskersSmoothedMask)
        idx = (whiskersSmoothedMask);
        [x, y] = find(whiskersSmoothedMask);
        [theta, rho] = cart2pol(x - X(2), y - X(1));
        %[theta, rho] = cart2pol(x - X(2), y - 120);
        minimumRho = 30; % HARCODED in order to avoid data dependence from min(rho);
        whiskersFeatures = [rho, theta, double(whiskersGrayImage(idx)), x, y];
        rhoSorted = sortrows(whiskersFeatures);
        
        
        
        if numel(potentialDensityArray) == 1,
            potentialDensityArray1 = 1:potentialDensityArray:max(rho);
            parfor i=1:numel(potentialDensityArray1),
                DELTA = 1;
                idx = (abs(rhoSorted(:,1) - (minimumRho + potentialDensityArray1(i))) < DELTA);
                if sum(idx)
                    extractedFeatures = rhoSorted(idx,:);
                    rhoThetaSortedFeatures = sortrows(extractedFeatures,2);
                    
                    
                    %polar(rhoThetaSortedFeatures(:,2), rhoThetaSortedFeatures(:,1),'bx');
                    %plot(rhoThetaSortedFeatures(:,5), rhoThetaSortedFeatures(:,4),'bx');
                    
                    intensityProfile = rhoThetaSortedFeatures(:,3);
                    if isfield(param, 'rhoThresh')
                        tokens = findLocalTokens(intensityProfile,param.localTokensN,method,rhoThetaSortedFeatures(:,1),param.thresh1, param.thresh2);
                    else
                        tokens = findLocalTokens(intensityProfile,param.localTokensN,method,rhoThetaSortedFeatures(:,1),param.thresh1, param.thresh2, param.rhoThresh);
                    end
                    if ~isempty(tokens)
                        if strcmp(method,'maxOnly')
                            idx = tokens(2,:);
                            % ORIGINAL UNGROUPED SET OF TOKENS
                            %whiskerTree{i} = [rhoThetaSortedFeatures(idx,5), rhoThetaSortedFeatures(idx,4), rhoThetaSortedFeatures(idx,3), rhoThetaSortedFeatures(idx,2)];
                            %plot(rhoThetaSortedFeatures(idx,5), rhoThetaSortedFeatures(idx,4),'mx');
                            whiskerTree{i} = groupConnectedTokens(idx, rhoThetaSortedFeatures);
                            
                            %plot(rhoThetaSortedFeatures(idx,5), rhoThetaSortedFeatures(idx,4),'gx');
                            %plot(whiskerTree{i}(:,2), whiskerTree{i}(:,1),'yo');
                        end
                    end
                end
            end
        else
            %     do  12.02.2010
            for i=1:numel(potentialDensityArray),
                DELTA = 1;
                idx = (abs(rhoSorted(:,1) - minimumRho * potentialDensityArray(i)) < DELTA);
                if sum(idx)
                    extractedFeatures = rhoSorted(idx,:);
                    rhoThetaSortedFeatures = sortrows(extractedFeatures,2);
                    
                    
                    %polar(rhoThetaSortedFeatures(:,2), rhoThetaSortedFeatures(:,1),'bx');
                    %plot(rhoThetaSortedFeatures(:,5), rhoThetaSortedFeatures(:,4),'bx');
                    
                    intensityProfile = rhoThetaSortedFeatures(:,3);
                    if isfield(param, 'rhoThresh')
                        tokens = findLocalTokens(intensityProfile,param.localTokensN,method,rhoThetaSortedFeatures(:,1),param.thresh1, param.thresh2);
                    else
                        tokens = findLocalTokens(intensityProfile,param.localTokensN,method,rhoThetaSortedFeatures(:,1),param.thresh1, param.thresh2, param.rhoThresh);
                    end
                    if ~isempty(tokens)
                        if strcmp(method,'maxOnly')
                            idx = tokens(2,:);
                            % ORIGINAL UNGROUPED SET OF TOKENS
                            %whiskerTree{i} = [rhoThetaSortedFeatures(idx,5), rhoThetaSortedFeatures(idx,4), rhoThetaSortedFeatures(idx,3), rhoThetaSortedFeatures(idx,2)];
                            %plot(rhoThetaSortedFeatures(idx,5), rhoThetaSortedFeatures(idx,4),'mx');
                            whiskerTree{i} = groupConnectedTokens(idx, rhoThetaSortedFeatures);
                            
                            %plot(rhoThetaSortedFeatures(idx,5), rhoThetaSortedFeatures(idx,4),'gx');
                            %plot(whiskerTree{i}(:,2), whiskerTree{i}(:,1),'yo');
                        end
                    end
                end
            end
        end
        
    case 'LOCALANGLE'
        
        whiskersSmoothedImage = imfilter(whiskersGrayImage, fspecial('average'));
        whiskersSmoothedMask = im2bw(whiskersSmoothedImage, 0.01);
        idx = (whiskersSmoothedMask);
        [x, y] = find(whiskersSmoothedMask);
        [theta, rho] = cart2pol(x - X(2), y - X(1));
        minimumRho = 55; % HARCODED in order to avoid data dependence from min(rho);
        whiskersFeatures = [rho, theta, double(whiskersGrayImage(idx)), x, y];
        rhoSorted = sortrows(whiskersFeatures);
        
        
        
        
        %figure(3)
        %imshow(whiskersSmoothedImage)
        %hold on
        
        for i=1:numel(potentialDensityArray),
            
            if potentialDensityArray(i) < 2,
                % take the point radius from the center of the snout
                DELTA = 0.5;
                idx = (abs(rhoSorted(:,1) - minimumRho * potentialDensityArray(i)) < DELTA);
                if sum(idx)
                    extractedFeatures = rhoSorted(idx,:);
                    rhoThetaSortedFeatures = sortrows(extractedFeatures,2);
                    
                    
                    polar(rhoThetaSortedFeatures(:,2), rhoThetaSortedFeatures(:,1),'bx');
                    %plot(rhoThetaSortedFeatures(:,5), rhoThetaSortedFeatures(:,4),'bx');
                    
                    intensityProfile = rhoThetaSortedFeatures(:,3);
                    tokens = findLocalTokens(intensityProfile,4,method,rhoThetaSortedFeatures(:,1));
                    if ~isempty(tokens)
                        if strcmp(method,'maxOnly')
                            idx = tokens(2,:);
                            
                            % ORIGINAL UNGROUPED SET OF TOKENS
                            %whiskerTree{i} = [rhoThetaSortedFeatures(idx,5), rhoThetaSortedFeatures(idx,4), rhoThetaSortedFeatures(idx,3), rhoThetaSortedFeatures(idx,2)];
                            whiskerTree{i} = groupConnectedTokens(idx, rhoThetaSortedFeatures);
                            
                            %plot(rhoThetaSortedFeatures(idx,5), rhoThetaSortedFeatures(idx,4),'gx');
                            %plot(whiskerTree{i}(:,2), whiskerTree{i}(:,1),'ro');
                        end
                    end
                end
                
                startingPoints = whiskerTree{i};
            else
                % find distance from strting Points
                %[rho, theta, double(whiskersGrayImage(idx)), x, y]
                idx = rhoSorted(:,1) > (minimumRho * potentialDensityArray(i));
                features = rhoSorted(idx,:);
                if ~isempty(features)
                    pts = startingPoints(:,[1 2])';
                    ptsbx = pts(:,1);
                    ptsby = pts(:,2);
                    ptsax = features(:,4)';
                    ptsay = features(:,5)';
                    %
                    Sa = numel(ptsax);
                    Sb = numel(ptsbx);
                    xDiff = repmat(ptsax,[Sb 1]) - repmat(ptsbx', [1 Sa]);
                    yDiff = repmat(ptsay,[Sb 1]) - repmat(ptsby', [1 Sa]);
                    % avoid sqrt for faster min searching
                    [distOfEl, minidx] = min(xDiff.^2 + yDiff.^2);
                    idx = distOfEl < distanceValue^2;
                end
            end
        end
        
        
        
end


