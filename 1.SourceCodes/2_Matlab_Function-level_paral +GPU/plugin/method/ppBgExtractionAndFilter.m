
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



% author goren gordon and/or igor perkon
% citation not required



function output = ppBgExtractionAndFilter(operation, job, input)

%*************************************************************************
%  Author: Igor Perkon    -    perkon@sissa.it




% PROBABLY SHOULD WORK THIS IN AS A PARAMETER
%
% mask image bwMask
%
%BWmask = ones(size(imageBuffer));
%BWmask = []; % default if empty is ones(size(...))
%----------------------------------------------------
% IF YOU WANT TO CHANGE YOUR MASK UNCOMMENT HERE
%----------------------------------------------------
%     figure(1)
%     imagesc(imageBuffer);
%     colormap(gray)
%     axis image
%     title('Select region!!')
%     BWmask = roipoly;
%
% PROBABLY SHOULD WORK THIS IN AS A PARAMETER





switch operation
	
	case 'info'
		
		output.author = 'Igor Perkon';
		
		
		
	case 'parameters'
		
		% empty
		pars = {};
		
		% constant
		par = [];
		par.name = 'bwMask';
		par.value = [];
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'histEqActivate';
		par.label = 'Activate Histogram Equalisation';
		par.type = 'flag';
		par.value = true;
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'histEqGamma';
		par.label = 'Histogram Equalisation Gamma';
		par.type = 'scalar';
		par.range = [0.1 4];
		par.value = 1;
		par.step = [0.05 0.5];
		par.precision = 2;
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'histEqMinT';
		par.label = 'Histogram Equalisation Min T';
		par.type = 'scalar';
		par.range = [0 255];
		par.value = 0;
		par.step = [1 4];
		par.constraints = {
			{'<' 'histEqMaxT'}
			};
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'histEqMaxT';
		par.label = 'Histogram Equalisation Max T';
		par.type = 'scalar';
		par.range = [0 255];
		par.value = 64;
		par.step = [1 4];
		par.constraints = {
			{'>' 'histEqMinT'}
			};
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'bwThreshold';
		par.label = 'Black/White Threshold';
		par.type = 'scalar';
		par.range = [0.05 1];
		par.value = 0.2;
		par.step = [0.01 0.05];
		par.precision = 2;
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'bwMorphology';
		par.label = 'Black/White Morphology';
		par.type = 'scalar';
		par.range = [0 1];
		par.value = 0.02;
		par.step = [0.001 0.005];
		par.precision = 3;
		pars{end+1} = par;
		
		% parameter
		par = [];
		par.name = 'razorSize';
		par.label = 'Razor Size';
		par.type = 'scalar';
		par.range = [1 40];
		par.value = 5;
		par.step = [1 3];
		pars{end+1} = par;
		
		% ok
		output.pars = pars;
		
		
		
	case 'initialize'
		
		% get background image from cache
		bgImage = job.cacheGet('bgImage');
		
		% if absent, recreate it
		if isempty(bgImage)
			bgImage = stGetBackgroundImage(job);
			job.cacheSet('bgImage', bgImage, 'vp');
		end
		
		% and store it in state
		output.bgImage = bgImage;
		
		% retrieve parameters
		output.pars = job.getRuntimeParameters();

		% empty mask means use ones()
		if isempty(output.pars.bwMask)
			info = job.getVideoInfo();
			output.pars.bwMask = true(info.Size);
		end
		
		
		
	case 'process'

		% get frame
		frame = job.getRuntimeFrame(input.frameIndex);

			% mask the image
			[~, bwShape] = stExtractWhiskersGrayValuesWithMorphologyAndShave( ...
				frame{1}, input.pars.bwThreshold, input.pars.bwMorphology, ...
				input.pars.razorSize, input.pars.bwMask);

			% extract background
			GRAYwhiskersImage = (input.bgImage - frame{1});
			GRAYwhiskersImage = immultiply(GRAYwhiskersImage, uint8(bwShape));

			% equalize the image
			if input.pars.histEqActivate
				frame = imadjust( ...
						GRAYwhiskersImage, ...
						[input.pars.histEqMinT/255 input.pars.histEqMaxT/255], ...
						[0 1], ...
						input.pars.histEqGamma ...
					);
			else
				frame = GRAYwhiskersImage;
			end

			% frame
			frame = {frame};

		% set frame
		job.setRuntimeFrame(input.frameIndex, frame);
		
		% propagate state
		output = input;
		
		
		
	otherwise
		output = [];
		
		
end



