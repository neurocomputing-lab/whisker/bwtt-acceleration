
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function whiskerTreeStructure = groupConnectedTokens(idx, rhoThetaSortedFeatures)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Function groupConnectedTokens
%
% Inputs:   idx, index of tokens in rhoThetaSortedfeatures,
%           rhoThetaSortedFeatures is the sorted matrix with [rho, theta,
%           double(whiskersGrayImage(idx)), x, y] format
%
% Outputs:  whiskerTreeStructure is the standard tokens structure with the
%           following format [x, y, intensityProfile, theta, rho] where
%           vectors are columns
%
% This function is required for the method oriented radial distance, since
% tends to detect a number of connected maxima. Therefore, in order to
% simplify the number of tokens, this function scans the array of detected
% and tokens and if they are connected it averages them.
% The 8N connectivity is used.
% <\outDOC>
tokensIndex = 0;
elementsInToken = 0;
currentX = 0;
currentY = 0;
whiskerTreeStructure = zeros(numel(idx), 5);
for t=1:numel(idx),
    i=idx(t);
    X = rhoThetaSortedFeatures(i,4);
    Y = rhoThetaSortedFeatures(i,5);
    if (abs(currentX-X) > 1) || (abs(currentY - Y) > 1)
        if tokensIndex
            % new token strategy
            M = numel(xOfToken);
            if M > 2,
                % if they are spread in a line
                if ((max(xOfToken)-min(xOfToken)+1) == M)||((max(yOfToken)-min(yOfToken)+1) == M)
                    % make two tokens with extrema
                    whiskerTreeStructure(tokensIndex,:) = [xOfToken(1), yOfToken(1), iOfToken(1), thetaOfToken(1), rhoOfToken(1)];
                    tokensIndex = tokensIndex+1;
                    whiskerTreeStructure(tokensIndex,:) = [xOfToken(end), yOfToken(end), iOfToken(end), thetaOfToken(end), rhoOfToken(end)];
                else
                    whiskerTreeStructure(tokensIndex,:) = [mean(xOfToken), mean(yOfToken), mean(iOfToken), mean(thetaOfToken), mean(rhoOfToken)];
                end
            else
                whiskerTreeStructure(tokensIndex,:) = [mean(xOfToken), mean(yOfToken), mean(iOfToken), mean(thetaOfToken), mean(rhoOfToken)];
            end
            elementsInToken = 0;
            xOfToken = 0;
            yOfToken = 0;
            iOfToken = 0;
            rhoOfToken = 0;
            thetaOfToken = 0;
        end
        tokensIndex = tokensIndex+1;
    end
    elementsInToken = elementsInToken+1;
    xOfToken(elementsInToken) = X;
    yOfToken(elementsInToken) = Y;
    iOfToken(elementsInToken) = rhoThetaSortedFeatures(i,3);
    rhoOfToken(elementsInToken) = rhoThetaSortedFeatures(i,1);
    thetaOfToken(elementsInToken) = rhoThetaSortedFeatures(i,2);
    currentX = X;
    currentY = Y;
end
% close the last opened token
if elementsInToken
    whiskerTreeStructure(tokensIndex,:) = [mean(xOfToken), mean(yOfToken), mean(iOfToken), mean(thetaOfToken), mean(rhoOfToken)];
end
% shrinkMatrix
idx = find(whiskerTreeStructure(:,1) == 0,1,'first');
if ~isempty(idx)
    whiskerTreeStructure = whiskerTreeStructure(1:(idx-1),:);
end



