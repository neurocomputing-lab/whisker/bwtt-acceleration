
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [ o1, o2, o3, o4 ] = convertTensorEV( i1, i2, i3, i4 )
% converts a
%   tensor field to eigenvectors and eigenvalues.
%   

    if nargin==1
        K11 = i1(:,:,1,1);
        K12 = i1(:,:,1,2);
        K21 = i1(:,:,2,1);
        K22 = i1(:,:,2,2);

        [n,p] = size(K11);

        o1 = zeros(n,p,2);
        o2 = zeros(n,p,2);
        o3 = zeros(n,p);
        o4 = zeros(n,p);

        % trace/2
        t = (K11+K22)/2;

        a = K11 - t;
        b = K12;

        ab2 = sqrt(a.^2+b.^2);
        o3 = ab2  + t;
        o4 = -ab2 + t;

        theta = atan2( ab2-a, b );

        o1(:,:,1) = cos(theta);
        o1(:,:,2) = sin(theta);
        o2(:,:,1) = -sin(theta); 
        o2(:,:,2) = cos(theta);
    else
        o1 = zeros( [size(i3),2,2] );
        o1(:,:,1,1) = i3.*i1(:,:,1).^2 + i4.*i2(:,:,1).^2;
        o1(:,:,1,2) = i3.*i1(:,:,1).*i1(:,:,2) + i4.*i2(:,:,1).*i2(:,:,2);
        o1(:,:,2,1) = o1(:,:,1,2);
        o1(:,:,2,2) = i3.*i1(:,:,2).^2 + i4.*i2(:,:,2).^2;
    end



end