
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [strokeIdx, measurementsLimits] = getStrokeIndexesFromNodesListWithHistAndMarker(whiskerTreeLUT, nodesListWithHistAndMarker)


% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Inputs:
%               -whiskerTreeLUTwithOrientations is a matrix, where the data are
%               [inCurve inMarker ::::: xofmarker, yofmarker, c, theta,
%               rho,angle, roundness, saliency]
%               nodesListWithHistAndMarker 
%               - nodesListWithHistAndMarker is a list with
%               [inCurve, inMarker, histogramOFedges, whiskerId]
% Outputs:
%               - strokeIdx is a vector of the same lenght of
%               whiskerTreeLUT and contains for every entry the stroke
%               index 
%               - measurementsLimits is a matrix where each row express
%               some measurement propertires. First column is lenght,
%               second is minRho and third is MaxRho
%               
%<\outDOC>
L = size(whiskerTreeLUT,1);
strokeIdx = zeros(L,1);
for i=1:L,
    inCurve= whiskerTreeLUT(i,1);
    inMarker = whiskerTreeLUT(i,2);
    idx = (nodesListWithHistAndMarker(:,1) == inCurve) &...
        (nodesListWithHistAndMarker(:,2) == inMarker);
    strokeNumber = nodesListWithHistAndMarker(idx,4);
    if ~isempty(strokeNumber),
        if strokeNumber > 0,
            strokeIdx(i) = strokeNumber;
        end
    end
end

magicDistance = 3.3;%IsTheAverageDistanceBetweenPointsofStroke
% elements data
idxOfSingleElements = (strokeIdx == 0);
numberOfSingleMeasuremets = sum(idxOfSingleElements);
extractedData = whiskerTreeLUT(idxOfSingleElements,7);
elementsLimits = [magicDistance *ones(numberOfSingleMeasuremets ,1), extractedData, extractedData];



% stroke calculations
numberOfStrokes = max(strokeIdx);
strokesLimits = zeros(numberOfStrokes,3);

for i=1:numberOfStrokes,
    % for every stroke check if it is at least partially matched
    idx = (strokeIdx == i);
    elementsOfWhisker = whiskerTreeLUT(idx,:);
    % sort according rho
    [sortedElements, sortedIdx] = sortrows(elementsOfWhisker, 7);
    [t,r] = cart2pol([sortedElements(1:end-1,3) - sortedElements(2:end,3)],...
        [sortedElements(1:end-1,4) - sortedElements(2:end,4)]);

    strokesLimits(i,1) = sum(r);
    % min rho
    strokesLimits(i,2) = sortedElements(1,7);
    % max rho
    strokesLimits(i,3) = sortedElements(end,7);
end


measurementsLimits = [elementsLimits; strokesLimits];
        
    