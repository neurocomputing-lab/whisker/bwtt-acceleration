
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function connectivityMatrix = eliminateRedundantEdges(connectivityMatrix, whiskerData, figNum, col, lineW)
% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Function eliminateRedundantEdges(connectivityMatrix)
% Inputs:   connectivityMatrix has the format [curveIn NodeIn curveOut
%           nodeOut distance]
%           whiskerData can be whiskerTree or whikserTreeLUT format
% Outputs:  same
%
%<\outDOC>

if nargin < 4,
    col = 'w';
end
if nargin < 5,
    lineW = 1;
end




linkEdges = sortrows(connectivityMatrix);
idx = 1;
connectivityMatrix = zeros(size(connectivityMatrix));
connectivityMatrix(idx,:) = linkEdges(1,:);

for i=2:size(connectivityMatrix,1),
    if sum(linkEdges(i-1,1:4) ~= linkEdges(i,1:4))
        idx = idx+1;
        connectivityMatrix(idx,:) = linkEdges(i,:);
    end
end

connectivityMatrix = connectivityMatrix(1:idx,:);

if figNum
    figure(figNum)
    hold on
    %cmap = colormap(jet(max(connectivityMatrix(:,5))));
    %colormap(gray)
    for i=1:size(connectivityMatrix,1),
        inCurve = connectivityMatrix(i,1);
        inMarker = connectivityMatrix(i,2);
        outCurve = connectivityMatrix(i,3);
        outMarker = connectivityMatrix(i,4);
        distOfEl = connectivityMatrix(i,5);
        if iscell(whiskerData)
            % use whiskerTree notatiom
            x1 = whiskerData{inCurve}(inMarker,1);
            y1 = whiskerData{inCurve}(inMarker,2);
            x2 = whiskerData{outCurve}(outMarker,1);
            y2 = whiskerData{outCurve}(outMarker,2);
        else
            % use whiskerTreeLUTwithOrientations
            idx = whiskerData(:,1) == inCurve & ...
                whiskerData(:,2) == inMarker;
            x1 = whiskerData(idx,3);
            y1 = whiskerData(idx,4);
            idx = whiskerData(:,1) == outCurve & ...
                whiskerData(:,2) == outMarker;
            x2 = whiskerData(idx,3);
            y2 = whiskerData(idx,4);
        end
            
        %line([y1 y2],[x1 x2],'Color',col);%cmap(distOfEl,:));

        line([y1 y2],[x1 x2],'Color', col, 'LineWidth', lineW);
    end
    hold off
end



