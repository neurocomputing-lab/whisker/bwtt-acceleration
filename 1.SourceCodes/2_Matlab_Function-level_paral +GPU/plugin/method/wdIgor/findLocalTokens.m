
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function tokens = findLocalTokens(f, N, method, rho, thresh1, thresh2, rhoThresh)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Function findLocalTokens
%
% Inputs:   f, the function to be analysed
%           N is the local neighbourhood
%           method is the string method indicator
%           rho is used for having a local threshold information
%           thresh1 internal threshold
%           thresh2 external threshold
%           rhoThresh defines the boundary between the areas
% Outputs:  tokes is the matrix with different outputs
%           depending of the method
%
% Method 'max'  :is the default method and scans for local maxima
% by comparing the local value with its shifted variant. It returns the max
% values and their positions in the syntax [maxs(1..), pos(1..)]
%
% <\outDOC>



if nargin < 2
    N = 6;
end
if nargin < 3
    method = 'maxOnly';
end

if nargin < 5,
    % fixed threshold

    THRESH = thresh1;
    L = length(f);

    switch method

        case 'maxOnly'

            maxVector = f;
            

            for i = 1 : floor(N/2),
                rightVector = [zeros(i,1); f(1:L-i)];
                leftVector = [f(i+1:L); zeros(i,1)];
                maxVector = max(maxVector,max(rightVector,leftVector));
            end

            idx = find((maxVector == f) & (f > THRESH));

            tokens = [f(idx)'; idx'];
    end

else
    % use maxOnly with variable threshold
    maxVector = f;
    L = length(f);
    if nargin < 7
        rhoThresh = 140;
    end

    if min(rho) < rhoThresh,
        THRESH = thresh1;
    else
        THRESH = thresh2;
    end


    if L > floor(N/2),
        for i = 1 : floor(N/2),
            rightVector = [zeros(i,1); f(1:L-i)];
            leftVector = [f(i+1:L); zeros(i,1)];
            maxVector = max(maxVector,max(rightVector,leftVector));
        end
    else
        maxVector = max(f);
    end


    idx = find((maxVector == f) & (f > THRESH));


    tokens = [f(idx)'; idx'];
end




