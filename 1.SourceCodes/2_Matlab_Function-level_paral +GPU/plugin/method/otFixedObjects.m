
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



% author ben mitch / goren gordon
% citation not required



function state = otFixedObjects(operation, job, state)

% original author : goren gordon
% rebuilt: ben mitch


switch operation
	
	case 'info'
		
		state.author = 'Ben Mitch / Goren Gordon';
		
		
		
	case 'parameters'
		
		% empty
		pars = {};
		
		% parameter
		par = [];
		par.name = 'models';
		par.label = 'Objects';
		par.value = {};
		par.type = 'custom';
		pars{end+1} = par;
		
		% ok
		state.pars = pars;
		
		
		
	case 'editParameter'
		
		% get value
		value = state.value;
		h_overlay = [];
		
		% switch
		switch state.par.name
			case 'models'
				
				% get frame
				fi = job.getFrameRange('initial');
				rawFrame = job.getVideoFrames(fi, 'a');
				
				% create GUI
				gui = stGUI([800 600], gcbf, 'Define Objects', ...
					'style', 'modal', ...
					'resizable', 'on');
				panelRoot = gui.rootPanel;
				h_dialog = gui.getMainWindow();
				
				% image axis
				h_axis = axes('parent', h_dialog, 'units', 'pixels');
				h_image = image(repmat(rawFrame, [1 1 3]), 'parent', h_axis);
				set(h_axis, 'xtick', [], 'ytick', []);
				hold(h_axis, 'on');
				axis(h_axis, 'image');
				set(h_axis, 'Color', 0.5*[1 1 1]);
				set(h_image, 'buttondownfcn', @callback_clickImage);
				
				% list
				h_listbox = uicontrol(h_dialog, ...
					'style', 'listbox', ...
					'hori', 'left', ...
					'fontname', stUserData('listFontName'), ...
					'fontsize', stUserData('listFontSize'), ...
					'callback', @callback_clickListbox, ...
					'BackgroundColor', [1 1 1], ...
					'string', {}, ...
					'Min', 1, 'Max', 2, ...
					'Value', 1, ...
					'units', 'pixels' ...
				);
				
				% help
				guipars = stGUIPars();
				h_help = uicontrol(h_dialog, ...
					'style', 'text', ...
					'hori', 'left', ...
					'fontname', guipars.fontName, ...
					'fontsize', guipars.fontSize, ...
					'string', '', ...
					'units', 'pixels' ...
				);
				
				% layout
				rhs = panelRoot.pack([], 'right', 240);
				lhs = panelRoot.pack(h_axis, 'right', -1);
				rhs.pack(h_help, 'top', -1);
				rhs.pack(h_listbox, 'top', -1);
				
				% buttons
				buttons = {'Add Polygon' 'Add Circle' 'Delete Selected'};
				for b = 1:length(buttons)
					h_button = uicontrol(h_dialog, ...
						'style', 'pushbutton', ...
						'fontname', 'Helvetica', ...
						'fontsize', 10, ...
						'userdata', b, ...
						'callback', @callback_clickButton, ...
						'string', buttons{b}, ...
						'units', 'pixels' ...
						);
					rhs.pack(h_button, 'bottom', 24);
				end
				
				% update
				updateList();
				updateOverlay();
				
				% wait for GUI
				gui.waitForClose();
				
		end
		
		% return
		state = [];
		state.value = value;
		
		
		
	case 'initialize'
		
		% retrieve parameters
		pars = job.getRuntimeParameters();
		state.result.models = pars.models;
		

		
	case 'process'

		% store results
		job.setResults(state.frameIndex, 'object', state.result);

		
		
	case 'plot'
		
		% collect handles
		state.h = [];
		
		% get result
		result = job.getResults(state.frameIndex);
		result = result{1};
		if isfield(result, 'object')
			
			% get models
			models = result.object.models;
			
			% for each object
			for o = 1:length(models)
				
				% get model
				model = models{o};
				
				% get contour
				contour = stObjectContourFromModel(model, 5);
				
				% plot
				state.h(end+1) = plot(state.h_axis, ...
					contour(2, :), contour(1, :), ...
					'y-', 'linewidth', 1);

			end
		
		end
		
		
		
	otherwise
		output = [];
		
		
end





	function callback_clickImage(obj, evt)
		
		% extract
		st = get(gcbf, 'SelectionType');
		cp = get(h_axis, 'CurrentPoint');
		rc = cp(1, [2 1])';
		selected = get(h_listbox, 'value');
		if selected > length(value)
			return
		end
		
		% get value
		v = value{selected};
		
		% handle
		switch v.type
			
			case 'circle'
				switch st
					case 'normal'
						v.center = rc;
						value{selected} = v;
						updateOverlay();
					case 'alt'
						v.radius = max(sqrt(sum((rc - v.center).^2)), 1);
						value{selected} = v;
						updateOverlay();
				end
				
			case 'polygon'
				switch st
					case 'normal'
						i = findClosestVertex(v.vertices, rc);
						v.vertices(:, i) = round(rc);
						value{selected} = v;
						updateOverlay();
					case 'alt'
						i = findClosestVertex(v.vertices, rc);
						if stConfirm('Do you want to add a vertex? If you click No, a vertex will be deleted.')
							l = i - 1;
							r = i + 1;
							if l < 1
								l = size(v.vertices, 2);
							end
							if r > size(v.vertices, 2)
								r = 1;
							end
							vv = v.vertices(:, [l r]);
							j = findClosestVertex(vv, rc);
							if j == 1
								% left was closest
								j = l;
							else
								% right was closest
								j = r;
							end
							v1 = v.vertices(:, i);
							v2 = v.vertices(:, j);
							vnew = (v1 + v2) / 2;
							if j == l
								v.vertices = [v.vertices(:, [1:i-1]) vnew v.vertices(:, [i:end])];
							else
								v.vertices = [v.vertices(:, [1:i]) vnew v.vertices(:, [i+1:end])];
							end
							value{selected} = v;
							updateOverlay();
						else
							if length(v.vertices) > 2
								v.vertices = v.vertices(:, [1:i-1 i+1:end]);
								value{selected} = v;
								updateOverlay();
							else
								stMessage('Cannot delete - only two vertices remain');
							end
						end
				end
				
		end
		
	end

	function callback_clickButton(obj, evt)
		
		switch get(obj, 'string')
			
			case 'Add Circle'
				object = [];
				object.type = 'circle';
				object.center = [100 100]';
				object.radius = 100;
				value{end+1} = object;
				updateList(length(value));
				updateOverlay();
			
			case 'Add Polygon'
				object = [];
				object.type = 'polygon';
				object.vertices = [100 200 200 100; 100 100 200 200];
				value{end+1} = object;
				updateList(length(value));
				updateOverlay();
				
			case 'Delete Selected'
				if length(value)
					selected = get(h_listbox, 'value');
					if selected > length(value)
						return
					end
					value = value([1:selected-1 selected+1:end]);
					updateList();
					updateOverlay();
				end
			
		end
		
	end

	function callback_clickListbox(obj, evt)
		
		updateList();
		updateOverlay();
		
	end

	function updateList(selected)
		
		ss = {};
		for i = 1:length(value)
			v = value{i};
			switch v.type
				case 'circle'
					ss{end+1} = 'Circle';
				case 'polygon'
					ss{end+1} = 'Polygon';
			end
		end
		
		if ~nargin
			selected = get(h_listbox, 'value');
		end
		if selected > length(ss)
			selected = 1;
		end
		set(h_listbox, 'string', ss, 'value', selected);
		
		if selected <= length(value)
			switch value{selected}.type
				case 'circle'
					set(h_help, 'string', 'Left click to set centre, right click to set radius.');
				case 'polygon'
					set(h_help, 'string', 'Left click to move nearest vertex, right click to add or delete a vertex.');
			end
		else
			set(h_help, 'string', '');
		end
		
	end

	function updateOverlay()
		
		delete(h_overlay)
		h_overlay = [];
		selected = get(h_listbox, 'value');

		for i = 1:length(value)
			v = value{i};
			if i == selected
				col = [1 0 0];
				inc = true;
			else
				col = 0.6*[1 1 1];
				inc = false;
			end
			switch v.type
				case 'circle'
					c = v.center;
					r = v.radius;
					vv = [];
 					for th = linspace(0, 2*pi, 81)
 						vv(:, end+1) = c + [cos(th); sin(th)] * r;
 					end
					h_overlay(end+1) = plot(vv(2, :), vv(1, :), 'color', col);
					if inc
						h_overlay(end+1) = plot(c(2, :), c(1, :), 'w.');
						h_overlay(end+1) = plot(c(2, :), c(1, :)-r, 'w.');
					end
				case 'polygon'
					vv = v.vertices(:, [1:end 1]);
					h_overlay(end+1) = plot(vv(2, :), vv(1, :), 'color', col);
					if inc
						h_overlay(end+1) = plot(vv(2, :), vv(1, :), 'w.');
					end
			end
		end
		
		set(h_overlay, 'buttondownfcn', @callback_clickImage);

	end



end







% function [iObject, xO, yO, iO] = FindClosestObject(objectContour, x, y)
% 
% minDist = 1e5;
% 
% for k=1:length(objectContour)
% 	[dist, iDist] = min( ((objectContour{k}(1,:) - y).^2) + ((objectContour{k}(2,:) - x).^2) );
% 	if(dist < minDist)
% 		minDist = dist;
% 		iObject = k;
% 		yO = objectContour{k}(1,iDist);
% 		xO = objectContour{k}(2,iDist);
% 		iO = iDist;
% 	end
% end
% 
% 
% end





function i = findClosestVertex(vs, v)

n = size(vs, 2);
d = vs - repmat(v, 1, n);
s = sum(d.^2);
[temp, i] = min(s);

end







