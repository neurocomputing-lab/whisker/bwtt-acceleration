
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [X, bTemplate, W, Q0, pp, perpdists,P] = trackAndUpdateTemplate(X, largestBoundary, bTemplate, W, Q0, numberOfProbePoints, maxDistance)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Inputs:   - X is the shape space parameters
%           - largestBoundary is the vector of all boundary points of the rat-s
%           rat's mask
%           - Btemplate is the B spline template 
%           - W, Q0 is the Blake notation
%           - numberOfProbePoints is the number of points used for
%           calculating distance
%           - maxDistance is the validation gate distance
%
% Outputs:  - X shape array 1x4
%           - bTemplate is the Bspline template
%           - W standard Blake notation
%           - Q0  standard Blake notation
%           - pp shape of the contour
%           - perpdists perpendicular distances
%           - P is the state covariance estimate
% <\outDOC>

[X, perpdists,P] = trackInSplineSpace(X, largestBoundary, bTemplate, W, Q0, numberOfProbePoints, maxDistance, 0);

% UPDATE TEMPLATES - transformation in the coefficient space Q = W*X+Q0
q = (W*X + Q0);
bTemplate.coefs =[q([1:numel(q)/2]), q([numel(q)/2+1:end])]';
%pp = cscvn([yInterp, xInterp]');
pp = fn2fm(bTemplate,'pp');