
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====



% FAIR USE
%
% Citation requirement. This method has been published and
% you should cite the publication in your own work. See the
% URL below for more information:
%
% http://bwtt.sourceforge.net/fairuse



% this is a stub that wraps the directory underneath, where
% the implementation is. you can equally well place the
% implementation in this file.

function state = wdGVM(operation, job, state)

switch operation
	
	case 'info'
		state.author = 'Georges Gyory, Vladan Rankov and Ben Mitch';
		output.citation = 'citation';
	
	case 'parameters'
		state.pars = wdGVMParameters();
		
	case 'initialize'
		state = [];
		state.pars = job.getRuntimeParameters();

		% OLD INIT CODE
% prevChunkData.localRadiusCurrent = [];
% prevChunkData.localRadiusPrev = [];
% prevChunkData.snoutCenterCurrent = [];
% prevChunkData.snoutCenterPrev = [];
% prevChunkData.angles = [];
		
	case 'process'
		state = wdGVMProcess(job, state);
		
	case 'plot'

		results = job.getResults(state.frameIndex);
		
		h = [];
		if isfield(results{1}, 'whisker')
			whisker = results{1}.whisker;
			col = 'wrgbmyc';
			for w = 1:length(whisker.contour)
				cn = whisker.contour{w};
				h(end+1) = plot(state.h_axis, cn(2, :), cn(1, :), [col(mod(w, length(col)) + 1) '-'], 'linewidth', 1);
			end
		end
		
		state = [];
		state.h = h;
		
	otherwise
		state = [];
		
end



