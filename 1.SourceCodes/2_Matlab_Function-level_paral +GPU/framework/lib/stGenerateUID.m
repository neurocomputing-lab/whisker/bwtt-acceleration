
% uid = stGenerateUID()
%	  generate a unique identifier


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function uid = stGenerateUID()

% new way - is this available in all conditions? -nojava?
% seems to be, but anyway is only needed whilst creating
% jobs, not whilst running them.

% make sure it starts with a letter, then we can use it as a
% field name, which is very convenient
uid = '1';
while uid(1) < 65
	uid = genuidsub();
end

function uid = genuidsub()

uid = upper(char(java.util.UUID.randomUUID()));
uid = [uid(1:4) '-' uid(5:28) '-' uid(29:32) '-' uid(33:36)];
uid(uid == '-') = '_';
uid = uid(1:19);


% % old way
% 
% persistent lastUID
% 
% while 1
% 	
%  	uid = ['#BWTT#' dec2hex(round((now-730000)*1e8)) '#'];
% 	
% 	if isempty(lastUID)
% 		break
% 	end
% 	if ~strcmp(uid, lastUID)
% 		break
% 	end
% 	pause(0.001);
% 	
% end
% 
% lastUID = uid;
% 
