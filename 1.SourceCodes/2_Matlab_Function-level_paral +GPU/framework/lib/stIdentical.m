
% identical = stIdentical(a, b)
%   returns true if a and b are identical, else returns
%   false.


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function same = stIdentical(a, b)

same = false;

if ~strcmp(class(a), class(b))
	return
end

if ndims(a) ~= ndims(b)
	return
end

if any(size(a) ~= size(b))
	return
end
nelem = prod(size(a));

cls = class(a);
if isnumeric(a) || islogical(a)
	cls = 'numeric';
end

switch cls
	
	case 'numeric'
		a = a(:);
		b = b(:);
		i = isnan(a) & isnan(b);
		same = all(a(~i) == b(~i));
		
	case 'cell'
		for e = 1:nelem
			s = stIdentical(a{e}, b{e});
			if ~s
				return
			end
		end
		same = true;
		
	case 'char'
		same = strcmp(a, b);
		
	case 'struct'
		if nelem > 1
			for n = 1:nelem
				if ~stIdentical(a(n), b(n))
					return
				end
				same = true;
			end
		else
			fa = fieldnames(a);
			fb = fieldnames(b);
			if ~stIdentical(fa, fb)
				return
			end
			for f = 1:length(fa)
				fn = fa{f};
				if ~stIdentical(a.(fn), b.(fn))
					return
				end
			end
			same = true;
		end
		
	otherwise
		error('BWTT:stIdentical:InternalError', ...
			['stIdentical(): case not coded for class "' class(a) '"']);
		
end



