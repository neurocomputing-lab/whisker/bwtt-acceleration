
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% CHANGE LOG
%
% 20/11/14 (mitch)
% fixed for R2014b, and fixed small bug



function [stateOut, stateChanged] = stPreferencesGUI(prefs, state, h_parent, title, wh, cols)

if nargin < 3
	h_parent = [];
end

if nargin < 4
	title = 'Preferences';
end

if nargin < 5
	wh = [640 400];
end

if nargin < 6
	cols = 2;
end

stateOut = [];
stateChanged = [];
gui = [];

try
	
	% create GUI
	gui = stGUI(wh, h_parent, title);
	set(gui.getMainWindow(), 'KeyPressFcn', @keydown);
	
	% create layout
	panelButtons = gui.rootPanel.pack([], 'bottom', 36);
	panelRemainder = gui.rootPanel.pack([], 'bottom', -1);
	for c = 1:cols
		if c ~= 1
			panelRemainder.pack([], 'left', 8);
		end
		panelCols{c} = panelRemainder.pack([], 'left', -1);
	end
	currentCol = 1;
	
	% java objects
	java_objects = [];
	
	% add prefs
	fn = fieldnames(prefs);
	h_controls = struct();
	for n = 1:length(fn)
		
		% extract
		key = fn{n};
		pref = prefs.(key);
		
		% ignore unless "type" is specified
		if ~isfield(pref, 'type')
			continue
		end
		
		% handle colbreak
		if isfield(pref, 'colbreak')
			currentCol = currentCol + 1;
		end
		
		% handle default value
		if ~isfield(state, key)
			if isfield(pref, 'default')
				state.(key) = pref.default;
			else
				state.(key) = [];
			end
		end
		
		% add component
		h_controls.(key) = create_control(gui, panelCols{currentCol}, pref, state.(key));
		
	end
	
	% create buttons
	bw = 72;
	panelButtons.pack(createButton('Cancel', @close_cancel), 'right', bw);
	panelButtons.pack(createButton('OK', @close_OK), 'right', bw);
	
	% focus particular control, if requested
	if isfield(state, 'focus__')
		drawnow
		uicontrol(h_controls.(state.focus__));
	end
	
	% string states
	string_state = [];
	
catch err
	
	if ~isempty(gui)
		gui.close();
	end
	
	rethrow(err)
	
end


% no interrupts
gui.noInterrupts();

% layout GUI
gui.resize();

% wait for closure
gui.waitForClose();



	function keydown(obj, evt)
		
		switch evt.Key
			
			case 'escape'
				close_cancel();
				
			case 'return'
				close_OK();
				
			otherwise
				try
					if strcmp(get(obj, 'style'), 'edit')
						key = get(obj, 'Tag');
						j = java_objects.(key);
						string_state.(key) = j.Text;
					end
				end
				
		end
		
	end

	function close_OK(obj, evt)
		
		stateOut = state;
		stateChanged = [];
		
		fn = fieldnames(prefs);
		for n = 1:length(fn)
			
			key = fn{n};
			pref = prefs.(key);
			
			% ignore unless "type" is specified
			if ~isfield(pref, 'type')
				stateChanged.(key) = 0;
				continue
			end
			
			if strcmp(pref.type, 'info')
				continue;
			end
			
			obj = findobj('Tag', key);
			value = get(obj, 'String');
			
			if strcmp(pref.type, 'string') && isfield(string_state, key)
				value = string_state.(key);
			end
			
			% default is just to store it
			stateOut.(key) = value;
			
			% but some options do some mods
			switch pref.type
				
				case 'dir'
					% check exists
					if ~exist(value, 'dir')
						if ~stConfirm(['The directory "' value '" does not exist. Do you want to keep this setting?']);
							return
						end
					end
					
				case 'option'
					% pare
					stateOut.(key) = value{get(obj, 'Value')};
					
					% convert to a number if specified
					for o = 1:length(pref.options)
						if strcmp(num2str(stateOut.(key)), num2str(pref.options{o}))
							stateOut.(key) = pref.options{o};
							break
						end
					end
					
				case 'servers'
					
					values = value;
					value = {};
					for v = 1:length(values)
						vv = values{v};
						f = find(vv == '=', 1);
						vs = vv(1:f-2);
						vp = vv(f+2:end);
						value{end+1, 1} = {vs vp};
					end
					
					% pare
					stateOut.(key) = value;
					
				case 'flag'
					% pare
					stateOut.(key) = get(obj, 'Value') == 1;
					
			end
			
			% changed?
			stateChanged.(key) = ~stIdentical(state.(key), stateOut.(key));
			
		end
		
		% ok
		gui.close();
		
	end

	function close_cancel(obj, evt)
		
		% ok
		gui.close();
		
	end

	function [h_label, labelPanel] = create_label(gui, panel, pref)
		
		if ~isfield(pref, 'label') || isempty(pref.label)
			h_label = [];
			item = [];
			return
		end
		
		h_parent = gui.getMainWindow();
		h_label = uicontrol(h_parent, ...
			'String', pref.label, ...
			'fontsize', 8, 'fontname', 'tahoma', ...
			'BackgroundColor', get(h_parent, 'Color'), ...
			'Hori', 'Left', ...
			'Style', 'Text');
		
		labelPanel = panel.pack(h_label, 'top', 16, 0);
		
	end

	function h_control = create_control(gui, panel, pref, value)
		
		switch pref.type
			
			case 'info'
				h_control = createControl_info(panel, pref, value);
				
			case 'flag'
				h_control = createControl_flag(panel, pref, value);
				
			case 'string'
				h_control = createControl_string(panel, pref, value);
				
			case 'option'
				h_control = createControl_option(panel, pref, value);
				
			case 'servers'
				h_control = createControl_servers(panel, pref, value);
				
			case 'dir'
				h_control = createControl_dir(panel, pref, value);
				
		end
		
	end





%% CONTROL TYPE: INFO

	function h_control = createControl_info(panel, pref, value)
		
		[h_label, labelPanel] = create_label(gui, panel, pref);
		
		h = pref.height;
		bgcol = get(h_parent, 'color');
		
		h_control = uicontrol(h_parent, ...
			'String', value, ...
			'Min', 0, ...
			'Max', 2, ...
			'HorizontalAlignment', 'left', ...
			'BackgroundColor', bgcol, ...
			'Style', 'text');
		
		info = panel.pack([], 'top', h);
		info.pack([], 'left', 8);
		info.pack(h_control, 'left', -1);
		
	end



%% CONTROL TYPE: FLAG

	function h_control = createControl_flag(panel, pref, value)
		
		[h_label, labelPanel] = create_label(gui, panel, pref);
		
		h_control = uicontrol(h_parent, ...
			'String', '', ...
			'HorizontalAlignment', 'right', ...
			'Tag', key, ...
			'Value', value, ...
			'Style', 'checkbox');
		labelPanel.pack(h_control, 'right', 16);
		
		panel.pack([], 'top', 0);
		
	end



%% CONTROL TYPE: STRING

	function h_control = createControl_string(panel, pref, value)
		
		[h_label, labelPanel] = create_label(gui, panel, pref);
		
		h = 24;
		bgcol = [1 1 1];
		
		h_control = uicontrol(h_parent, ...
			'String', value, ...
			'HorizontalAlignment', 'left', ...
			'BackgroundColor', bgcol, ...
			'KeyPressFcn', @keydown, ...
			'Tag', key, ...
			'Style', 'edit');
		
		panel.pack(h_control, 'top', h);
		
		java_objects.(key) = stFindJObj(h_control);
		
	end



%% CONTROL TYPE: OPTIONS

	function h_control = createControl_option(panel, pref, value)
		
		[h_label, labelPanel] = create_label(gui, panel, pref);
		
		h = 24;
		bgcol = [1 1 1];
		
		% find value
		i_value = [];
		for o = 1:length(pref.options)
			if strcmp(num2str(value), num2str(pref.options{o}))
				i_value = o;
				break
			end
		end
		
		h_control = uicontrol(h_parent, ...
			'String', pref.options, ...
			'BackgroundColor', bgcol, ...
			'Tag', key, ...
			'Max', 1, ...
			'Min', 0, ...
			'Value', i_value, ...
			'Style', 'popupmenu');
		
		panel.pack(h_control, 'top', h);
		
	end



%% CONTROL TYPE: DIR

	function control_browse(obj, evt)
		
		key = get(obj, 'Tag');
		key = key(1:end-1);
		
		edit = findobj('Tag', key);
		path = get(edit, 'String');
		path = uigetdir(path, 'Choose Directory');
		if isnumeric(path)
			return
		end
		
		% store
		set(edit, 'String', path);
		
	end

	function h_control = createControl_dir(panel, pref, value)
		
		[h_label, labelPanel] = create_label(gui, panel, pref);
		
		h = 24;
		bw = 36;
		bgcol = [1 1 1];
		
		sub = panel.pack([], 'top', h);
		
		h_control = uicontrol(gui.getMainWindow(), ...
			'String', '...', ...
			'Hori', 'Left', ...
			'Tag', [key '_'], ...
			'Callback', @control_browse, ...
			'Style', 'Pushbutton');
		sub.pack(h_control, 'right', 36);
		
		h_control = uicontrol(gui.getMainWindow(), ...
			'String', value, ...
			'BackgroundColor', bgcol, ...
			'Hori', 'Left', ...
			'Tag', key, ...
			'Style', 'Edit');
		sub.pack(h_control, 'right', -1);
		
	end



%% CONTROL TYPE: SERVERS

	function control_add(obj, evt)
		
		key = get(obj, 'Tag');
		key = key(1:end-1);
		
		list = findobj('Tag', key);
		str = get(list, 'string');
		
		path = uigetdir('', 'Browse to the root folder on your system which represents the server. In the next dialog you will name the server.');
		if isnumeric(path)
			return
		end
		path = stTidyFilename(path);
		
		while 1
			name = stInput('medium', ['What is the name of this server?' 10 10 ...
				'This is the server name that will be set in the job files. ' ...
				'When they are opened on another machine, this name can refer ' ...
				'to something different, local to that machine.']);
			if isempty(name)
				return
			end
			if isempty(name{1})
				return
			end
			name = upper(name{1});
			if ~all((name >= 65 & name <= 90) | (name >= 48 & name <= 57) | (name == '_') | (name == '-') | (name == '.'))
				stMessage('Server names must be alphanumeric, plus underscore, dash and period.');
				continue
			end
			break
		end
		
		str{end+1} = [name ' = ' path];
		set(list, 'string', str, 'value', length(str));
		
	end

	function control_del(obj, evt)
		
		key = get(obj, 'Tag');
		key = key(1:end-1);
		
		list = findobj('Tag', key);
		str = get(list, 'string');
		
		if ~isempty(str)
			sel = get(list, 'Value');
			str = str([1:sel-1 sel+1:end]);
			set(list, 'string', str, 'value', max(sel-1, 1));
		end
		
	end

	function h_control = createControl_servers(panel, pref, value)
		
		[h_label, labelPanel] = create_label(gui, panel, pref);
		
		h = 64;
		bgcol = [1 1 1];
		
		sub = panel.pack([], 'top', h);
		right = sub.pack([], 'right', 40);
		
		values = {};
		for v = 1:length(value)
			vv = value{v};
			values{end+1, 1} = [vv{1} ' = ' vv{2}];
		end
		
		h_add = uicontrol(gui.getMainWindow(), ...
			'String', 'Add', ...
			'Callback', @control_add, ...
			'Tag', [key '_'], ...
			'Style', 'pushbutton');
		right.pack(h_add, 'top', 24);
		
		h_del = uicontrol(gui.getMainWindow(), ...
			'String', 'Del', ...
			'Callback', @control_del, ...
			'Tag', [key '_'], ...
			'Style', 'pushbutton');
		right.pack(h_del, 'top', 24);
		
		h_control = uicontrol(gui.getMainWindow(), ...
			'BackgroundColor', bgcol, ...
			'Tag', key, ...
			'String', values, ...
			'Style', 'listbox');
		sub.pack(h_control, 'right', -1);
		
	end



%% LAYOUT ELEMENTS

	function h = createButton(text, callback)
		h = uicontrol(gui.getMainWindow(), ...
			'String', text, ...
			'Callback', callback, ...
			'Style', 'Pushbutton');
	end



end











