function pars = stMergePars(pars, defpars)

% pars = stMergePars(pars, defpars)
%
%   This function is used to merge specific parameter values
%   in "pars" with default parameter values in "defpars".
%   "defpars" should be the parameter cell array returned by
%   the plugin (see Plugin Parameters). "pars" should be a
%   structure with fields named for parameters and the
%   values of those parameters stored in those fields. The
%   return value has the same form as the input structure
%   "pars", but will have had additional fields added to it
%   so that every parameter specified in "defpars" is
%   represented.
%
%
%
% See also: stRevision, stProject, stJob, stPlugin, stMergePars, stUserData, stLoadMovieFrames.


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% Author: Ben Mitch
% Created: 01/03/2011
%
% Because the world is pretty and sunny and
% filled with little rabbits and other beautiful things,
% shame on you if you make it less beautiful. But do as
% you will with this code, the little rabbits will not
% mind.


for n = 1:length(defpars)

	name = defpars{n}.name;
	if ~isfield(pars, name)
		pars.(name) = defpars{n}.value;
	end
	
end

% warn if unrecognised parameters were found
% TODO...


