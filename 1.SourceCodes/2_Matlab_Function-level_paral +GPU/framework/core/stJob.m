% Class "stJob"
%
%   This class represents a tracking "job". That is, a video
%   file, a set of frames to track, a set of methods to run,
%   parametrizations of those methods, and the outputs of
%   those methods (if they have been run). A job can be
%   manipulated at the console, or through the BWTT GUI.
%
%   stJob is primarily a repository for these data. In
%   addition, it provides write locking for the file on disk
%   where these data are stored. Thus, only one instance of
%   stJob can modify the file at once, but multiple
%   instances can read the file.
%
%   stJob provides two interfaces. The "framework side"
%   interface allows the job to be manipulated by the
%   framework (either through the GUI or from the command
%   line). The "method side" interface provides interaction
%   between the job and the processing methods (tracking
%   methods, etc.). A complete list of methods is available
%   if you call "doc stJob" (under See Also).
%
%
%
% See also: stRevision, stProject, stJob, stPlugin, stMergePars, stUserData, stLoadMovieFrames.


%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====





% stJob Methods:
%
%   (Parameters Interface)
%   setRuntimeParameters - set the Runtime Parameters
%   getRuntimeParameters - retrieve the Runtime Parameters
%
%   (Runtime State Interface)
%   setRuntimeState - store data in the Runtime State
%   getRuntimeState - retrieve data from the Runtime State
%   cleanupRuntimeState - discard some or all of Runtime State
%
%   (Results Array Interface)
%   setResults - store results in the Results Array
%   getResults - retrieve results from the Results Array


% Author: Ben Mitch
% Created: 01/03/2011
%
% Because the world is pretty and sunny and
% filled with little rabbits and other beautiful things,
% shame on you if you make it less beautiful. But do as
% you will with this code, the little rabbits will not
% mind.



%
%
% SEARCH FOR "TODO" for jobs that still need completing.
%
%



classdef stJob < handle
	
	
	
	%% EXPIRY
	
	methods (Static)
		
		function t = checkExpiry()
			
			% AS OF 03/07/12, we are going open source, so no
			% expiry code is necessary
% 			t = datenum('01-Jul-2012');
% 			if now > t
% 				error(['This release has expired (Expires On: ' datestr(t) '). Obtain a more recent release from the developers.']);
% 			end
			
		end
		
	end
	
	
	
	%% PROPERTIES
	
	properties (Constant = true, Hidden = true)
		
		CURRENT_VERSION = 4
		
	end
	
	properties (Access = private)
		
		% job state and filename
		%
		% the job starts life in STATE_READONLY, and has no
		% data. when we try to load the job, we will find the
		% job file either under its specified .stJob filename or
		% in its locked .stJob-locked form. if the former, we
		% try to move the file to its locked form and, if
		% successful, we have write lock on the job, and move to
		% STATE_LOCKED. if the latter, we can read the locked
		% file, but remain in STATE_READONLY. in STATE_READONLY,
		% we cannot be modified, but we can make a further call
		% to load(), and try again to move into STATE_LOCKED. in
		% STATE_LOCKED, we can modify the job, and that moves us
		% into STATE_MODIFIED. a call to save(), once
		% successful, will move us back into STATE_LOCKED. to
		% release the job (e.g. if we are about to start batch
		% processing other jobs in a project), we can call
		% release() which will move us from STATE_LOCKED
		% into STATE_READONLY. from there, we have to call
		% load() to get write lock again.
		%
		% summary of states:
		%
		% READONLY w/out filename:
		%   the job is born in this state, but must move
		%   immediately into a LOCKED state or READONLY w/
		%   filename. it doesn't make sense to remain in this
		%   state, because the lack of a filename means the job
		%   is stored only in memory, therefore it is implicitly
		%   locked because nobody else can access it.
		%
		% READONLY w/ filename:
		%   "m_pars" holds the contents of the job file, either
		%   the locked one or the unlocked one, but we do not
		%   have write lock, and therefore cannot modify
		%   anything. the filename will be the locked file if we
		%   ended up in READONLY because somebody else had the
		%   job locked, or in unlocked form if we ended up in
		%   READONLY through release(). in either case, a
		%   further call to load() may lock the job, or may
		%   fail.
		%
		% LOCKED w/out filename:
		%   the job is stored only in memory, is not associated
		%   with a file, therefore we can modify it safely.
		%
		% LOCKED w/ filename:
		%   "m_pars" holds the contents of the locked job file,
		%   and we have write lock, so we can modify. the
		%   filename is the locked form.
		%
		% MODIFIED w/out filename:
		%   the job is stored only in memory and is modified, so
		%   it should be save()d before being deleted.
		%
		% MODIFIED w/ filename:
		%   the job is associated with a file and is modified,
		%   so it should be save()d before being deleted. the
		%   filename is the locked form.
		state
		filename
		
		% this meta data flag is not used, functionally, but
		% lets the caller know if the job is locked elsewhere
		elsewhere
		
		% persistent data - this is what gets stored in the job
		% file. shallow entries in m_data.cache will be removed
		% before the job is save()d.
		m_pars
		m_data
		
		% callbacks are set by the stGUI to redirect user
		% feedback calls to parts of the stGUI. if they are not
		% set, i.e. if stJob is being used at the command-line,
		% stJob does an impoverished form of user feedback.
		callbackObject
		
		% this temporary state is set during run-time
		% processing, and helps to smooth that over
		context
		
		% here we keep the temporary state data for the
		% currently open task
		runtime
		
		% this flag is used to help the caller keep track of the
		% job's state
		invalidated
		
		% this flag is set during taskRun()
		inTaskRun
		
		% we keep a separate copy of the parameters on a "stack"
		% that we can stackPush() and stackPop(). this is
		% helpful whilst setting parameters, so we can set
		% parameters and test them, but easily revert them if
		% desired. it also allows us to discard spurious results
		% sets created during parametrization.
		stack
		
		% instance flags can be set/get to keep track of the
		% context the job is running in
		flags
		
	end
	
	
	
	
	
	
	
	%% STATE MANAGEMENT
	
	properties (Constant = true, Hidden = true)
		
		STATE_READONLY = 1
		STATE_LOCKED = 2
		STATE_MODIFIED = 3
		
	end
	
	methods
		
		function job = stJob(filename, callbackObject)

			% expiry is handled here, since stJob will be p-coded,
			% so since stJob is core to the whole thing, the whole
			% thing is effectively protected.
			stJob.checkExpiry();
			
			% stJob constructor
			%
			% job = stJob(filename)
			%
			% construct a job (e.g. at the command-line) by
			% loading the specified stJob file.
			
			% job = stJob(filename, callbackObject)
			%
			% this form is used by the GUI and provides a callback
			% object as well. thus, callbacks from the job have a
			% sink to go to. if the callback object is not
			% supplied, the job does its best (spitting out log
			% entries to the command window, for instance).
			
			% the job starts in READONLY, and can be moved out of
			% this state only by the ctor, below, that constructs
			% a new job in memory, or by the load() call, that
			% locks an existing job for writing.
			job.m_pars = [];
			job.filename = '';
			job.state = job.STATE_READONLY;
			job.elsewhere = false;
			job.invalidated = true;
			job.inTaskRun = false;
			job.flags = {};
			job.runtime = {};
			
			% callbackObject
			if nargin == 2
				job.callbackObject = callbackObject;
				job.flagSet('gui');
			else
				job.callbackObject = [];
			end
			
			% handle argument
			if nargin < 1
				error('must supply job or video filename');
			end
			
			% switch on file extension
			switch lower(stProcessPath('justExtension', filename))
				
				case 'stjob'
					
					% the job is being created from a file
					job.load(filename);
					
				case 'avi'
					
					% check file exists
					if ~exist(filename)
						error(['file "' filename '" not found']);
					end
					
					% header
					header = [];
					header.version = stJob.CURRENT_VERSION;
					header.uid = stGenerateUID();
					header.creator = stGetUserName();
					header.created = now;
					header.modified = now;
					header.saved = [];
					
					% video
					video = [];
					video.server = '';
					video.filename = '';
					video.info = [];
					
					% frames
					frames = [];
					frames.first = 1;
					frames.last = 1;
					frames.step = 1;
					frames.initial = 1;
					
					% parameters
					pars = struct();
					
					% userdata
					userdata = struct();
					
					% internal cache
					cache = struct();
					
					% job pars (these are loaded immediately)
					m_pars = [];
					m_pars.header = header;
					m_pars.video = video;
					m_pars.frames = frames;
					m_pars.pars = pars;
					m_pars.userdata = userdata;
					m_pars.cache = cache;
					job.m_pars = m_pars;
					
					% job data (these are loaded JIT)
					m_data = [];
					m_data.tasks = {};
					m_data.outputs = struct();
					m_data.cache = struct();
					job.m_data = m_data;
					
					% "meta data" was once implemented, to support
					% viewIndex and mmPerPixel and recordedFrameRate,
					% but i never quite finished it, so i took it out
					% to keep things tidy for now. we may reinstate it
					% later.
					%m_pars.meta = struct;
					
					% the job is being created in memory so it is
					% effectively locked and can be modified
					job.state = job.STATE_LOCKED;
					
					% set filename
					job.setVideoFilename(filename);
					
				otherwise
					
					error(['filename extension "' stProcessPath('justExtension', filename) '" was not recognised']);
					
			end
			
		end
		
		function delete(job)
			
			% (housekeeping) destructor
			
			% we're being deleted, so the job.callbackObject may
			% not be ready for us anymore
			job.callbackObject = [];
			
			% warn on job state
			switch job.state
				
				case job.STATE_LOCKED
					
					% release job
					job.release();
					
				case job.STATE_MODIFIED
					
					warning('modified job was deleted when in MODIFIED state - cannot be saved automatically, and data may have been lost');
					
			end
			
		end
		
		function release(job)
			
			% a job that is in LOCKED state can be moved to the
			% READONLY state to "release the write lock". this is
			% useful, because it means other stJob instances
			% (including in other instances of Matlab) can now
			% successfully get write lock on the job file. once
			% we've moved to READONLY, we have to call load() to
			% attempt to get back into the LOCKED state, because
			% we are then competing with those other potential
			% instances for the write lock.
			
			% 08 Jul 2011: in addition, i added today that it also
			% is the trial mechanism for discarding the JIT load
			% data ("m_data"). this seems like a sensible place to
			% do it, since this can be called after processing is
			% complete (during execution), or it can be called by
			% the GUI as it sees fit (there is no harm in
			% releasing a job, any time). NB: this does NOT mean
			% that the JIT data must be loaded when the job is
			% locked for writing, no no no.
			
			% switch on job state
			switch job.state
				
				case job.STATE_MODIFIED
					
					% must save first
					error('cannot set job to READONLY - it is in state MODIFIED (must save() first)');
					
				case job.STATE_LOCKED
					
					% unlock job file
					if ~isempty(job.filename)
						
						% attempt to move
						lockfilename = [job.filename '-locked'];
						success = movefile(lockfilename, job.filename);
						
						% check
						if ~success
							error('failed to release job - it is still in state LOCKED');
						end
						
						% ok
						job.changeState(job.STATE_READONLY);
						
						% discard JIT data
						job.offerJITdiscard();
						
					else
						
						error('failed to release job - it does not have a job filename');
						
					end
					
				case job.STATE_READONLY
					
					% no action required
					
			end
			
		end
		
		function stampOnLock(job)
			
			% attempt to release a lock that is not held by this
			% job object. this is only to be used when the user is
			% SURE that the job is not really locked, but has been
			% left in a locked state as a result of a matlab
			% crash.
			if job.state ~= job.STATE_READONLY
				error('cannot stamp on lock (job is not in READONLY state)');
			end
			if ~job.elsewhere
				error('cannot stamp on lock (job is not locked elsewhere)');
			end
			
			% prepare
			filename = job.filename;
			lockfilename = [filename '-locked'];
			
			% check
			if exist(filename, 'file')
				error('cannot stamp on lock (unlocked job file is present)');
			end
			if ~exist(lockfilename, 'file')
				error('cannot stamp on lock (locked job file is not present)');
			end
			
			% do the job
			success = movefile(lockfilename, filename);
			
			% if unsuccessful
			if ~success
				error(['failed to unlock file "' lockfilename '"']);
			end
			
			% update
			job.elsewhere = false;
			job.invalidate();
			
		end
		
		function lock(job)
			
			% attempt to lock job for writing. this is only
			% possible if the job is in the READONLY state. it
			% will only succeed if no other instance has the job
			% locked for writing.
			if job.state ~= job.STATE_READONLY
				error('cannot lock job for writing (is not in READONLY state)');
			end
			
			% and we must have a filename
			if isempty(job.filename)
				error('cannot lock job for writing (does not have a filename)');
			end
			
			% prepare
			filename = job.filename;
			lockfilename = [filename '-locked'];
			
			% if file exists
			if exist(filename)
				
				% mark elsewhere
				job.elsewhere = false;
				
				% try to lock file
				success = movefile(filename, lockfilename);
				
				% if unsuccessful
				if ~success
					
					% failed to lock file
					error(['failed to lock file "' filename '"']);
					
				end
				
			else
				
				% mark elsewhere
				job.elsewhere = true;
				
				% fail
				error(['cannot lock job for writing (is locked by another instance)']);
				
			end
			
			% load file - if this fails, the job (or other file,
			% in fact) may be left in the locked state, so we've
			% got to try and put it back, but we ignore the return
			% value in case we fail.
			try
				
				% file may be upgraded - we have write lock.
				job.jobfile_load(lockfilename, true);
				
			catch err
				
				success = movefile(lockfilename, filename);
				rethrow(err);
				
			end
			
			% raise event
			job.changeState(job.STATE_LOCKED);
			
		end
		
		function save(job, filename, forceSave)
			
			% default
			if nargin < 2
				filename = '';
			end
			
			% default
			if nargin < 3
				forceSave = false;
			end
			
			% this moves a job from the MODIFIED state into the
			% LOCKED state, (from where it can be moved to the
			% READONLY state, if desired, using release()). if the
			% job already has an associated filename, you needn't
			% supply one; if not, we'll fail unless you help us
			% out with that.
			if job.state ~= job.STATE_MODIFIED && ~forceSave
				
				% offer JIT discard, at least
				job.offerJITdiscard();
				
				% silently, do nothing
				return
				
			end
			
			% was a filename passed?
			if ~isempty(filename)
				
				% handle passed filename
				ext = stProcessPath('justExtension', filename);
				if ~strcmp(ext, 'stJob')
					error('filename must have an .stJob extension');
				end
				
				% fail if file exists in either form
				if exist(filename)
					error(['cannot save to "' filename '", file exists']);
				end
				if exist([filename '-locked'])
					error(['cannot save to "' filename '", file exists (locked)']);
				end
				
			else
				
				% handle existing filename
				if isempty(job.filename)
					error('must supply a filename to save()');
				end
				filename = job.filename;
				
			end
			
			% invalidate shallow cache items
			job.cachePurge('s');
			
			% set saved date
			job.m_pars.header.saved = now;
			
			% treat slightly differently depending on whether
			% m_data has been loaded, or not
			if isempty(job.m_data)
				
				% in this case, the file MUST exist, since the only
				% way we can end up with an empty job.m_data is if
				% we loaded from a file already, and opted not to
				% load the m_data variable
				lockfilename = [filename '-locked'];
				if ~exist(lockfilename, 'file')
					error('error during save: m_data not loaded, but file not found');
				end
				
				% save just m_pars
				m_pars = job.m_pars;
				save(lockfilename, '-append', 'm_pars');
				
			else
				
				% in this case, the file MAY NOT exist, since this
				% may be the first time the file is being persisted.
				lockfilename = [filename '-locked'];
				
				% update current task state cache
				job.refreshCache();
				
				% save both variables
				m_pars = job.m_pars;
				m_data = job.m_data;
				save(lockfilename, 'm_pars', 'm_data');
				
			end
			
			% ok, saved, move to LOCKED state and lay in filename
			job.filename = filename;
			job.changeState(job.STATE_LOCKED);
			
			% offer JIT discard, at least
			job.offerJITdiscard();
			
		end
		
		function discard(job)
			
			% (framework side) discard changes to the job
			
			% discard() moves the job from MODIFIED to LOCKED, so
			% that it can be unlocked at deletion without saving
			% the changes. it is also used to indicate that the
			% data has been put back into its unmodified state, in
			% stackPop().
			if job.state == job.STATE_MODIFIED
				job.changeState(job.STATE_LOCKED);
			end
			
		end
		
		function [state, elsewhere] = getState(job)
			
			% (framework side) get "job state" information
			
			% return information
			state = job.state;
			elsewhere = job.elsewhere;
			
		end
		
		function b = isLocked(job)
			
			% (framework side) get "job locked" information
			
			b = job.state ~= job.STATE_READONLY;
			
		end
		
		function b = isModified(job)
			
			% (framework side) get "job modified" information
			
			b = job.state == job.STATE_MODIFIED;
			
		end
		
		function b = isInMemory(job)
			
			% (framework side) get "job in memory" information
			
			b = isempty(job.filename);
			
		end
		
		function filename = getFilename(job)
			
			% (framework side) get "job filename" information
			
			filename = job.filename;
			
		end
		
		function invalidated = isInvalidated(job)
			
			% (framework side) get "job invalidated" information
			
			% NB: this call also marks the job as "not
			% invalidated", since the caller will be expected to
			% respond by handling the invalidated job.
			invalidated = job.invalidated;
			job.invalidated = false;
			
		end
		
		function invalidate(job)
			
			% (framework side) mark job as invalidated
			
			% the job is marked as invalidated whenever its
			% nature has changed such that its display in the job
			% list will be affected. strictly, the job shouldn't
			% be keeping track of this, since it has no concept of
			% "job list" (that belongs to the GUI framework).
			% still, it's easy to implement it here. then, when
			% the framework has made all the changes to the job
			% that it intends to (i.e. when the user event is
			% exiting), all jobs that are marked as invalidated
			% have their display in the job list refreshed. thus,
			% all graphical changes are deferred until the end,
			% avoiding constantly changing appearance while tasks
			% are being executed.
			
			job.invalidated = true;
			
			% if in taskRun(), raise event immediately
			if job.inTaskRun
				job.raiseEvent('jobInvalidated');
			end
			
		end
		
	end
	
	
	
	%% STATE MANAGEMENT (PRIVATE SUBROUTINES)
	
	methods (Access = private)
		
		function changeState(job, state)
			
			if state == job.state
				return
			end
			
			job.state = state;
			
			% raise event
			job.invalidate();
			
		end
		
		function load(job, filename)
			
			% this call comes only when the job is initially being
			% created, and we only read the job (i.e. we don't
			% lock it for writing). if writing is required, a call
			% to lock() will be required first.
			if job.state ~= job.STATE_READONLY
				error('cannot load (job is not in READONLY state)');
			end
			
			% prepare
			lockfilename = [filename '-locked'];
			
			% if file exists
			if exist(filename)
				
				% file may be upgraded - this is, strictly speaking,
				% risky, since another user might access the file as
				% we are writing it. in practice, this is so
				% unlikely (since projects will be long to single
				% users, and job file upgrades will themselves be
				% rare), that i have allowed this. it is practical,
				% because it means that all job files get upgraded
				% the first time they are loaded (unless they
				% actually _are_ already locked elsewhere).
				job.jobfile_load(filename, true);
				
			elseif exist(lockfilename)
				
				% file may not be upgraded - it is locked elsewhere.
				job.jobfile_load(lockfilename, false);
				
				% mark elsewhere
				job.elsewhere = true;
				
			else
				
				error(['file not found "' filename '"']);
				
			end
			
			% lay in filename
			job.filename = filename;
			
		end
		
		function modify(job)
			
			% this call MUST always be made before writing to the
			% m_pars field, or reading from the m_pars field with
			% intent to modify and write back. generally, this
			% call is made at the top of any function that will
			% modify the m_pars field.
			
			% if the job is in the READONLY state, an attempt is
			% made to lock the job (move it to the LOCKED state).
			% if this call fails, the call to lock() raises an
			% error.
			if job.state == job.STATE_READONLY
				
				% attempt to lock
				job.lock();
				
			end
			
			% the job is now either in the LOCKED or MODIFIED
			% state. here, we ensure it is in the MODIFIED state.
			job.changeState(job.STATE_MODIFIED);
			
			% we also record the time that the job was last
			% modified.
			job.m_pars.header.modified = now;
			
			% as this function exits, the job is in the MODIFIED
			% state, always.
			
		end
		
	end
	
	
	
	%% JOB FILE INTERACTION
	
	methods (Access = private)
		
		function jobfile_load(job, filename, mayUpgradeFile)
			
			% check file exists
			if ~exist(filename, 'file')
				error('stJob:jobFileNotFound', ['job file "' filename '" not found']);
			end
			
			% load m_pars
			w = warning('off', 'MATLAB:load:variableNotFound');
			loadData = load(filename, '-mat', 'm_pars');
			warning(w);
			
			% upgrade
			if ~isfield(loadData, 'm_pars')
				
				% if "m_pars" is not present, must be v1, upgrade it
				job.log(['upgrading job file v1 --> v2 "' filename '"']);
				
				% if not allowed, throw error
				if ~mayUpgradeFile
					error('job cannot be upgraded - it is locked by another user - so it will not be loaded');
				end
				
				% version 1 file has one variable in it, called "data".
				loadData = load(filename, '-mat');
				if ~isfield(loadData, 'data')
					error('stJob:jobFileCorrupt', ['job file "' filename '" corrupt (or not a v1 job file)']);
				end
				if ~isfield(loadData.data, 'header')
					error('stJob:jobFileCorrupt', ['job file "' filename '" corrupt (or not a v1 job file)']);
				end
				
				% and it contains m_pars and m_data, both, as one
				% big structure array. (this is why we upgraded from
				% v1 to v2, so that we could load the m_data part JIT).
				m_pars = loadData.data;
				m_data = [];
				m_data.tasks = loadData.data.tasks;
				m_pars = rmfield(m_pars, 'tasks');
				m_data.outputs = loadData.data.output; % note field renamed "output" to "outputs" between v1 --> v2, also
				m_pars = rmfield(m_pars, 'output');
				m_data.cache = loadData.data.cache;
				m_pars = rmfield(m_pars, 'cache');
				
				% we also added m_pars.cache, as an internal cache
				% for the job object (it is used, to begin with, to
				% cache "current task state")
				m_pars.cache = struct();
				
				% pre-release versions of 0.5 may have had a field
				% "data.meta" in some cases, so this may be hanging
				% around in old job files. we take the opportunity
				% to purge it.
				if isfield(m_pars, 'meta')
					m_pars = rmfield(m_pars, 'meta');
				end
				
				% mark upgrade to version 2
				m_pars.header.version = 2;
				
				% store cache data in m_pars
				job.m_pars = m_pars;
				job.m_data = m_data;
				job.refreshCache();
				m_pars = job.m_pars;
				
				% upgrade job file
				save(filename, 'm_pars', 'm_data');
				
				% construct an "loadData" variable that looks like it has
				% been loaded from a version 2 file
				loadData = [];
				loadData.m_pars = m_pars;
				
			end
			
			% extract m_pars from load structure
			m_pars = loadData.m_pars;
			
			% check version
			if m_pars.header.version > stJob.CURRENT_VERSION
				error('stJob:jobFileTooNew', ...
					sprintf('Job (version %i) was created by a later version of the BWTT so it cannot be loaded.', ...
					m_pars.header.version));
			end
			
			% check version
			if m_pars.header.version < stJob.CURRENT_VERSION && ~mayUpgradeFile
				error('stJob:jobCannotBeUgraded', ...
					sprintf('Job (version %i) cannot be upgraded because it is locked by another user so it cannot be loaded.', ...
					m_pars.header.version));
			end
			
			% upgrade
			if m_pars.header.version == 2
				
				% results fields "objects" and "whiskers" were
				% renamed to "object" and "whisker", so that the
				% results fields "object/snout/whisker" were
				% consistent with the pipeline names
				% "object/snout/whisker".
				job.log(['upgrading job file v2 --> v3 "' m_pars.header.uid '"']);
				m_pars.header.version = 3;
				
				% load m_data
				loadData = load(filename, '-mat', 'm_data');
				m_data = loadData.m_data;
				
				% for each task
				for t = 1:length(m_data.tasks)
					for r = 1:length(m_data.tasks{t}.results)
						result = m_data.tasks{t}.results{r};
						if isfield(result, 'objects')
							result.object = result.objects;
							result = rmfield(result, 'objects');
						end
						if isfield(result, 'whiskers')
							result.whisker = result.whiskers;
							result = rmfield(result, 'whiskers');
						end
						m_data.tasks{t}.results{r} = result;
					end
				end
				
				% update file
				save(filename, 'm_pars', 'm_data');
				
			end
			
			% upgrade
			if m_pars.header.version == 3
				
				% specification was changed so that the job file
				% must _always_ have the m_pars.video.info field
				% completely filled (in previous versions, it could
				% be left empty and filled JIT when it was
				% requested).
				job.log(['upgrading job file v3 --> v4 "' m_pars.header.uid '"']);
				m_pars.header.version = 4;
				
				% load video info
				job.m_pars = m_pars;
				[firstFrame, info] = stLoadMovieFrames(job.getVideoFilename(), 1);
				m_pars.video.info = info;
				
				% update file
				save(filename, '-append', 'm_pars');
				
			end
			
			% initialise job
			job.m_pars = m_pars;
			job.m_data = []; % deferred for JIT loading
			
		end
		
		function jobfile_load_JIT(job, filename)
			
			% check file exists
			if ~exist(filename, 'file')
				error('stJob:jobFileNotFound', ['job file "' filename '" not found']);
			end
			
			% load m_data
			job.log(['$loading JIT data "' job.getUID() '"...']);
			loadData = load(filename, '-mat', 'm_data');
			job.m_data = loadData.m_data;
			
			% this causes a display change as well...
			job.invalidate();
			
		end
		
	end
	
	
	
	%% DATA INTERFACE
	
	% since "m_data" has special logistics (that is, it is
	% loaded JIT from the job file when required), we access
	% it, even internally, only through this interface. thus,
	% this interface defines the logistics, and the rest of
	% stJob doesn't have to worry about them.
	
	% NB: the "set_" methods in this interface all modify
	% the job. however, they do not call job.modify()
	% themselves, though that might seem convenient. this is
	% because the caller must call job.modify() before it
	% reads the data, which it then modifies, and passes
	% back through one of these calls. to reiterate, it is
	% the responsibility of the caller of each of these
	% methods to call job.modify() as appropriate.
	
	methods (Access = private)
		
		%% JIT load/discard
		
		function m_data_JIT_load(job)
			if isempty(job.m_data)
				
				% m_data is empty. this means, at first, that we
				% have loaded the job from file, and that we did not
				% load the m_data when we did that. the job may
				% since have had its pars modified (so it may be in
				% the MODIFIED state) or it may just have been
				% locked for writing (so it may be in the LOCKED
				% state). or, it may not have been changed, and may
				% still be in the READONLY state. thus, the file
				% with the "m_data" variable in it may be called
				% filename or lockfilename, at this point. what is
				% NOT possible is for the file to be absent - m_data
				% can only ever start empty if we load from the job
				% file, so the job file must be there somewhere.
				%
				% caveat: the m_data can also end up empty if the
				% m_data was "discarded" (i.e. the memory was
				% freed). however, discard()ing can only be done
				% once the job has been serialized, so, again, there
				% MUST be a job file on disc.
				
				% get filenames
				if isempty(job.filename)
					error('stJob:NoFilenameAtJITLoad', 'internal error: job.filename is empty at m_data_JIT_load()');
				end
				filename = job.filename;
				lockfilename = [job.filename '-locked'];
				
				% load from either file
				if exist(filename, 'file')
					job.jobfile_load_JIT(filename);
				elseif exist(lockfilename, 'file')
					job.jobfile_load_JIT(lockfilename);
				else
					error('stJob:NoFileAtJITLoad', 'internal error: no job file was found on disc at m_data_JIT_load()');
				end
				
			end
		end
		
		function offerJITdiscard(job)
			
			% JIT data, if loaded, can be discarded if a job
			% filename exists (the job has been saved) and the job
			% state is not modified (i.e. is READONLY or LOCKED).
			% we save on memory by discarding the JIT data
			% whenever appropriate. really, the only time not to
			% discard it is when the job is still the "displayed"
			% job, since at all other times the user is probably
			% not about to read or write the job's JIT data.
			if ~isempty(job.m_data) && ~isempty(job.filename) && job.state ~= job.STATE_MODIFIED
				
				% discard JIT data
				job.log(['$discarding JIT data "' job.getUID() '"...']);
				job.refreshCache();
				job.m_data = [];
				
				% raise event
				job.invalidate();
				
			end
			
		end
		
		function refreshCache(job)
			
			% before we discard the m_data, or (equivalently) save
			% the job, we cache some information. that way, when
			% the caller only wants this information to display or
			% query the state of the job, we don't have to do the
			% JIT loading to find it out.
			
			% store current task state
			item = [];
			[item.state, item.errors, item.pipelines] = job.getCurrentTaskState();
			job.m_pars.cache.currentTaskState = item;
			
			% store current task hash
			item = [];
			[item.hash] = job.getCurrentTaskHash();
			job.m_pars.cache.currentTaskHash = item;
			
		end
		
		%% tasks
		
		function tasks = get_tasks(job)
			job.m_data_JIT_load();
			tasks = job.m_data.tasks;
		end
		
		function task = get_task(job, taskIndex)
			job.m_data_JIT_load();
			if taskIndex < 1 || taskIndex > length(job.m_data.tasks)
				error('invalid taskIndex');
			end
			task = job.m_data.tasks{taskIndex};
		end
		
		function count = get_task_count(job)
			job.m_data_JIT_load();
			count = length(job.m_data.tasks);
		end
		
		function set_tasks(job, tasks)
			job.m_data_JIT_load();
			job.m_data.tasks = tasks;
		end
		
		function set_task(job, taskIndex, task)
			job.m_data_JIT_load();
			if taskIndex < 1 || taskIndex > length(job.m_data.tasks)
				error('invalid taskIndex');
			end
			job.m_data.tasks{taskIndex} = task;
		end
		
		function taskIndex = append_task(job, task)
			job.m_data_JIT_load();
			job.m_data.tasks{end+1} = task;
			taskIndex = length(job.m_data.tasks);
		end
		
		function delete_last_task(job)
			job.m_data_JIT_load();
			if length(job.m_data.tasks) == 0
				error('cannot delete last task - no tasks present');
			end
			job.m_data.tasks = job.m_data.tasks(1:end-1);
		end
		
		%% outputs
		
		function outputs = get_outputs(job)
			job.m_data_JIT_load();
			outputs = job.m_data.outputs;
		end
		
		function output = get_output(job, key)
			job.m_data_JIT_load();
			if isfield(job.m_data.outputs, key)
				output = job.m_data.outputs.(key);
			else
				output = [];
			end
		end
		
		function set_outputs(job, outputs)
			job.m_data_JIT_load();
			job.m_data.outputs = outputs;
		end
		
		function set_output(job, key, value)
			job.m_data_JIT_load();
			job.m_data.outputs.(key) = value;
		end
		
		%% cache
		
		% for cache, all the calls are restricted to the section
		% "%% CACHE", so i just topped all those calls with a
		% call to the JIT load function, rather than piss
		% around. this could be tidied up once this
		% functionality has bedded in (time of writing, 8 Jul
		% 2011).
		
	end
	
	
	
	
	%% CACHE
	
	methods
		
		function cacheSet(job, key, value, flags)
			
			% Store an item in the cache
			%
			% job.cacheSet(key, value, flags)
			%
			%   A cache is maintained for each pipeline/method
			%   combination. Its management is up to the
			%   individual method itself. This call stores a value
			%   in the cache.
			%
			%   There are two levels of cache - deep and shallow.
			%   Shallow storage lasts until the job is unloaded;
			%   deep storage persists over the save/load cycle.
			%   Deep storage should be avoided, unless it really
			%   speeds things up, because it makes the job file
			%   larger.
			%
			%   The cache accessed by this call is determined by
			%   the current job context, as set through
			%   setContext(). If there is no context (e.g.
			%   following clearContext(), this call will fail).
			%
			% Recognised flags are:
			%
			%   d (deep): Do not purge on save(). Use this
			%     judiciously, since it makes the job file larger.
			%
			%   v (video-dependent): Purge on setVideoFilename().
			%	    An example is the bgImage item used by ppBg.
			%
			%   p (pipeline-independent): Caches for each pipeline
			%	    are coincident. If this flag is not set, the cache
			%	    for this particular method/key combination is
			%	    distinct in each pipeline. You do not need to
			%	    pass this flag to cacheGet(), in either case,
			%	    since a cache item can only be present with
			%	    pipeline-specific or non-pipeline-specific
			%	    resolution, not both at once.
			%
			% See also: cacheSet, cacheGet, cachePurge, cacheReport
			
			% call JIT load
			job.m_data_JIT_load();
			
			% get what we need
			context = job.getContext();
			
			% check flags
			if nargin < 5
				flags = '';
			end
			if ~ischar(flags) || sum(size(flags)>1) > 1 || any(flags ~= 'd' & flags ~= 'v' & flags ~= 'p')
				error('unrecognised flags in call to cacheSet()');
			end
			
			% add shallow flag
			if ~any(flags == 'd')
				flags = [flags 's'];
			end
			
			% create cache data
			item = [];
			item.context = job.context;
			item.key = key;
			item.value = value;
			item.flags = flags;
			
			% no need to get permission to modify, or to mark the
			% job as modified, since cache data can be assumed to
			% be discardable, at a pinch, and we may as well not
			% cause trouble for read-only clients, or cause the
			% job to be locked unneccessarily.
			
			% compute cache key
			if any(flags == 'p')
				
				cachekey = [context.method '_' key];
				cachekeyalt = [context.pipeline '_' context.method '_' key];
				
			else
				
				cachekey = [context.pipeline '_' context.method '_' key];
				cachekeyalt = [context.method '_' key];
				
			end
			
			% cannot have same key used at both resolution levels
			if isfield(job.m_data.cache, cachekeyalt)
				error('cache item already exists with different resolution');
			end
			
			% store
			job.m_data.cache.(cachekey) = item;
			
		end
		
		function value = cacheGet(job, key)
			
			% Get an item from the cache
			%
			% value = job.cacheGet(key)
			%
			%   Retrieve the value stored in the cache against the
			%   current key, and under the current context. See
			%   cacheSet() for details.
			%
			% See also: cacheSet, cacheGet, cachePurge, cacheReport
			
			% call JIT load
			job.m_data_JIT_load();
			
			% get what we need
			context = job.getContext();
			
			% get cache key
			cachekey1 = [context.pipeline '_' context.method '_' key];
			cachekey2 = [context.method '_' key];
			
			% if it exists
			if isfield(job.m_data.cache, cachekey1)
				value = job.m_data.cache.(cachekey1).value;
			elseif isfield(job.m_data.cache, cachekey2)
				value = job.m_data.cache.(cachekey2).value;
			else
				value = [];
			end
			
		end
		
		function cachePurge(job, flag)
			
			% Purge the cache
			%
			% job.cachePurge(flag)
			%
			%   The cache can be purged on different bases, as
			%   follows, based on the passed flag.
			%
			% s: Purge shallow items (on save())
			% v: Purge video-dependent items (on setVideoFilename())
			%
			% See also: cacheSet, cacheGet, cachePurge, cacheReport
			
			% call JIT load
			job.m_data_JIT_load();
			
			% get list of cache items
			fn = fieldnames(job.m_data.cache);
			
			% for each
			for f = 1:length(fn)
				item = job.m_data.cache.(fn{f});
				if any(item.flags == flag)
					job.m_data.cache = rmfield(job.m_data.cache, fn{f});
				end
			end
			
		end
		
		function report = cacheReport(job)
			
			% Get cache report for display
			%
			% report = job.cacheReport()
			%
			%   "report" is a string summarising the contents of
			%   the cache. This function is used by the display()
			%   function, when preparing a summary of the job for
			%   display.
			%
			% See also: cacheSet, cacheGet, cachePurge, cacheReport
			
			% call JIT load
			job.m_data_JIT_load();
			
			% get list of cache items
			fn = fieldnames(job.m_data.cache);
			
			% collate
			count_d = 0;
			count_s = 0;
			bytes_d = 0;
			bytes_s = 0;
			
			% for each
			for f = 1:length(fn)
				item = job.m_data.cache.(fn{f});
				w = whos('item');
				if any(item.flags == 'd')
					count_d = count_d + 1;
					bytes_d = bytes_d + w.bytes;
				else
					count_s = count_s + 1;
					bytes_s = bytes_s + w.bytes;
				end
			end
			
			% display
			report = '';
			report = [report '  Deep ' sprintf('%i items (%s)', count_d, formatBytes(bytes_d)) 10];
			report = [report '  Shallow ' sprintf('%i items (%s)', count_s, formatBytes(bytes_s)) 10];
			
		end
		
	end
	
	
	
	
	%% DEBUG
	
	% TODO: these methods are not required long-term
	
	methods
		
		function taskAppend(job, task)
			
			% Append a task to the job
			%
			% job.taskAppend(task)
			%
			%   This function is only used by
			%   stUpgradeProject_v4_v5(), and can be removed once
			%   all users have upgraded their version 4 projects.
			%
			%   Suggested removal date: 1st Jan 2013
			
			job.modify();
			tasks = job.get_tasks();
			tasks{end+1} = task;
			job.set_tasks(tasks);
			
		end
		
	end
	
	
	
	%% STACK
	
	methods
		
		function stackPush(job)
			
			% Push the job state onto the stack.
			%
			% job.stackPush()
			%
			%   This function is used to quickly store the job
			%   state so that a task can be test-run, and then the
			%   job state restored.
			%
			% See also: stackPop
			
			% don't need modify permission, no change made to "data"
			job.stack.tasks = job.get_tasks();
			job.stack.pars = job.m_pars.pars;
			job.stack.state = job.state;
			
		end
		
		function stackPop(job, keepPars)
			
			% Pop the job state from the stack.
			%
			% job.stackPop()
			%
			%   This function is used to restore the job state
			%   previously stored using stackPush().
			%
			% See also: stackPush
			
			% if keep pars, pass current pars to the stack again,
			% so that they get popped back without change
			if nargin == 2 && keepPars
				
				job.stack.pars = job.m_pars.pars;
				unmodify = false;
				
			else
				
				% if we're not keeping the pars, we can revert the
				% modified state as well
				unmodify = true;
				
			end
			
			% get permission to modify
			job.modify();
			
			% pop
			job.set_tasks(job.stack.tasks);
			job.m_pars.pars = job.stack.pars;
			
			% unmodify
			if unmodify && job.stack.state ~= job.STATE_MODIFIED;
				job.discard();
			end
			
		end
		
	end
	
	
	
	
	%% FLAGS
	
	methods
		
		function flagSet(job, flag, state)
			
			% Set the state of a flag
			%
			% job.flagSet(flag, state)
			%
			%   Set the state of the flag "flag" (if "state" is not
			%   specified, it defaults to TRUE). Recognised flags
			%   are listed below.
			%
			% "unattended": If set, the job is being processed
			%   without user supervision. In this case, when user
			%   input is needed, the job should fail silently so
			%   that other jobs in the job list still get
			%   computed.
			%
			% "gui": If set, the job is being processed within the
			%   BWTT GUI. Processing methods can behave
			%   differently in the presence of this flag, for
			%   instance by popping up a GUI dialog for the
			%   setting of transient parameters.
			%
			% See also: flagSet, flagClear, flagIsSet
			
			if nargin < 3
				state = true;
			end
			
			if ~ischar(flag) || size(flag, 1) ~= 1
				error('"flag" must be a string');
			end
			
			switch flag
				
				case {'unattended' 'gui'}
					if state
						if ~any(ismember(job.flags, flag))
							job.flags{end+1} = flag;
						end
					else
						job.flags = job.flags(~ismember(job.flags, flag));
					end
					
				otherwise
					error(['unrecognised flag "' flag '"']);
					
			end
			
		end
		
		function flagClear(job, flag)
			
			% Clear the state of a flag
			%
			% job.flagClear(flag)
			%
			%   Unset the flag "flag". For a list of valid flags,
			%   see flagSet().
			%
			% See also: flagSet, flagClear, flagIsSet
			
			job.flagSet(flag, false);
			
		end
		
		function has = flagIsSet(job, flag)
			
			% Return the state of a flag
			%
			% isSet = job.flagIsSet(flag)
			%
			%   Returns true if the specified flag is set on the
			%   job, and false otherwise. For a list of valid
			%   flags, see flagSet().
			%
			% See also: flagSet, flagClear, flagIsSet
			
			if ~ischar(flag) || size(flag, 1) ~= 1
				error('"flag" must be a string');
			end
			
			has = any(ismember(job.flags, flag));
			
		end
		
	end
	
	
	
	
	%% DISPLAY
	
	methods (Hidden = true)
		
		function report = getInfo(job)
			
			% Retrieve information about the job
			%
			% report = job.getInfo()
			%
			%   "report" is a string containing summary
			%   information about the job.
			
			% return value
			report = '';
			
			% read data
			header = job.m_pars.header;
			video = job.m_pars.video;
			
			% header
			report = [report 'stJob object' 10];
			report = [report '  Version ' int2str(header.version) 10];
			report = [report '  UID ' header.uid 10];
			report = [report '  Created by ' header.creator 10];
			report = [report '  Created ' datestr(header.created) 10];
			report = [report '  Modified ' datestr(header.modified) 10];
			report = [report '  Saved ' formatDateMayBeNull(header.saved) 10];
			filename = strrep(job.filename, header.uid, '<UID>');
			report = [report '  Filename ' formatMaybeEmpty(filename) 10];
			switch job.state
				case job.STATE_READONLY
					s = 'READONLY';
				case job.STATE_LOCKED
					s = 'LOCKED';
				case job.STATE_MODIFIED
					s = 'MODIFIED';
			end
			report = [report '  State ' s 10];
			report = [report 10];
			
			% video
			report = [report 'Video Data' 10];
			report = [report '  Video Server ' formatMaybeEmpty(video.server) 10];
			report = [report '  Video Filename ' formatMaybeEmpty(video.filename) 10];
			report = [report '  (Locally) ' job.getVideoFilename() 10];
			report = [report '  Video Info ' formatVideoInfo(job.getVideoInfo()) 10];
			report = [report '  Frame Range ' formatFrameData(job.getFrameRange()) 10];
			report = [report 10];
			
			% cache
			report = [report 'Cache State' 10];
			report = [report job.cacheReport()];
			
		end
		
		function display(job)
			
			% Print a summary of the job to the command window
			%
			% display(job)
			%
			%   This is the implementation of the Matlab standard
			%   "display" function.
			
			% eye candy
			line = [repmat('_', [1 60]) 10];
			disp(line)
			
			% data
			disp(job.getInfo());
			
			% eye candy
			disp(line)
			
		end
		
	end
	
	
	
	
	
	
	%% HEADER DATA
	
	methods
		
		% NB: this is a wrapper for the user: internally, we use
		% the underscore convention, but for external functions
		% we use camel case
		
		function header = getHeader(job)
			
			% Return the job header object
			%
			% header = job.getHeader()
			
			header = job.m_pars.header;
			
		end
		
		function uid = getUID(job)
			
			% Return the job UID
			%
			% uid = job.getUID()
			
			uid = job.m_pars.header.uid;
			
		end
		
	end
	
	
	
	
	
	
	
	%% VIDEO DATA
	
	methods
		
		function frames = getVideoFrames(job, frameIndices, flags)
			
			% Retrieve frames from the associated video file
			%
			% frames = job.getVideoFrames(frameIndices, flags)
			%
			%   This call just wraps stLoadMovieFrames(), passing
			%   in job.getVideoFilename() for you.
			%
			% IMPORTANT: this function is NOT intended for use
			% within methods at run-time. it is provided as a
			% convenience wrapper for the command-line user, or
			% for plugin tools, or for output methods, etc. To
			% obtain a frame during pipeline processing, use
			% getRuntimeFrame().
			%
			% Why so? Well, because getRuntimeFrame() returns the
			% current frame in the pipeline, which may have been
			% pre-processed, whereas this function goes back to
			% the video for the raw frame. It also makes it
			% impossible for the job to optimise performance. It's
			% just bad form; don't do it.
			%
			% However, you should use this function, rather than
			% calling stLoadMovieFrames directly, wherever
			% possible, since it protects your code from future
			% changes to the stLoadMovieFrames interface. if you
			% want a _raw_ video frame, rather than a
			% pre-processed frame (e.g. for illustration of the
			% frame to the user, or for parametrisation), _do_ use
			% this function!
			
			if nargin < 3
				flags = '';
			end
			
			% delegate to stLoadMovieFrames
			frames = stLoadMovieFrames(job.getVideoFilename(), frameIndices, flags);
			
		end
		
		function setVideoFilename(job, videoFilename)
			
			% Set (or change) the video filename
			%
			% job.setVideoFilename(videoFilename)
			%
			%   "videoFilename" should be the fully-specified path
			%   to the video file. The filename is unresolved
			%   through the server list, if possible, before being
			%   stored.
			%
			%   The specified video is read when this filename is
			%   set to retrieve information about the video file.
			%   This also causes a purge of video-dependent cache
			%   items.
			
			% check file exists
			if ~exist(videoFilename)
				error(['file "' videoFilename '" not found']);
			end
			
			% tidy filename
			videoFilename = stTidyFilename(videoFilename);
			
			% un-resolve server
			videoServer = '';
			videoServers = stUserData('videoServers!');
			for s = 1:length(videoServers)
				vs = videoServers{s};
				serverPrefix = stTidyFilename(vs{2});
				vl = length(serverPrefix);
				if length(videoFilename) >= (vl+1) && stCompareFilenames(videoFilename(1:vl), serverPrefix) && videoFilename(vl+1) == '/'
					videoServer = vs{1};
					videoFilename = videoFilename(vl+2:end);
					break
				end
			end
			
			% get permission to modify
			job.modify();
			
			% modify video filename
			job.m_pars.video.server = videoServer;
			job.m_pars.video.filename = videoFilename;
			
			% get video info, because this may not be the
			% same video
			[firstFrame, info] = stLoadMovieFrames(job.getVideoFilename(), 1);
			job.m_pars.video.info = info;
			
			% invalidate video-dependent cache items
			job.cachePurge('v');
			
		end
		
		function videoFilename = getVideoFilename(job)
			
			% resolve server
			videoServer = job.m_pars.video.server;
			if ~isempty(videoServer)
				videoServers = stUserData('videoServers!');
				found = false;
				for s = 1:length(videoServers)
					vs = videoServers{s};
					if strcmp(vs{1}, videoServer)
						videoServer = [vs{2} filesep];
						found = true;
						break
					end
				end
				if ~found
					warning(['Video Server "' videoServer '" was not found - video filename is probably incomplete']);
					videoServer = '';
				end
			end
			
			% return complete filename
			videoFilename = stTidyFilename([videoServer job.m_pars.video.filename]);
			
		end
		
		% 		function info = getVideoInfo(job)
		%
		% 			% we don't get permission to modify, here, or mark as
		% 			% modified, since this data can easily be retrieved so
		% 			% need not be saved (and so that read-only access can
		% 			% still get this information, even if it's not there).
		%
		% 			% get the info, if absent
		% 			if isempty(job.m_pars.video.info)
		% 				[firstFrame, info] = stLoadMovieFrames(job.getVideoFilename(), 1);
		% 				job.m_pars.video.info = info;
		% 			end
		%
		% 			% return the info
		% 			info = job.m_pars.video.info;
		%
		% 		end
		
		% MITCH 05/09/11
		% i got rid of this "JIT" collection of video info because
		% there was a need to always have the video info, so that we
		% don't get into some nasty loops when we try to do "Find
		% Missing Videos". i don't see any reason _not_ to obtain
		% the video info when the job is created, so why not have it
		% right from the start. the video info cannot change once
		% the job is created, that's a specification point, so no
		% harm in doing this.
		
		function info = getVideoInfo(job)
			
			% return the info, even if this is empty
			info = job.m_pars.video.info;
			
		end
		
	end
	
	
	
	% 	%% META DATA
	%
	% 	methods
	%
	% 		function [value, source] = getMetaData(job, key)
	%
	% 			% we don't get permission to modify, here, or mark as
	% 			% modified, since this data can easily be retrieved so
	% 			% need not be saved (and so that read-only access can
	% 			% still get this information, even if it's not there).
	%
	% 			% list of recognised meta data fields
	% 			fields = {'viewIndex' 'recordedFrameRate' 'mmPerPixel'};
	%
	% 			% no "key" means return all - this is used by the
	% 			% project during export to store the meta data along
	% 			% with the job
	% 			if nargin == 1
	% 				value = struct();
	% 				for i = 1:length(fields)
	% 					key = fields{i};
	% 					value.(key) = job.getMetaData(key);
	% 				end
	% 				return
	% 			end
	%
	% 			% retrieve data from job
	% 			if isfield(job.m_pars.meta, key)
	% 				source = job.m_pars.meta.(key).source;
	% 				value = job.m_pars.meta.(key).value;
	% 				return
	% 			end
	%
	% 			% retrieve data from stLocal
	% 			if ~isempty(which('stLocal'))
	% 				value = stLocal('getJobMetaData', struct('job', job, 'key', key));
	% 				if ~isempty(value)
	%
	% 					% store
	% 					met = [];
	% 					met.source = 'stLocal';
	% 					met.value = value;
	% 					job.m_pars.meta.(key) = met;
	%
	% 					% return
	% 					source = 'stLocal';
	% 					return
	%
	% 				end
	% 			end
	%
	% 			% generate default value
	% 			source = 'defaultValue';
	% 			switch key
	% 				case 'viewIndex'
	% 					value = [];
	% 				case 'recordedFrameRate'
	% 					value = [];
	% 				case 'mmPerPixel'
	% 					value = [];
	% 				otherwise
	% 					error(['unrecognised meta data field name "' key '"']);
	% 			end
	%
	% 		end
	%
	% 		function setMetaData(job, key, value)
	%
	% 			% list of recognised meta data fields
	% 			fields = {'viewIndex' 'recordedFrameRate' 'mmPerPixel'};
	%
	% 			% if unrecognised, error
	% 			if ~ismember(key, fields)
	% 				error(['unrecognised meta data field name "' key '"']);
	% 			end
	%
	% 			% validate
	% 			if ~isnumeric(value) || ~isscalar(value)
	% 				error(['invalid value for meta data field "' key '"']);
	% 			end
	%
	% 			% validate
	% 			switch key
	% 				case 'viewIndex'
	% 					if ~any(value == [1 2])
	% 						error(['invalid value for meta data field "' key '"']);
	% 					end
	% 				case {'recordedFrameRate' 'mmPerPixel'}
	% 					if value <= 0
	% 						error(['invalid value for meta data field "' key '"']);
	% 					end
	% 			end
	%
	% 			% get permission to modify
	% 			job.modify();
	%
	% 			% store
	% 			met = [];
	% 			met.source = 'setMetaData';
	% 			met.value = value;
	% 			job.m_pars.meta.(key) = met;
	%
	% 		end
	%
	% 	end
	
	
	
	
	%% FRAME RANGE DATA
	
	methods
		
		function frames = getFrameRange(job, key)
			
			if nargin < 2
				key = 'raw';
			end
			
			ff = job.m_pars.frames;
			
			switch key
				
				case 'forward'
					frames = ff.initial : ff.step : ff.last;
					
				case 'backward'
					frames = ff.initial : -ff.step : ff.first;
					
					% if the backward pass is only one frame, we know
					% it's already been computed by the forward pass,
					% so we can skip it
					if isscalar(frames)
						frames = [];
					end
					
				case 'range'
					frames = [ff.first ff.last];
					
				case 'selected'
					truefirst = ff.initial - floor((ff.initial - ff.first) / ff.step) * ff.step;
					frames = truefirst : ff.step : ff.last;
					
				case 'raw'
					frames = ff;
					
				case 'complete'
					frames = ff;
					info = job.getVideoInfo();
					frames.available = info.NumFrames;
					
				case 'all'
					info = job.getVideoInfo();
					frames = 1:info.NumFrames;
					
				case 'available'
					% this is not really part of the frames data, but
					% is obtained from the video info
					info = job.getVideoInfo();
					frames = info.NumFrames;
					
				case 'export'
					% return range that includes all active results
					results = job.getResults('all');
					n = size(results, 2);
					f1 = [];
					for i = 1:n
						if ~isempty(fieldnames(results{i}))
							f1 = i;
							break
						end
					end
					f2 = [];
					if ~isempty(f1)
						for i = n:-1:1
							if ~isempty(fieldnames(results{i}))
								f2 = i;
								break
							end
						end
					end
					frames = f1:f2;
					
				case {'first' 'last' 'step' 'initial'}
					frames = ff.(key);
					
				otherwise
					error('invalid call to getFrameRange()');
					
			end
			
		end
		
		function setFrameRange(job, frames)
			
			% setFrameRange(frames)
			%   set the frame range for the job. various formats
			%   are accepted, as follows.
			%
			% struct:
			%   frames.first
			%   frames.last
			%   frames.step
			%   frames.initial
			%     (fields must be in this order)
			%
			% list:
			%   [first:step:last]
			%     (initial is assumed equal to first listed, step can be -ve)
			%
			% specified:
			%   [first last step initial]
			
			% get video info
			info = job.getVideoInfo();
			
			% decide which format we were passed
			if isstruct(frames)
				
			elseif stIsOfSize(frames, [1 4]) && frames(4) <= frames(2)
				
				frames = struct( ...
					'first', frames(1), ...
					'last', frames(2), ...
					'step', frames(3), ...
					'initial', frames(4) ...
					);
				
			else
				
				initial = frames(1);
				if frames(1) > frames(end)
					frames = fliplr(frames);
				end
				if length(frames) == 1
					step = 1;
				else
					step = diff(frames(1:2));
				end
				
				frames = struct( ...
					'first', frames(1), ...
					'last', frames(end), ...
					'step', step, ...
					'initial', initial ...
					);
				
			end
			
			% validate
			if ~isstruct(frames)
				error('frames must be a structure');
			end
			fn = fieldnames(frames);
			if ~stIdentical(fn, {'first' 'last' 'step' 'initial'}')
				error('frames has the wrong fields');
			end
			for f = 1:length(fn)
				value = frames.(fn{f});
				if ~isscalar(value) || ~isa(value, 'double') || value < 1 || value ~= round(value) || value > info.NumFrames
					error(['frames.' fn{f} ' has an invalid value']);
				end
			end
			available = job.getFrameRange('available');
			if frames.last < frames.first || frames.initial < frames.first || frames.initial > frames.last || ...
					any([frames.first frames.last frames.initial] < 1) || any([frames.first frames.last frames.initial] > available)
				error('cannot set this value of frame(s)');
			end
			
			% get permission to modify
			job.modify();
			
			% modify
			changed = ~stIdentical(job.m_pars.frames, frames);
			job.m_pars.frames = frames;
			
			% invalidate
			if changed
				job.invalidate();
				job.raiseEvent('jobFrameRangeChanged');
			end
			
		end
		
	end
	
	
	
	
	
	
	%% PARAMETERS
	
	% "method parameters" ("parameters") are set in advance of
	% processing by the user, usually through the BWTT GUI
	% (Parameters GUI, or some such). these choices are stored
	% in "job.m_pars.pars", and may be incomplete (i.e. only
	% parameters that the user has changed will be specified
	% herein). when a task is started (see taskStart()) the
	% pars stored in "job.m_pars.pars" are merged with default
	% values supplied by the plugin using stMergePars(), and
	% the resulting "runtime parameters" are stored in the
	% task itself at "task.pars". thus, each task may have
	% different parameter values - those at "job.m_pars.pars"
	% represent the values that will be used by the next task
	% that is run. there are two interfaces to the parameters.
	% the first, "framework-side", modifies "job.m_pars.pars".
	% the second, "method-side", accesses only the "runtime
	% parameters", stored in "task.pars".
	%
	% NB: thus, the method developer only needs to use the
	% single call "getRuntimeParameters()"; the framework will
	% handle all other calls.
	
	methods
		
		% this is the framework-side interface to the method
		% parameters stored in the job. it MUST NOT be used to
		% retrieve "Runtime Parameters" - to do that, use the
		% method-side interface (see below).
		
		function pars = getAllParameters(job)
			
			% (framework side) get all method parameter values
			
			pars = job.m_pars.pars;
			
		end
		
		function setAllParameters(job, pars)
			
			% (framework side) set all method parameter values
			
			% get permission to modify
			job.modify();
			
			% lay in
			job.m_pars.pars = pars;
			
		end
		
		function pars = getParameters(job, pipeline, method, merge)
			
			% (framework side) get method parameter values for a particular method
			
			% cachekey
			cachekey = [pipeline '_' method];
			
			% resolve
			if isfield(job.m_pars.pars, cachekey)
				pars = job.m_pars.pars.(cachekey);
			else
				pars = struct();
			end
			
			% merge
			if nargin == 4 && merge
				pars = stMergePars(pars, stPlugin(method, 'parameters'));
			end
			
		end
		
		function setParameters(job, pipeline, method, pars)
			
			% (framework side) set method parameter values for a particular method
			
			% cachekey
			cachekey = [pipeline '_' method];
			
			% get permission to modify
			job.modify();
			
			% resolve
			job.m_pars.pars.(cachekey) = pars;
			
		end
		
		function clearParameters(job)
			
			% (framework side) clear all method parameter values
			
			% get permission to modify
			job.modify();
			
			% resolve
			job.m_pars.pars = struct();
			
		end
		
		
		
		% this is the method-side interface to the method
		% parameters stored in the job. the call
		% getRuntimeParameters() is the only one that is usually
		% used, by the method that needs its parameter values
		% when it has been asked to "process". however, if a
		% method is running without the "unattended" flag, it
		% may choose to obtain further parameter values from the
		% user at runtime - in this case, these parameter values
		% can be stored in the parameter array for the current
		% task using the setRuntimeParameters() call.
		%
		% this interface can only be used when a context is set;
		% i.e. when the CONTEXT METHODS interface is in use.
		
		function pars = getRuntimeParameters(job)
			
			% method-side parameters interface
			%
			% this call gets the parameters for the
			% pipeline/method combination that is currently in
			% context. it can only be used when a context is set
			% (i.e. when a method is being called through the
			% CONTEXT METHODS interface).
			
			% validate task state
			if job.getCurrentTaskState() ~= stJob.TASK_STARTED
				error('method call invalid - current task is not in STARTED state');
			end
			
			% extract
			context = job.getContext();
			
			% extract
			taskIndex = job.getCurrentTask();
			
			% recover parameters
			cachekey = [context.pipeline '_' context.method];
			task = job.get_task(taskIndex);
			pars = task.pars.(cachekey);
			
		end
		
		function setRuntimeParameters(job, pars)
			
			% method-side parameters interface
			%
			% this call sets the parameters for the
			% pipeline/method combination that is currently in
			% context. it can only be used when a context is set
			% (i.e. when a method is being called through the
			% CONTEXT METHODS interface).
			%
			% NB: usually, there is no need to use this call on
			% this interface, since the parameters are set by the
			% user ahead of processing. however, some methods
			% provide a way for unspecified parameters to be set
			% at run-time (provided the "unattended" flag is not
			% set). in that case, the method may choose to update
			% its runtime parameters using this call, afterwards,
			% to save the user being asked for input again.
			
			% modify
			job.modify();
			
			% validate task state
			if job.getCurrentTaskState() ~= stJob.TASK_STARTED
				error('method call invalid - current task is not in STARTED state');
			end
			
			% extract
			context = job.getContext();
			
			% extract
			taskIndex = job.getCurrentTask();
			
			% lay in parameters
			cachekey = [context.pipeline '_' context.method];
			task = job.get_task(taskIndex);
			task.pars.(cachekey) = pars;
			job.set_task(taskIndex, task);
			
		end
		
	end
	
	
	
	
	
	%% OUTPUT
	
	methods
		
		function clearOutputs(job)
			
			% clear all stored outputs
			%
			% job.clearOutputs()
			%
			% this method clears all previously computed outputs
			% stored in the job. any outputs requested in
			% updateOutput() will, thus, be re-computed.
			%
			% see also: setOutput, getOutput, getOutputs,
			%   updateOutput, getSupplementaryOutputDir
			
			% get permission to modify
			job.modify();
			
			% clear
			job.set_outputs(struct());
			
		end
		
		function setOutput(job, key, value)
			
			% store an output value
			%
			% job.setOutput(key, value)
			%
			% store the output value "value" against the key
			% "key". "value" can be any matlab variable, whilst
			% "key" must be a valid matlab field name.
			%
			% see also: clearOutputs, getOutput, getOutputs,
			%   updateOutput, getSupplementaryOutputDir
			
			% get permission to modify
			job.modify();
			
			% lay in
			job.set_output(key, value);
			
		end
		
		function value = getOutput(job, key)
			
			% retrieve an output value
			%
			% value = job.getOutput(key)
			%
			% retrieve the output value stored against the key
			% "key". "key" must be a valid matlab field name.
			%
			% see also: clearOutputs, setOutput, getOutputs,
			%   updateOutput, getSupplementaryOutputDir
			
			% retrieve
			value = job.get_output(key);
			
		end
		
		function outputs = getOutputs(job)
			
			% retrieve all stored outputs
			%
			% outputs = job.getOutputs()
			%
			% retrieve all previously stored output key/value
			% pairs.
			%
			% see also: clearOutputs, setOutput, getOutput,
			%   updateOutput, getSupplementaryOutputDir
			
			% retrieve
			outputs = job.get_outputs();
			
		end
		
		function path = getSupplementaryOutputDir(job)
			
			% retrieve supplementary output directory path
			%
			% path = job.getSupplementaryOutputDir()
			%
			% the path to the supplementary output directory is
			% returned in "path". if an output method needs to
			% output more than just a variable (e.g. a video
			% file), it should store the additional data in this
			% directory.
			%
			% see also: clearOutputs, setOutput, getOutput,
			%   getOutputs, updateOutput
			
			path = [fileparts(job.filename) '/output'];
			if ~exist(path, 'dir')
				mkdir(path);
			end
			
		end
		
		function updateOutput(job, methodName, pars, invalidate)
			
			% update an output
			%
			% job.updateOutput(methodName, pars, invalidate)
			%
			% if the specified output is invalid, recompute it. if
			% the specified output is valid, do nothing.
			% "methodName" should be an Output Method, and is also
			% used as the storage "key" (see setOutput()). "pars"
			% are passed as the Runtime Parameters of the
			% specified Output Method. if "invalidate" is
			% supplied, and evaluates to true, the output is
			% recomputed whether or not it has been invalidated.
			%
			% see also: clearOutputs, setOutput, getOutput,
			%   getOutputs, getSupplementaryOutputDir
			
			% invalidate is true forces a remake, even if
			% up-to-date
			if nargin < 4
				invalidate = false;
			end
			
			% not allowed unless we're saved, since we need to
			% offer a supplementary folder
			if isempty(job.filename)
				error('cannot update job output because the job is not yet saved to disk');
			end
			
			% do pre-requisites
			info = stPlugin(methodName, 'info');
			if isfield(info, 'prerequisites')
				prereq = info.prerequisites;
				for p = 1:length(prereq)
					job.updateOutput(prereq{p}, pars, invalidate);
				end
			end
			
			% check if invalidated
			if isempty(job.get_output(methodName)) || invalidate
				
				% get permission to modify
				job.modify();
				
				% report
				logid = job.log(['  ' methodName ': processing...']);
				
				% get pars
				if isfield(pars, methodName)
					pars = pars.(methodName);
				else
					pars = [];
				end
				
				% build state
				state = [];
				state.pars = stMergePars(pars, stPlugin(methodName, 'parameters'));
				state.frameIndices = job.getFrameRange('export');
				
				% call
				if isempty(state.frameIndices)
					state.result = [];
				else
					state = stPlugin(methodName, 'process', job, state);
				end
				
				% lay in - plugin will return empty if it was unable
				% to provide any output
				if ~isempty(state.result)
					job.setOutput(methodName, state.result);
					job.log(logid);
				else
					logid.result = 'NO RESULTS';
					job.log(logid);
				end
				
			else
				
				% report
				logid = job.log(['  ' methodName ': (up to date)']);
				
			end
			
		end
		
	end
	
	
	
	
	
	
	%% USERDATA
	
	methods
		
		% "user data" is a place where the caller can store
		% information on the job object. currently, the fields
		% that are used are:
		%
		% "parametrized": flag, indicating that the user has
		% parametrized (or reviewed the parameters of) the job.
		%
		% "tag": string, user-specified tag, such as "the one
		% where the rat falls over".
		
		function value = getUserData(job, key, defaultValue)
			
			% get
			if isfield(job.m_pars.userdata, key)
				value = job.m_pars.userdata.(key);
			else
				if nargin == 3
					value = defaultValue;
				else
					value = [];
				end
			end
			
		end
		
		function setUserData(job, key, value)
			
			% get permission to modify
			job.modify();
			
			% set
			job.m_pars.userdata.(key) = value;
			
			% invalidate
			job.invalidate();
			
		end
		
	end
	
	
	
	
	%% USER FEEDBACK
	
	methods (Access = private)
		
		function result = raiseEvent(job, eventType, data)
			
			if nargin < 3
				data = struct();
			end
			
			if ~isempty(job.callbackObject)
				data.job = job;
				result = job.callbackObject.jobEvent(eventType, data);
			else
				result = [];
			end
			
		end
		
	end
	
	methods
		
		function logid = log(job, msg)
			
			if isempty(job.callbackObject)
				disp(msg);
				if nargout
					logid = [];
				end
			else
				data = [];
				data.job = job;
				data.msg = msg;
				if nargout
					logid = job.callbackObject.jobEvent('log', data);
				else
					job.callbackObject.jobEvent('log', data);
				end
			end
			
		end
		
		function cancel = progress(job, fraction)
			
			if isempty(job.callbackObject)
				disp(['progress: ' int2str(fraction*100) '%']);
				cancel = false;
			else
				data = [];
				data.job = job;
				data.fraction = fraction;
				data.offerPause = true;
				cancel = job.callbackObject.jobEvent('progress', data);
			end
			
		end
		
	end
	
	
	
	
	
	
	
	%% RUNTIME STATE
	
	methods (Access = private)
		
		function frameIndex = parseFrameIndex(job, frameIndex)
			
			% convert string frameIndex
			if isstring(frameIndex)
				frameIndex = job.getFrameRange(frameIndex);
			end
			
			% validate frameIndex
			if ~isnumeric(frameIndex) || ~isscalar(frameIndex)
				error('frameIndex should be a scalar value');
			end
			
			% validate range
			info = job.getVideoInfo();
			if frameIndex < 1 || frameIndex > info.NumFrames
				error('frameIndex is out of range');
			end
			
		end
		
	end
	
	methods
		
		function cleanupRuntimeState(job, frameIndex)
			
			% clean up (discard) the Runtime State
			%
			% PART OF THE "RUNTIME STATE" INTERFACE
			%
			% cleanupRuntimeState(job, frameIndex)
			%
			% this function just cleans up runtime state, either
			% for a specific frame, or for all frames if the
			% frameIndex is not specified. it can be called as a
			% task progresses to keep the memory usage low, if
			% desired. runtime state is not persisted, so will be
			% freed over the save()/load() cycle anyway.
			%
			% this function is generally called automatically by
			% the job object as frames are processed. if you are
			% supervising processing in your own code (e.g. in Ten
			% Line Manager), you may call this function as you
			% process frames, or call it once a pass is finished,
			% or neglect to call it at all, depending on your
			% memory management preferences.
			%
			% see also: getRuntimeState, setRuntimeState
			
			% all or some
			if nargin == 2
				frameIndex = job.parseFrameIndex(frameIndex);
				job.runtime{frameIndex} = [];
			else
				job.runtime = {};
			end
			
		end
		
		function value = getRuntimeState(job, frameIndex, key)
			
			% get a value from the runtime state
			%
			% PART OF THE "RUNTIME STATE" INTERFACE
			%
			% value = job.getRuntimeState(frameIndex, key)
			%
			%   this function provides access to the Runtime State
			%   array. the function returns a value previously
			%   stored against "key" for the specified frame, or
			%   the empty matrix if no value was previously
			%   stored.
			%
			% see also: setRuntimeState, cleanupRuntimeState
			
			% get what we need
			context = job.getContext();
			frameIndex = job.parseFrameIndex(frameIndex);
			
			% validate key
			if ~isstring(key)
				error('key should be a string');
			end
			
			% get value
			if length(job.runtime) >= frameIndex && ...
					isfield(job.runtime{frameIndex}, context.pipeline) && ...
					isfield(job.runtime{frameIndex}.(context.pipeline), key)
				value = job.runtime{frameIndex}.(context.pipeline).(key);
			else
				value = [];
			end
			
		end
		
		function setRuntimeState(job, frameIndex, key, value)
			
			% store a value in the runtime state
			%
			% PART OF THE "RUNTIME STATE" INTERFACE
			%
			% job.setRuntimeState(frameIndex, key, value)
			%
			%   this function provides access to the Runtime State
			%   array. the function stores "value" against "key"
			%   for the specified frame.
			%
			% see also: getRuntimeState, cleanupRuntimeState
			
			% get what we need
			context = job.getContext();
			frameIndex = job.parseFrameIndex(frameIndex);
			
			% validate key
			if ~isstring(key)
				error('key should be a string');
			end
			
			% lay in
			job.runtime{frameIndex}.(context.pipeline).(key) = value;
			
		end
		
		function present = isPreprocPresent(job, frameIndex, pipelineName)
			
			% context
			if nargin < 3
				context = job.getContext();
				pipelineName = context.pipeline;
			end
			
			% get what we need
			frameIndex = job.parseFrameIndex(frameIndex);
			
			% assume not
			present = false;
			
			% if there is an entry in runtime for this frame
			if length(job.runtime) >= frameIndex
				
				% and if there is a field for the frame server
				if isfield(job.runtime{frameIndex}, pipelineName)
					
					% and if there is a field in that for this pipeline
					if isfield(job.runtime{frameIndex}.(pipelineName), 'frame__')
						present = true;
					end
				end
			end
			
		end
		
		function frame = getRuntimeFrame(job, frameIndex, pipelineName)
			
			% frame = getRuntimeFrame(frameIndex, pipelineName)
			%
			%   this call, along with setRuntimeFrame(), constitutes the
			%   "frame server" of the task, which provides exchange
			%   of video data between methods in a pipeline. pp
			%   methods will call this, process the frame data,
			%   and then call setRuntimeFrame() to store the result.
			%   other methods that need the pre-processed frame
			%   will just call this method.
			%
			%   the argument "pipelineName" is not needed if a
			%   context is set (i.e. within a method).
			%
			% NB: "frame" is returned as a cell containing the
			%   frame, rather than as a raw array.
			%
			%   as a shortcut, we allow frameIndex to be a string,
			%   in which case it is passed to getFrameRange() to
			%   be processed into a numeric frameIndex before use.
			
			% context
			if nargin < 3
				context = job.getContext();
				pipelineName = context.pipeline;
			end
			
			% get what we need
			frameIndex = job.parseFrameIndex(frameIndex);
			
			% if there is an entry in runtime for this frame
			if length(job.runtime) >= frameIndex
				
				% and if there is a field for the frame server
				if isfield(job.runtime{frameIndex}, pipelineName)
					
					% and if there is a field in that for this pipeline
					if isfield(job.runtime{frameIndex}.(pipelineName), 'frame__')
						
						% then we have a frame
						frame = job.runtime{frameIndex}.(pipelineName).frame__;
						return
						
					end
				end
			end
			
			% otherwise, we can get the frame from the video
			frame = stLoadMovieFrames(job.getVideoFilename(), frameIndex);
			
		end
		
		function setRuntimeFrame(job, frameIndex, frame)
			
			% setRuntimeFrame(frameIndex, frame)
			%
			%   this call, along with getRuntimeFrame(), constitutes the
			%   "frame server" of the job, which provides exchange
			%   of video data between methods in the pipelines.
			%   only pp methods should call this function, to
			%   provide their output.
			%
			% NB: "frame" must be a cell containing the frame,
			% rather than a raw array.
			
			% get what we need
			context = job.getContext();
			frameIndex = job.parseFrameIndex(frameIndex);
			
			% validate frame
			if ~iscell(frame) || ~isscalar(frame)
				error('frame should be a scalar cell containing the frame');
			end
			
			% set latest runtime frame
			job.runtime{frameIndex}.(context.pipeline).frame__ = frame;
			
		end
		
	end
	
	
	
	
	
	
	
	%% TASK RUNTIME INTERFACE
	
	methods
		
		function setResults(job, frameIndex, key, value)
			
			% setResults(job, frameIndex, key, value)
			%
			%   this call stores the results from a method for a
			%   particular frame specified by frameIndex. key can
			%   be any string, and should usually follow the
			%   conventions agreed between method authors. for
			%   instance: snout tracking data is always stored
			%   against the key "snout"; whisker tracking data is
			%   stored against the key "whisker". value should be
			%   a struct array with any fields, as necessary.
			%   these data are not read by the framework, but form
			%   the tokens of exchange between plugins (and the
			%   user, who will see these data when they 'export').
			
			% modify
			job.modify();
			
			% validate task state
			if job.getCurrentTaskState() ~= stJob.TASK_STARTED
				error('method call invalid - current task is not in STARTED state');
			end
			
			% get what we need
			context = job.getContext();
			taskIndex = job.getCurrentTask();
			frameIndex = job.parseFrameIndex(frameIndex);
			
			% get index into results array
			task = job.get_task(taskIndex);
			f = find(task.frames.selected == frameIndex);
			if ~isscalar(f)
				error('frameIndex invalid in setResults()');
			end
			
			% store
			task.results{f}.(key) = value;
			job.set_task(taskIndex, task);
			
		end
		
		% this is not usually a runtime call, but it's so
		% closely intertwined with the above function that it's
		% easiest to keep them together. in any case, some
		% methods will call this to get the results posted by
		% earlier processing methods.
		
		function [results, frameIndices] = getResults(job, frameIndices, taskIndices)
			
			% [results, frameIndices] = getResults(job, frameIndices, taskIndices)
			%
			%   this call retrieves the results from one or more
			%   tasks and one or more frames. if frameIndices is
			%   not specified, it defaults to 'selected', i.e.
			%   using start, end, step, and key frame indices. if
			%   taskIndices is not specified, it defaults to
			%   'collapse', which means to retrieve results
			%   collapsed across all existing tasks (including any
			%   that are open, e.g. currently being processed).
			%
			%   from within a method plugin, the most common
			%   calling convention is:
			%
			%     results = job.getResults(frameIndex)
			%
			%   which returns all results for the specified frame
			%   from any previously-computed method.
			
			% frameIndices
			if nargin < 2
				frameIndices = 'selected';
			end
			
			% taskIndices
			if nargin < 3
				taskIndices = 'collapse';
			end
			
			% frameIndices
			if ischar(frameIndices)
				switch frameIndices
					
					case 'all'
						info = job.getVideoInfo();
						frameIndices = 1:info.NumFrames;
						
					case 'selected'
						frameIndices = job.getFrameRange('selected');
						
					otherwise
						error('unrecognised frame indices');
						
				end
			end
			
			% create results
			results = repmat({struct()}, 1, length(frameIndices));
			
			% get tasks
			tasks = job.get_tasks();
			
			% translate taskIndices
			if ischar(taskIndices)
				switch taskIndices
					
					case 'latest'
						
						% latest task - note that returned results will
						% be empty if no task exists
						if isempty(tasks)
							taskIndices = [];
						else
							taskIndices = length(tasks);
						end
						
					case 'collapse'
						
						% collapse of all tasks - this call is still
						% valid if there are no tasks yet, since empty
						% is an acceptable return value.
						taskIndices = 1:length(tasks);
						
					otherwise
						error(['unrecognised task index "' taskIndices '"']);
						
				end
			end
			
			% validate
			if any(taskIndices < 1 | taskIndices > length(tasks))
				error('at least one of the specified tasks was not found');
			end
			
			% collapse across specified tasks
			for taskIndex = taskIndices
				
				task = tasks{taskIndex};
				
				if ~task.active.flag
					continue
				end
				selected = task.frames.selected;
				for fi = 1:length(frameIndices)
					frameIndex = frameIndices(fi);
					si = find(frameIndex == selected);
					fa = find(frameIndex == task.active.frames);
					if isempty(fa)
						continue
					end
					if ~isempty(si)
						if length(task.results) >= si
							sub = task.results{si};
							if isstruct(sub)
								fn = fieldnames(sub);
								for ni = 1:length(fn)
									results{fi}.(fn{ni}) = sub.(fn{ni});
								end
							end
						end
					end
				end
			end
			
		end
		
		function methods_list = getAllUsedMethods(job)
			
			% get tasks
			tasks = job.get_tasks();
			
			methods_list = struct();
			for t = 1:length(tasks)
				pp = tasks{t}.pipelines;
				for p = 1:length(pp)
					pipeline = pp{p};
					for m = 1:length(pipeline.methods)
						methods_list.([pipeline.name '_' pipeline.methods{m}]) = 1;
					end
				end
			end
			fn = fieldnames(methods_list);
			methods_list = {};
			for f = 1:length(fn)
				n = fn{f};
				i = find(n == '_', 1);
				methods_list{end+1} = {n(1:i-1) n(i+1:end)};
			end
			
		end
		
		function count = getTaskCount(job)
			
			count = job.get_task_count();
			
		end
		
		function info = getTaskInfo(job, taskIndex)
			
			% get task
			task = job.get_task(taskIndex);
			
			info = [];
			info.time = task.time;
			info.frames = task.frames;
			info.active = task.active;
			
			% add available
			vinfo = job.getVideoInfo();
			info.frames.available = vinfo.NumFrames;
			
		end
		
		function setActiveState(job, taskIndex, flag)
			
			if ~isscalar(flag) || ~islogical(flag)
				error('invalid flag value');
			end
			
			% modify
			job.modify();
			
			task = job.get_task(taskIndex);
			task.active.flag = flag;
			job.set_task(taskIndex, task);
			
		end
		
		function setActiveFrames(job, taskIndex, frames)
			
			% modify
			job.modify();
			
			task = job.get_task(taskIndex);
			task.active.frames = frames;
			job.set_task(taskIndex, task);
			
		end
		
	end
	
	
	
	
	
	%% CONTEXT MANAGEMENT
	
	% all runtime calls (cache, state, parameters, frames,
	% results, etc.) have a context, which is set here. this
	% means the methods making the calls do not have to
	% identify themselves, because the manager sets their
	% context before calling the methods (and clears their
	% context after the call). therefore, to call a method on
	% a job, you call the method "through" the job, using the
	% CONTEXT METHODS interface, below. this ensures that the
	% context is set correctly when the method is called.
	
	methods (Access = private)
		
		function context = getContext(job)
			
			if isempty(job.context)
				error('stJob:NoContext', 'no context is set for this call');
			end
			context = job.context;
			
		end
		
		function setContext(job, pipeline, method)
			
			job.context.pipeline = pipeline;
			job.context.method = method;
			
		end
		
		function clearContext(job)
			
			job.context = [];
			
		end
		
	end
	
	
	
	
	%% CONTEXT METHODS
	
	methods
		
		% these methods wrap a call to the plugin in calls to
		% setContext() and clearContext(), so the user doesn't
		% have to faff with that.
		
		function state = methodInitialize(job, pipeline, method, initdata)
			
			% initialize and return the state.
			
			job.setContext(pipeline, method);
			state = stPlugin(method, 'initialize', job, initdata);
			job.clearContext();
			
		end
		
		function state = methodProcess(job, pipeline, method, state, frameIndex)
			
			% process the specified frame (note that we augment
			% the state structure with "frameIndices", to keep to
			% the plan of only passing one argument to stPlugin
			% other than the job). it gets returned, but i don't
			% see why that would be problematic (we could
			% rmfield() it if we felt like it).
			
			job.setContext(pipeline, method);
			state.frameIndex = frameIndex;
			state = stPlugin(method, 'process', job, state);
			job.clearContext();
			
		end
		
		function state = methodFinalize(job, pipeline, method, state)
			
			% just call finalize on the method to give it a
			% chance to shut down.
			
			job.setContext(pipeline, method);
			state = stPlugin(method, 'finalize', job, state);
			job.clearContext();
			
		end
		
		function state = methodPlot(job, pipeline, method, plotdata)
			
			% call plot
			
			job.setContext(pipeline, method);
			state = stPlugin(method, 'plot', job, plotdata);
			job.clearContext();
			
		end
		
	end
	
	
	
	
	
	
	
	
	%% TASK MANAGEMENT
	
	% the task can be in any of the following states, and is
	% moved between the listed states by the calls listed.
	%
	% TASK_ABSENT
	%     taskCreate()
	% TASK_CREATED
	%     taskStart()
	% TASK_STARTED
	%     taskFinish()
	%     taskFinishAfterError()
	% TASK_FINISHED
	%     taskClose()
	% TASK_CLOSED
	%
	% NB: "ABSENT" is not really a task state, but
	% represents a task that isn't there. some functions
	% work with the "current task", which refers to the last
	% task in the tasks list, unless that task is "CLOSED",
	% or the task list is empty, in which case the current
	% task is "ABSENT".
	%
	% NB: taskRestart() is the remaining state change function
	% which, despite the name, moves the task from FINISHED to
	% CREATED, allowing it to be computed again. this is used
	% during parametrisation, because we "try" the task with
	% different parameters then restart it and try again. it
	% can also be used by users in the same way - that is, if
	% they process a task and it turns out the parameters were
	% no good, they can restart, reparametrize, and run again.
	
	properties (Constant = true, Hidden = true)
		
		TASK_ABSENT = 0
		TASK_CREATED = 1
		TASK_STARTED = 2
		TASK_FINISHED = 3
		TASK_CLOSED = -1
		
	end
	
	methods (Access = private)
		
		% convert pipelines from the form used in the arguments
		% of taskCreate(), {'name', {'method', 'method', ...}}, to
		% the form used internally:
		%
		% .name: pipeline name
		% .methods: cell array of method names
		
		function pipelines = parsePipelines(job, pipelines)
			
			for p = 1:length(pipelines)
				
				pipeline = pipelines{p};
				
				% taskRestart() passes us the existing pipelines, so
				% they won't need parsing if they're already structs
				if ~isstruct(pipeline)
					
					% validate
					if ~iscell(pipeline) || ~stIsOfSize(pipeline, [1 2]) || ~ischar(pipeline{1}) || ~iscell(pipeline{2})
						error('stJob:InvalidArguments', 'pipeline should be a cell array of the form {name, {''method1'', ''method2'', ...}}');
					end
					
					% convert
					pipeline = struct('name', pipeline{1}, 'methods', {pipeline{2}});
					
					% validate
					switch pipeline.name
						case {'object' 'snout' 'whisker'}
						otherwise
							error('stJob:InvalidArguments', ['pipeline name "' pipeline.name '" was not recognised']);
					end
					
					% validate
					for c = 1:numel(pipeline.methods)
						if ~isstring(pipeline.methods{c})
							error('stJob:InvalidArguments', 'each entry in pipeline methods should be a string');
						end
					end
					
					pipelines{p} = pipeline;
					
				end
				
			end
			
		end
		
		function taskIndex = getCurrentTask(job, validateExists)
			
			tasks = job.get_tasks();
			
			if isempty(tasks)
				taskIndex = [];
			elseif tasks{end}.state == stJob.TASK_CLOSED
				taskIndex = [];
			else
				taskIndex = length(tasks);
			end
			
			if nargin == 2 && validateExists && isempty(taskIndex)
				
				% this is used by the runtime state engine, to do
				% simple validation of method calls
				error('job does not have a current task');
				
			end
			
		end
		
	end
	
	methods
		
		function deselect(job)
			
			% (framework side) job is being deselected in the job list
			
			% when the user deselects the job, we can offer JIT
			% discard
			if ~isempty(job.filename) && job.state ~= job.STATE_MODIFIED
				job.offerJITdiscard();
			end
			
		end
		
		function [state, errors, pipelines, JITdataLoaded] = getCurrentTaskState(job)
			
			% if the JIT data is absent, but the cache is present,
			% use the cache
			if isempty(job.m_data) && isfield(job.m_pars.cache, 'currentTaskState')
				item = job.m_pars.cache.currentTaskState;
				state = item.state;
				errors = item.errors;
				pipelines = item.pipelines;
				JITdataLoaded = false;
				return
			end
			
			% get current task index
			taskIndex = job.getCurrentTask();
			JITdataLoaded = true;
			
			% if absent
			if isempty(taskIndex)
				state = stJob.TASK_ABSENT;
				errors = {};
				pipelines = {};
			else
				task = job.get_task(taskIndex);
				state = task.state;
				errors = task.errors;
				pipelines = task.pipelines;
			end
			
		end
		
		function hash = getCurrentTaskHash(job)
			
			% if the JIT data is absent, but the cache is present,
			% use the cache
			if isempty(job.m_data) && isfield(job.m_pars.cache, 'currentTaskHash')
				item = job.m_pars.cache.currentTaskHash;
				hash = item.hash;
				return
			end
			
			% get current task index
			taskIndex = job.getCurrentTask();
			
			% if absent
			if isempty(taskIndex)
				hash = 'aaaa_no_current_task';
			else
				task = job.get_task(taskIndex);
				pipelines = task.pipelines;
				hash = '';
				for p = 1:length(pipelines)
					pp = pipelines{p};
					hash = [hash '_' pp.name];
				end
				for p = 1:length(pipelines)
					pp = pipelines{p};
					for m = 1:length(pp.methods)
						hash = [hash '_' pp.methods{m}];
					end
				end
				hash = hash(2:end);
			end
			
		end
		
		function taskCreate(job, pipelines)
			
			% Create a new task
			
			% modify
			job.modify();
			
			% validate task state
			if job.getCurrentTaskState() ~= stJob.TASK_ABSENT
				error('cannot CREATE current task - the job already has a current task');
			end
			
			% if pipelines is empty, ignore this call
			if isempty(pipelines)
				return
			end
			
			% create a new task
			task = [];
			task.time.created = now;
			task.time.started = [];
			task.time.finished = [];
			task.time.closed = [];
			task.state = stJob.TASK_CREATED;
			task.pipelines = job.parsePipelines(pipelines);
			task.frames = struct();
			task.pars = struct();
			task.errors = {};
			task.results = {};
			task.active.flag = false;
			task.active.frames = [];
			
			% store it
			taskIndex = job.append_task(task);
			
			% invalidate
			job.invalidate();
			
		end
		
		function taskDelete(job)
			
			% Delete the current task
			
			% modify
			job.modify();
			
			% validate task state
			if job.getCurrentTaskState() == stJob.TASK_ABSENT
				error('cannot DELETE current task - the job has no current task');
			end
			
			% delete
			job.delete_last_task();
			
			% invalidate
			job.invalidate();
			
		end
		
		function taskRestart(job, keepResults)
			
			% Restart the current task (TASK_STARTED/TASK_FINISHED -> TASK_STARTED)
			%
			%   This function can be used either on a task that
			%   has completed successfully, or on a task that
			%   failed to complete.
			
			% modify
			job.modify();
			
			% validate task state
			state = job.getCurrentTaskState();
			if state ~= stJob.TASK_STARTED && state ~= stJob.TASK_FINISHED
				error('cannot RESTART current task - it is not in a restartable state');
			end
			
			% args
			if nargin < 2
				keepResults = false;
			end
			
			% recover data from current task
			taskIndex = job.getCurrentTask();
			task = job.get_task(taskIndex);
			pipelines = task.pipelines;
			results = task.results;
			
			% delete current task
			job.taskDelete();
			
			% and re-create it
			job.taskCreate(pipelines);
			
			% keepResults
			if keepResults
				task = job.get_task(taskIndex);
				task.results = results;
				job.set_task(taskIndex, task);
			end
			
		end
		
		function taskStart(job)
			
			% Start the current task (TASK_CREATED -> TASK_STARTED)
			%
			%   TASK_CREATED -> TASK_STARTED is not just a state
			%   change. it is also the cusp at which the frames
			%   and pars of the job are locked in to the task, and
			%   at which we initialize the runtime state (using
			%   the cleanupRuntimeState() call).
			
			% modify
			job.modify();
			
			% validate task state
			if job.getCurrentTaskState() ~= stJob.TASK_CREATED
				error('cannot START current task - it is not in the CREATED state');
			end
			
			% get current task
			taskIndex = job.getCurrentTask();
			task = job.get_task(taskIndex);
			
			% update state
			task.state = stJob.TASK_STARTED;
			task.time.started = now;
			
			% set frames
			task.frames = job.m_pars.frames;
			task.frames.pass{1} = job.getFrameRange('forward');
			task.frames.pass{2} = job.getFrameRange('backward');
			task.frames.selected = job.getFrameRange('selected');
			
			% set active
			task.active.flag = true;
			task.active.frames = task.frames.selected;
			
			% set pars
			for p = 1:length(task.pipelines)
				pipeline = task.pipelines{p};
				methods_list = pipeline.methods;
				for m = 1:length(methods_list)
					method = methods_list{m};
					cachekey = [pipeline.name '_' method];
					if isfield(job.m_pars.pars, cachekey)
						pars = job.m_pars.pars.(cachekey);
					else
						pars = struct();
					end
					task.pars.(cachekey) = ...
						stMergePars(pars, stPlugin(method, 'parameters'));
				end
			end
			
			% lay in
			job.set_task(taskIndex, task);
			
			% clean up runtime state
			job.cleanupRuntimeState();
			
			% outputs are now invalid, as well
			job.clearOutputs();
			
			% raise event
			job.invalidate();
			
		end
		
		function taskFinish(job)
			
			% Finish the current task (TASK_STARTED -> TASK_FINISHED)
			
			% modify
			job.modify();
			
			% validate task state
			if job.getCurrentTaskState() ~= stJob.TASK_STARTED
				error('cannot FINISH current task - it is not in the STARTED state');
			end
			
			% update current task
			taskIndex = job.getCurrentTask();
			task = job.get_task(taskIndex);
			task.state = stJob.TASK_FINISHED;
			task.time.finished = now;
			job.set_task(taskIndex, task);
			
			% raise event
			job.invalidate();
			
		end
		
		function taskFinishAfterError(job, err)
			
			% Finish the current task after an error (TASK_STARTED -> TASK_FINISHED)
			
			% modify
			job.modify();
			
			% validate task state
			if job.getCurrentTaskState() ~= stJob.TASK_STARTED
				error('cannot FINISH current task - it is not in the STARTED state');
			end
			
			% get current task
			taskIndex = job.getCurrentTask();
			task = job.get_task(taskIndex);
			
			% add error
			task.errors{end+1} = err;
			
			% set current task
			job.set_task(taskIndex, task);
			
			% delegate to finish
			job.taskFinish();
			
		end
		
		function taskClose(job)
			
			% Close the current task (TASK_FINISHED -> TASK_CLOSED)
			
			% modify
			job.modify();
			
			% validate task state
			if job.getCurrentTaskState() ~= stJob.TASK_FINISHED
				error('cannot CLOSE current task - it is not in the FINISHED state');
			end
			
			% update current task
			taskIndex = job.getCurrentTask();
			task = job.get_task(taskIndex);
			task.state = stJob.TASK_CLOSED;
			task.time.closed = now;
			job.set_task(taskIndex, task);
			
			% invalidate
			job.invalidate();
			
		end
		
	end
	
	
	
	
	
	%% TASK MANAGER: THE JOB'S TenLineManager
	
	methods
		
		% this function prepares the data for Initialize
		
		function initdata = getInitializeData(job, pass)
			
			% Return state data for "initialize" request
			%
			% initdata = job.getInitializeData(pass)
			%
			%   This function prepares and returns the state data
			%   that needs to be passed to the "initialize"
			%   request of a processing method.
			
			% validate task state
			if job.getCurrentTaskState() ~= stJob.TASK_STARTED
				error('cannot get initialize data - the current task is not in the STARTED state');
			end
			
			% get current task
			taskIndex = job.getCurrentTask();
			task = job.get_task(taskIndex);
			
			% translate pass
			if isstring(pass)
				switch pass
					case 'forward'
						pass = 1;
					case 'backward';
						pass = 2;
					otherwise
						error(['unrecognised pass "' pass '"']);
				end
			end
			
			% create state
			initdata  = [];
			initdata.frameStep = (3 - (pass * 2)) * task.frames.step;
			initdata.frameIndices = task.frames.pass{pass};
			
		end
		
		function errors = collectErrors(job)
			
			% Return the errors that occurred in the current task
			%
			% errors = job.collectErrors()
			%
			%   Return a cell array of all the errors that
			%   occurred whilst the current task was being
			%   executed. This list will be empty if the current
			%   task completed successfully.
			
			% validate task state
			if job.getCurrentTaskState() ~= stJob.TASK_FINISHED
				error('cannot collect errors for this job - it is not in the FINISHED state');
			end
			
			% get current task
			taskIndex = job.getCurrentTask();
			task = job.get_task(taskIndex);
			errors = task.errors;
			
		end
		
		function taskRun(job, varargin)
			
			% Supervise running of the task (TASK_CREATED -> TASK_FINISHED)
			%
			% job.taskRun(...)
			%
			%   This function actually performs the task, and
			%   starts by "starting" it (which commits the
			%   pipelines and pars), then iterates through the
			%   task until completion or until an error occurs,
			%   then "finishes" it.
			
			job.inTaskRun = true;
			
			try
				job.taskRun_sub(varargin{:});
				job.inTaskRun = false;
			catch err
				job.inTaskRun = false;
			end
			
		end
		
	end
	
	methods (Access = private)
		
		function taskRun_sub(job, varargin)
			
			% context
			context = '';
			
			% validate task state
			if job.getCurrentTaskState() ~= stJob.TASK_CREATED
				error('cannot RUN current task - it is not in the CREATED state');
			end
			
			% handle arguments
			a = 0;
			job.flagClear('unattended');
			while a < length(varargin)
				a = a + 1;
				switch varargin{a}
					case 'unattended'
						job.flagSet('unattended');
					otherwise
						error(['unrecognised argument "' varargin{a} '"']);
				end
			end
			
			% start the current task
			job.taskStart();
			
			% get current task
			taskIndex = job.getCurrentTask();
			task = job.get_task(taskIndex);
			
			% now that we've started the task, enter try, so we
			% can record any error during processing
			try
				
				% extract
				n_frames_total = length(task.frames.pass{1}) + length(task.frames.pass{2});
				
				% report
				videoFilename = job.getVideoFilename();
				job.log(['running job "' stProcessPath('justFilename', videoFilename) '"...']);
				
				% for each pipeline
				for pi = 1:length(task.pipelines)
					
					% for this pipeline
					pipeline = task.pipelines{pi};
					
					% progress counter
					n_frames_done = 0;
					
					% extract
					n_methods = length(pipeline.methods);
					
					% report
					job.log(['running pipeline "' pipeline.name '"...']);
					
					% prepare plotdata
					plotdata = [];
					plotdata.h_overlay = [];
					
					% for each pass
					for pass = 1:2
						
						% build initdata
						initdata = job.getInitializeData(pass);
						if isempty(initdata.frameIndices)
							job.log(['skipping pass "' int2str(pass) '" (no frames)...']);
						else
							job.log(['running pass "' int2str(pass) '"...']);
						end
						
						% state vector
						states = cell(1, n_methods);
						
						% initialize
						for m = 1:n_methods
							
							% call initialize
							states{m} = job.methodInitialize( ...
								pipeline.name, ...
								pipeline.methods{m}, ...
								initdata);
							
						end
						
						% for each frame
						for fi = 1:length(initdata.frameIndices)
							
							% extract
							frameIndex = initdata.frameIndices(fi);
							
							% context
							context = sprintf('whilst computing frame %i (of pass %i, frames %i to %i)', frameIndex, pass, initdata.frameIndices(1), initdata.frameIndices(end));
							
							% for each method
							for m = 1:n_methods
								
								% process method
								states{m} = job.methodProcess( ...
									pipeline.name, ...
									pipeline.methods{m}, ...
									states{m}, frameIndex);
								
							end
							
							% update monitor
							plotdata.h_axis = job.raiseEvent('jobProcessFrame', struct('frameIndex', frameIndex));
							if ~isempty(plotdata.h_axis)
								
								% clear previous
								stSafeDelete(plotdata.h_overlay);
								plotdata.h_overlay = [];
								
								% set frame index
								plotdata.frameIndex = frameIndex;
								
								% for each method
								for m = 1:n_methods
									
									result = job.methodPlot( ...
										pipeline.name, ...
										pipeline.methods{m}, ...
										plotdata);
									if ~isempty(result) && isfield(result, 'h')
										if ~isnumeric(result.h) || any(~ishandle(result.h(:)))
											disp('DEBUG INFO:');
											result.h
											error('plugin method returned an invalid handle array in "result.h"');
										end
										plotdata.h_overlay = [plotdata.h_overlay result.h];
									end
									
								end
								
								% drawnow
								drawnow
								
							end
							
							% interim clean up
							job.cleanupRuntimeState(frameIndex);
							
							% context
							context = '';
							
							% progress
							n_frames_done = n_frames_done + 1;
							cancel = job.progress(n_frames_done / n_frames_total);
							
							% throw error on cancel
							if cancel
								error('stJob:UserCancel', 'user cancelled processing');
							end
							
						end
						
						% finalize
						for m = 1:n_methods
							
							% call finalize
							job.methodFinalize(pipeline.name, ...
								pipeline.methods{m}, states{m});
							
						end
						
					end % for each pass
					
					% clear plotdata
					stSafeDelete(plotdata.h_overlay);
					plotdata.h_overlay = [];
					
				end % for each pipeline
				
				% finish the task
				job.taskFinish();

				% report
				job.log(['finished running job (OK)...']);
				
			catch err
				
				% clear plotdata
				stSafeDelete(plotdata.h_overlay);
				plotdata.h_overlay = [];
				
				% convert err to struct
				w = warning('off', 'MATLAB:structOnObject');
				serr = struct(err);
				warning(w);
				
				% add context
				if ~isempty(context)
					serr.message = [serr.message 10 10 context];
				end
				
				% store err
				job.taskFinishAfterError(serr);
				
				% report
				job.log(['finished running job (ERROR)...']);
				
				% throw err to caller
				if job.flagIsSet('unattended')
					% 					disp(getReport(err));
				else
					rethrow(err);
				end
				
			end
			
		end
		
	end
	
	
	
	
	
	%% HIDE SUPERCLASS METHODS
	%% stStripCode: STOP
	
	methods (Hidden = true)
		
		function out = vertcat(p, q)
			error('stJob:MethodNotImplemented', 'concatenation is not supported');
		end
		
		function out = horzcat(p, q)
			error('stJob:MethodNotImplemented', 'concatenation is not supported');
		end
		
		function out = cat(dim, p, q)
			error('stJob:MethodNotImplemented', 'concatenation is not supported');
		end
		
		function out = ge(p, q)
			error('stJob:MethodNotImplemented', 'inequality operators are not supported');
		end
		
		function out = le(p, q)
			error('stJob:MethodNotImplemented', 'inequality operators are not supported');
		end
		
		function out = lt(p, q)
			error('stJob:MethodNotImplemented', 'inequality operators are not supported');
		end
		
		function out = gt(p, q)
			error('stJob:MethodNotImplemented', 'inequality operators are not supported');
		end
		
		function out = eq(p, q)
			out = eq@handle(p, q);
		end
		
		function out = ne(p, q)
			out = ne@handle(p, q);
		end
		
		function out = addlistener(p, varargin)
			error('stJob:MethodNotImplemented', 'method is not supported');
		end
		
		function out = findobj(p, varargin)
			error('stJob:MethodNotImplemented', 'method is not supported');
		end
		
		function out = findprop(p, varargin)
			error('stJob:MethodNotImplemented', 'method is not supported');
		end
		
		function out = notify(p, varargin)
			error('stJob:MethodNotImplemented', 'method is not supported');
		end
		
	end
	
	
end























% HELPERS : FORMATTING FUNCTIONS

function s = formatBoolean(b)

if b
	s = 'Yes';
else
	s = 'No';
end

end

function s = formatMaybeEmpty(s)

if isempty(s)
	s = '(empty)';
end

end

function s = formatVideoInfo(info)

if isfield(info, 'error')
	s = ['[ERROR: ' info.error ']'];
else
	s = sprintf('%i frames @ %ix%i', info.NumFrames, info.Width, info.Height);
end

end

function s = formatFrameData(frames)

s = sprintf('%i:%i:%i (%i)', frames.first, frames.step, frames.last, frames.initial);

end

function s = formatBytes(bytes)

if bytes < 1024
	s = [int2str(bytes) 'B'];
elseif bytes < (1024*1024)
	bytes = bytes / 1024;
	s = [int2str(ceil(bytes)) 'kiB'];
else
	bytes = bytes / 1024 / 1024;
	s = [int2str(ceil(bytes)) 'MiB'];
end

end

function s = formatDateMayBeNull(d)

if isempty(d) || ~d
	s = '[not yet]';
else
	s = datestr(d);
end

end



% HELPERS : validation functions

function b = isstring(s)

b = ischar(s) && ndims(s) == 2 && size(s, 1) == 1;

end

function b = isflag(v)

b = isscalar(v) && (isnumeric(v) || islogical(v));

end






