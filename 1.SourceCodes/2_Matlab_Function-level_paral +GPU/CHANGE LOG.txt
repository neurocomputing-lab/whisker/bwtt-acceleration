
RELEASE 0.6.3 @ R497

(Igor added some wtIgor stuff around here)

30/12/11 (mitch)
* some minor updates to stJob to protect against a malformed handle array being returned by the client plugin from the plot method.
* some updates to wtIgor whilst tracking these things down, for Igor to review.

08/01/12 (mitch)
* fixed two instances of extractWhiskersGrayValuesWithMorphologyAndShave(), one in sdGeneric/ and one in ppBgExtractionAndFilter.m, for frame sizes smaller than 400 pixels. these two instances are the same - we should combine them into a single file in shared/. check with Igor.

08/01/12 (mitch)
* minor change to logging of errors in stProject.

08/01/12 (mitch)
* extracted stExtractWhiskersGrayValuesWithMorphologyAndShave to the shared folder (Igor was ok with that).

(Igor updated wtIgor around here)

03/07/12 (mitch)
* working towards the open source release
* removed expiry code
* updated version to 1.0.0
* removed spurious materials that aren't needed in the GPL release
* reorganised repository into "trunk", "offtrunk", and "branches" (branches will not survive the move to sourceforge, and will be archived)
* sorted licensing and initial display, but not done per-file tags yet
* added copyright messages to all files
* updated about dialog and help menu to reflect new status
* updated all docs/files from ST -> BWTT
* moved standardTrackerGUI.m to bwtt.m
* moved userdata to %PATH%/BWTT/userdata.mat or ~/.BWTT/userdata.mat.



RELEASE 1.0.0 @ source forge

I (mitch) am releasing BWTT at source forge 18 Dec 2012. Obsoleted material (old version branches and scripts that are no longer relevant for the public release and docs that have been subsumed into the online docs, etc.) are in my work files archive dated today.



19/11/14 (mitch)
* Fix bug caused by changed interface to image() in Matlab R2014b. Files affected:
	stVideoSlidersGUI.m

20/11/14 (mitch)
* Added support for R2014b which deprecates aviread & mmreader, so need to use VideoReader instead. Files affected:
	stLoadMovieFrames.m
	stUserData.m.

20/11/14 (mitch)
* Various fixes for R2014b. Minor change to behaviour of setdiff(), event argument "evt" has become read-only (presumably pass-by-reference for performance), uicontrol now displays differently for pure text strings so I've rejigged to use cell arrays of text lines in one or two places. Also, added gui.resize() to most *GUI.m files, because of a change in the timing of a resize event with Matlab's new graphics system.

05/12/14 (mitch)
* Changed default video writer in omAnnotatedVideo to VideoWriter, avifile having been removed in R2014b.

22/06/15 (mitch)
* Changed Java Fail warning to be given only once per session
* Added some support for completing a release in folder above this one



RELEASE 1.0.1 @ source forge




