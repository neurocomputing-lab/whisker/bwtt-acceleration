
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%   
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function whiskerTreeLUT = convertInNodesToTable(whiskerTree)

% Author: Perkon Igor    -    perkon@sissa.it
% 
%<\inDOC>
% The function convertInNodesToTable takes the whiskerTree and converts it
% into a LUT
% Inputs:   -whiskerTree is a cell array, that gets indexed by 
%           whiskerTree{inCurve}=[xofmarker, yofmarker, c, theta,
%           rho](for ORIENTED RADIAL DISTANCE in createWhiskerTree
% Outputs:  -whiskerTreeLUT is a matrix, where the data are
%           [inCurve inMarker ::::: xofmarker, yofmarker, c, theta,
%           rho](for ORIENTED RADIAL DISTANCE in createWhiskerTree
%<\outDOC>

whiskerTreeLUT = zeros(2000,7);
idx = 0;
for i=1:numel(whiskerTree),
    wt = whiskerTree{i};    
    if ~isempty(wt)
        for j=1:size(wt,1),
        idx = idx + 1;
        whiskerTreeLUT(idx,:) = [i j wt(j,:)];
        end
    end
end
    
    
whiskerTreeLUT = whiskerTreeLUT(1:idx,:);