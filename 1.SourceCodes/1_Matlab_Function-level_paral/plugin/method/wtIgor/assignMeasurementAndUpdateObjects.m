
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [updatedObjects,assignedObjects] = assignMeasurementAndUpdateObjects(objects, measurements,...
    assignement, method, param)


% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Inputs:
%       - measures is a cell array of cubic splines in polar coordinates
%       - objects is a cell array of cubic splines in polar coordinates
%       - method defines the method used for calculatinf
%
% The function assigns measurements and updates position and speed of the
% tracked object - it also deletes old objects

switch method
    
    
    case 'shapedifference'
        numP = 50;
        % every assigned object gets updated - no acceleration update
        for i=find(assignement),
            spo = objects{i}.state;
            sporho = spo.knots;
            spm = objects{i}.oldState;
            spmrho = spm.knots;
            rhoTest = linspace(min(spmrho(1), sporho(1)), min(spmrho(end), sporho(end)), numP);
            signedDistTheta = fncmb(fncmb(fnxtr(spm),'-', fnxtr(spo)),'-',param.snoutOrientationDifference);
            
            objects{i}.oldState = objects{i}.state;
            objects{i}.state = measurements{assignement(i)};
            
            objects{i}.theCost = param.costMatrixObjects{1}(assignement(i),i);
            
            if signedDistTheta > 6,
                signedDistTheta = signedDistTheta - 2*pi;
            elseif signedDistTheta < -6,
                signedDistTheta = signedDistTheta + 2*pi;
            end
            
            
            newspeed = signedDistTheta;
            objects{i}.acceleration = [];%1/2*(objects{i}.speed - newspeed);
            objects{i}.speed = newspeed;
            objects{i}.flagNotAssigned = false;
            objects{i}.framesNotAssigned = param.framesNotAssigned;
            s.minTheta = min(fnval(spo, linspace(spo.knots(1), spo.knots(end), numP)));
            s.maxTheta = max(fnval(spo, linspace(spo.knots(1), spo.knots(end), numP)));
            
        end
        
    case 'keepLength'
        numP = 50;
        % every assigned object gets updated - but with direct substitution
        % only to estimated object lenght (in both directions)
        for i=find(assignement),
            spo = objects{i}.state;
            sporho = spo.knots;
            spm = objects{i}.oldState;
            spmrho = spm.knots;
            rhoTest = linspace(min(spmrho(1), sporho(1)), min(spmrho(end), sporho(end)), numP);
            signedDistTheta = mean(fnval(fnxtr(spm),rhoTest) - fnval(fnxtr(spo),rhoTest))-param.snoutOrientationDifference;
            
            objects{i}.oldState = objects{i}.state;
            % new substitution threshold
            rT = 5;
            spm = measurements{assignement(i)};
            spmrho = spm.knots;
            
            %% occlusion handling process
            if sum(assignement(i) == assignement) > 1,
                % occlusion
                longerRho = 0;
                for ii=find(assignement(i) == assignement),
                    longerRho = max(longerRho,objects{ii}.learnedRho);
                end
                if  objects{i}.learnedRho < longerRho
                    % slow drift in length - not too fast for long
                    % occlusions
                    arho = 0.05;
                    objects{i}.learnedRho = arho * spmrho(end) + (1-arho)*objects{i}.learnedRho;
                    
                    objects{i}.OcclusionTime = objects{i}.OcclusionTime - 1;
                    if objects{i}.OcclusionTime <= 0,
                        % kill the object
                        objects{i}.framesNotAssigned = 0;
                    end
                else
                    arho = 0.4;
                    objects{i}.learnedRho = arho * spmrho(end) + (1-arho)*objects{i}.learnedRho;
                end
            else
                % no occlusion - restore time
                objects{i}.OcclusionTime = param.maxOcclusionTime;
                % update lenght to cope with changes due to texture
                % occlusion
                arho = 0.4;
                objects{i}.learnedRho = arho * spmrho(end) + (1-arho)*objects{i}.learnedRho;
            end
            
            
            % update state
            rhoTest = linspace(mean([spmrho(1), sporho(1)]), objects{i}.learnedRho);
            thetaa = fnval(fnxtr(spm,2), rhoTest);
            thetaa = unwrap(thetaa);
            
            objects{i}.state = fn2fm(spap2(2,  3, rhoTest, thetaa),'B-');
            % NDI 8.6.2011
            %objects{i}.theCost = param.costMatrixObjects{1}(assignement(i),i);
            
            if signedDistTheta > 5,
                signedDistTheta = signedDistTheta - 2*pi;
            elseif signedDistTheta < -5,
                signedDistTheta = signedDistTheta + 2*pi;
            end
            
            
            newspeed = signedDistTheta;
            objects{i}.acceleration = 1/2*(objects{i}.speed - newspeed);
            objects{i}.speed = newspeed;
            objects{i}.flagNotAssigned = false;
            if objects{i}.framesNotAssigned > 0,
                % if it has been assigned, restore the counter except if it
                % is occluded - see upper code
                objects{i}.framesNotAssigned = param.framesNotAssigned;
            end
            s.minTheta = min(fnval(spo, linspace(spo.knots(1), spo.knots(end), numP)));
            s.maxTheta = max(fnval(spo, linspace(spo.knots(1), spo.knots(end), numP)));
            
            
            
            
        end
        
        
    otherwise
        numP = 50;
        % every assigned object gets updated
        for i=find(assignement),
            spo = objects{i}.state;
            sporho = spo.knots;
            spm = objects{i}.oldState;
            spmrho = spm.knots;
            rhoTest = linspace(min(spmrho(1), sporho(1)), min(spmrho(end), sporho(end)), numP);
            signedDistTheta = mean(fnval(fnxtr(spm),rhoTest) - fnval(fnxtr(spo),rhoTest))-param.snoutOrientationDifference;
            if signedDistTheta > 6,
                signedDistTheta = signedDistTheta - 2*pi;
            elseif signedDistTheta < -6,
                signedDistTheta = signedDistTheta + 2*pi;
            end
            
            objects{i}.oldState = objects{i}.state;
            objects{i}.state = measurements{assignement(i)};
            
            objects{i}.theCost = param.costMatrixObjects{1}(assignement(i),i);
            
            newspeed = signedDistTheta;
            objects{i}.acceleration = 1/2*(objects{i}.speed - newspeed);
            objects{i}.speed = newspeed;
            objects{i}.flagNotAssigned = false;
            objects{i}.framesNotAssigned = param.framesNotAssigned;
            s.minTheta = min(fnval(spo, linspace(spo.knots(1), spo.knots(end), numP)));
            s.maxTheta = max(fnval(spo, linspace(spo.knots(1), spo.knots(end), numP)));
        end
end

%***********************************************
%    update motion model -    10.9.2011
%***********************************************
alpha = 0.4;
beta = 0.6;
pOrder = 2;
for i=1:numel(objects)
    temp = polyval(objects{i}.tscan,objects{i}.rscan) + polyval(objects{i}.velocity, objects{i}.rscan);
    if assignement(i) == 0,
        % increase search area and keep prediction
        objects{i}.sigma = min(0.35,2*objects{i}.sigma);
        objects{i}.p =  polyval(objects{i}.tscan, objects{i}.rscan) + polyval(objects{i}.velocity, objects{i}.rscan);
    else
        objects{i}.p = fnval(fnxtr(objects{i}.state), objects{i}.rscan);
        % update
        if objects{i}.OcclusionTime == param.maxOcclusionTime,
            objects{i}.sigma = min(inf,max(param.minSigma,alpha*( median(abs(objects{i}.p - temp))) + (1-alpha) * objects{i}.sigma));
        else
            % increase area to search for diocclusion
            objects{i}.sigma = max(param.minSigma,alpha*( median(abs(objects{i}.p - temp))) + (1-alpha) * objects{i}.sigma);
        end
        vdiff = beta * (objects{i}.p - polyval(objects{i}.tscan, objects{i}.rscan))+...
            (1 - beta) *  polyval(objects{i}.velocity,objects{i}.rscan);
        objects{i}.velocity = polyfit(objects{i}.rscan, vdiff, pOrder);
    end
    pval = alpha * objects{i}.p  + (1-alpha) * polyval(objects{i}.tscan,objects{i}.rscan);
    objects{i}.tscan = polyfit(objects{i}.rscan, pval, pOrder);
end







% every unassigned object is flagged with status
% not assigned, its state is updated only with prediciton
% and its flag starts counting down in update
for i=find((assignement==0)),
    objects{i}.flagNotAssigned = true;
    objects{i}.framesNotAssigned =  objects{i}.framesNotAssigned - 1;
    objects{i}.state = objects{i}.predictedState;
end



% every new object, if assigned enters the list of object to
% be displayed, otherwise it exits the list
for i=1:numel(objects)
    if objects{i}.flagIsNew,
        if assignement(i) && (sum(assignement == assignement(i))==1)
            % chnage status
            objects{i}.flagIsNew = false;
        else
            % to be deleted
            objects{i}.framesNotAssigned = 0;
        end
    end
end



% update object postion and delete them
updatedObjectsIdx = true(1,numel(objects));
for i=1:numel(objects)
    if objects{i}.framesNotAssigned == 0,
        updatedObjectsIdx(i) = 0;
    end
end
updatedObjects = objects(updatedObjectsIdx);
assignedObjects = assignement(updatedObjectsIdx);