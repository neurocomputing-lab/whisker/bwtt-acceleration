
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [pts, h] = plotData(object, snoutTrackingData, color, w, numP)
%*************************************************************************
%  Author: Igor Perkon   -    perkon@sissa.it
%***********************************************

% <\inDOC>
% Inputs:
%       - objects is the polar coordinate based spline
%       - snoutTrackingData encodes the snout position
%       - color
%
% The function assigns measurements and updates position and speed of the
% tracked object - it also deletes old objects

if nargin < 5
	numP = 50;
end

if nargin < 4
	w = 1;
end
if nargin < 3,
	color = [ 0 0 1];
end


rhoTest = linspace(object.knots(1), object.knots(end), numP);
pts = fnval(object, rhoTest);

[xpts, ypts] = pol2cart(pts, rhoTest);
xpts = xpts + snoutTrackingData.center(2);
ypts = ypts + snoutTrackingData.center(1);

% MITCH
if w > 0,
	h = plot(xpts, ypts, 'color', color,'linewidth',w);
else
	h = [];
end
% MITCH

pts = [xpts; ypts];
