
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function  [X, bTemplate, W, Q0, pp] = ModelFromPoints(pp, a1, b1, a2, b2)

[W, Q0, UR, bTemplate, X, H] = getSplineSpaceParameters(pp);

X(1) = a1;
X(2) = b1;

%theta = -asin((X(1)-a)/(X(2)-b));
%scale = sqrt((X(1)-a).^2 + (X(2)-b).^2);
[theta, r] = cart2pol(X(1)-a2,X(2)-b2);

X(3) = r/80*(cos(theta+pi/2)) - 1;
X(4) = r/80*(sin(theta+pi/2));

% hold on
% plot(X(2),X(1),'rx')
% plot(largestBoundary(2,:), largestBoundary(1,:),'r')

% update


q = (W*X + Q0);
bTemplate.coefs =[q([1:numel(q)/2]), q([numel(q)/2+1:end])]';
pp = fn2fm(bTemplate,'pp');