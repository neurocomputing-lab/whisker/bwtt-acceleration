
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function pars = stShapeSpaceKalmanParameters()

% stShapeSpaceKalmanParameters.m
%
% Outputs:
%           -stParameters is a struct with the following parameters
%           *)stParameters.numberOfProbePoints is the number of probe points
%           used for measuring distance
%           *)stParameters.maxDistance is the maximum distance allowed
%           - previousChunkData are the data initialized in the previous
%           step
%           *) previousChunkData.X is the internal value of the shape space
%           *) previousChunkData.bTemplate is the pp form of the snout template
%           *) previousChunkData.W is the shape space matrix
%           *) previousChunkData.Q0 is the average displacement vector
%           *) previousChunkData.pp is the snout spline template
%           *) previousChunkData.A is the internal matrix dynamics
%           *) previousChunkData.H is the I/O matrix
%           *) previousChunkData.Q is the state noise matrix
%           *) previousChunkData.R is the measurements noise matrix
%           *) previousChunkData.Xs is the state matrix
%           *) previousChunkData.P is the error covariance matrix
%           - frameChunk is the number of frames required to the loader
%           - frameJump is the jump in the buffer
%           - flagGetDataFromSD is a flag setting if the snout tracker
%           needs data from SD
%
%*************************************************************************
%  Author: Perkon Igor    -    perkon@sissa.it



% empty
pars = {};



%% CONSTANTS

% constant (range is 20 to 400)
par = [];
par.name = 'numberOfProbePoints';
par.value = 50;
pars{end+1} = par;

% constant
par = [];
par.name = 'automaticInitialize';
par.value = false;
pars{end+1} = par;

% constant (range is 0 to 1)
par = [];
par.name = 'scale';
par.value = 0.6;
pars{end+1} = par;

% constant (range is -PI to PI)
par = [];
par.name = 'orientation';
par.value = 0;
pars{end+1} = par;

% constant
par = [];
par.name = 'initializeFlag';
par.value = false;
pars{end+1} = par;

% constant
par = [];
par.name = 'initialXState';
par.value = zeros(6, 1);
pars{end+1} = par;

% constant
par = [];
par.name = 'centroid';
par.value = [0 0];
pars{end+1} = par;



%% VARIABLES

% variable
par = [];
par.name = 'maxDistance';
par.type = 'scalar';
par.label = 'Max distance in one frame change';
par.value = 15;
par.range = [2 50];
par.step = [1 5];
pars{end+1} = par;

% variable
par = [];
par.name = 'noseTip';
par.label = 'Nose Tip Location';
par.type = 'xy';
par.symbol = 'rx';
par.value = [NaN NaN];
par.propagate = false;
pars{end+1} = par;

% variable
par = [];
par.name = 'snoutCenter';
par.label = 'Snout Center Location';
par.type = 'xy';
par.symbol = 'yo';
par.value = [NaN NaN];
par.propagate = false;
pars{end+1} = par;

% variable
par = [];
par.name = 'nIter';
par.label = 'Number of iterations';
par.type = 'scalar';
par.value = 1;
par.range = [1 5];
par.step = [1 1];
pars{end+1} = par;

% variable
%
% changed from a constant to a variable by mitch 26/05/2011
% because i needed to change it to do Coolization
par = [];
par.name = 'flagAllowScalingOfTemplateDuringTracking';
par.label = 'Allow scaling during tracking';
par.type = 'flag';
par.help = 'If this flag is set, the template can be scaled (in size) during tracking. This may be useful if you are lazy at setting the initial snout parameters, but will make tracking less reliable.';
par.value = false;
pars{end+1} = par;











