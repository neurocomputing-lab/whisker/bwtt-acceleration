/*
 * (mex_ggy121009.cpp)
 *  inversion
 *
 *  Created by Dr Vladan Rankov, Dr Georges Gyory  on 23/03/2010.
 	Adaptive Behaviour Research Group
	Department of Psychology
	University of Sheffield
	Western Bank
	Sheffield S10 2TP
 *

 rebuilt 2010/2011 Ben Mitch & Vladan Rankov



%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2012 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====
*/



/* 

$$$$		marks values you have to change for treating a new dataset

*/

//#include <stdafx.h> 
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <iostream>
#include <string>
#include <sstream>
#include <vector>
#include "windows.h"
using namespace std;

#include "mex.h"



typedef unsigned char uchar;

const float PI = 3.141592653;

//		tMax
const int HhHorSize = 360;
//		rMax
//	$$$$	image Y size probably best
const int HhVerSize = 768;
//		max length of gap in inverted image in a whisker's image
const int maxGapLength = 80;
//		half rect size (2 -> 5) for finding maxima
const int hhMaxRectSize = 4;
//		allows 360 * hhTauQuantile angle from vertical for finding maxima
const float hhTauQuantile = 0.025;
//		same on the "wrong" side for finding maxima
//		(set it to the same value for symmetrical treatment)
//	#define		hhTauAllowance	0.017
const float hhTauAllowance = 0.025;

/* the parameters below are set from the par structure passed in to the mex function */

//	parameter
int erodeRadius = 0; // was 6, tried other values (2) when working with small mice whiskers
//	$$$$	raduis of cirle of inversion, ~ 110 - 160
//			- depends on the size of the head
float radInv = 0.0;
//	$$$$	min number of points on a line
int hhThreshold = 0; // default = 160
int erodeBtoW = 0; // default = 10
int edgeDetectLevelNum = 0; // default =32
unsigned int xSize = 0;
unsigned int ySize = 0;




struct	wPoint 
{
	float			x;
	float			y;
}								;
struct	tangentData 
{
	int				threadNo;
	int				frameNo;
	int				extrapol;
	int				r;
	int				tau;
	float			y;
	float			x;
	float			angle;
}								;



//	tangents
const int sequenceLength = 100;
const int MAX_TANGENTS = 200 * sequenceLength;
int nextTangentIndex = 0;
struct tangentData tangents[MAX_TANGENTS];




float			Square (float x);
int				compar (const void *A, const void *B);
struct			wPoint urPunkt (struct	wPoint in, float centerX, float centerY, float bearingRad);
//void			treatHoughMax (int r, int tau, struct tangentData tangents[]);
void			treatHoughMax (vector<DOUBLE>& out, vector<DOUBLE>& outWide, unsigned char* combiOut, unsigned char* dataBuf2, int r, int tau, float centerX, float centerY, float bearingRad);
void			Fill1 (unsigned char *dataIn, unsigned char *dataOut, float ii, float jj, int i, int j, int ifCut);
void			Fill (unsigned char *dataIn, unsigned char *dataOut, float ii, float jj, int i, int j);

unsigned char* LoadPGM(const char* filename, int xSize, int ySize);
void SavePGM (const char *filename, unsigned char *data, int xSize, int ySize);


inline unsigned char* newBytes(int N)
{
	unsigned char* buf = new unsigned char[N];
	if (!buf) throw "new failed to throw";
	return buf;
}

inline unsigned int* newUnsignedInts(int N)
{
	unsigned int* buf = new unsigned int[N];
	if (!buf) throw "new failed to throw";
	return buf;
}

inline string n2s(double n)
{
	stringstream ss;
	ss << n;
	return ss.str();
}


/*******			main		********/

void mexFunction(
				 int nlhs,
				 mxArray *plhs[],
				 int nrhs, 
				 const mxArray *prhs[]
)
{
    try
	{
        //  WHISKERS = func(image, mask, [y x head_bearing])
        //
        //  "WHISKERS" is an Mx11 matrix, with each row representing a found
        //  whisker. first three entries in each row are x,y of base, and
        //  whisker angle.

        if (nlhs < 1 || nlhs > 2)
            throw ("expects one or two output arguments");

        if (nrhs != 3)
            throw("expects three input arguments");
        
        ySize = mxGetM(prhs[0]);
        xSize = mxGetN(prhs[0]);

        if (!mxIsUint8(prhs[0]))
            throw("img arg invalid");

        if (!(mxIsUint8(prhs[1]) && mxGetM(prhs[1]) == ySize && mxGetN(prhs[1]) == xSize))
            throw("mask arg invalid");

        /*
        if (!(mxIsDouble(prhs[2]) && mxGetM(prhs[2]) == 1 && mxGetN(prhs[2]) == 3))
            throw("data arg invalid");
         */
        if (!(mxIsStruct(prhs[2]) && mxGetM(prhs[2]) == 1 && mxGetN(prhs[2]) == 1))
            throw("data arg invalid");

        
    unsigned int imageBytes = xSize * ySize;
        
        
        
//  define this to avoid overwriting the passed-in image
#define USE_COPY_OF_INPUT_DATA


#ifdef USE_COPY_OF_INPUT_DATA

        uchar* p_img_ro = (uchar*)mxGetData(prhs[0]);
        vector<uchar> data_img;
        data_img.resize(imageBytes);
        uchar* p_img = &data_img[0];
        memcpy(p_img, p_img_ro, imageBytes);

#else

        uchar* p_img = (uchar*)mxGetData(prhs[0]);

#endif

        uchar* p_mask = (uchar*)mxGetData(prhs[1]);

        
        //  construct debug output
        struct Debug
        {
            Debug()
            {
                memset(this, 0, sizeof(Debug));
                on = false;
            }
            
            bool on;
            
            uchar* input;
            uchar* posterode;
            uchar* postedgedetect;
            uchar* postmaskerase;
            uchar* databufblack;
            uchar* postsobel;
            uchar* posttransform1;
            uchar* posttransform2;
            //uchar* final;
            uchar* combiout;
        }
        debug;
        
        if (nlhs >= 2)
        {
            debug.on = true;
            
            const char* fieldnames[] = {"input", "posterode", "postedgedetect", "postmaskerase",
                    "databufblack", "postsobel", "posttransform1", "posttransform2"/*, "final"*/, "combiout"};
            plhs[1] = mxCreateStructMatrix(1, 1, 9, fieldnames);
            
            mxArray* temp;
            int fieldNumber = 0;
            
            temp = mxCreateNumericMatrix(ySize, xSize, mxUINT8_CLASS, mxREAL);
            mxSetFieldByNumber(plhs[1], 0, fieldNumber++, temp);
            debug.input = (uchar*) mxGetData(temp);
            
            temp = mxCreateNumericMatrix(ySize, xSize, mxUINT8_CLASS, mxREAL);
            mxSetFieldByNumber(plhs[1], 0, fieldNumber++, temp);
            debug.posterode = (uchar*) mxGetData(temp);
            
            temp = mxCreateNumericMatrix(ySize, xSize, mxUINT8_CLASS, mxREAL);
            mxSetFieldByNumber(plhs[1], 0, fieldNumber++, temp);
            debug.postedgedetect = (uchar*) mxGetData(temp);
            
            temp = mxCreateNumericMatrix(ySize, xSize, mxUINT8_CLASS, mxREAL);
            mxSetFieldByNumber(plhs[1], 0, fieldNumber++, temp);
            debug.postmaskerase = (uchar*) mxGetData(temp);
            
            temp = mxCreateNumericMatrix(ySize, xSize, mxUINT8_CLASS, mxREAL);
            mxSetFieldByNumber(plhs[1], 0, fieldNumber++, temp);
            debug.databufblack = (uchar*) mxGetData(temp);
            
            temp = mxCreateNumericMatrix(ySize, xSize, mxUINT8_CLASS, mxREAL);
            mxSetFieldByNumber(plhs[1], 0, fieldNumber++, temp);
            debug.postsobel = (uchar*) mxGetData(temp);
            
            temp = mxCreateNumericMatrix(ySize, xSize, mxUINT8_CLASS, mxREAL);
            mxSetFieldByNumber(plhs[1], 0, fieldNumber++, temp);
            debug.posttransform1 = (uchar*) mxGetData(temp);
            
            temp = mxCreateNumericMatrix(ySize, xSize, mxUINT8_CLASS, mxREAL);
            mxSetFieldByNumber(plhs[1], 0, fieldNumber++, temp);
            debug.posttransform2 = (uchar*) mxGetData(temp);

            /*
            temp = mxCreateNumericArray(ySize, xSize, mxUINT8_CLASS, mxREAL);
            mxSetFieldByNumber(plhs[1], 0, fieldNumber++, temp);
            debug.final = (uchar*) mxGetData(temp);
             */
            
            temp = mxCreateNumericMatrix(xSize + HhHorSize, ySize, mxUINT8_CLASS, mxREAL);
            mxSetFieldByNumber(plhs[1], 0, fieldNumber++, temp);
            debug.combiout = (uchar*) mxGetData(temp);
        }

        
        
        
        
		float centerX, centerY, bearingRad; 

        //  extract parameters
        mxArray* field;
        double* p_data;
        
        //  par
        field = mxGetField(prhs[2], 0, "whiskerCentre");
        if (!field) throw("missing field from pars");
        p_data = (double*)mxGetData(field);
        centerX = p_data[0];
        centerY = p_data[1];
        
        //  par
        field = mxGetField(prhs[2], 0, "headBearing");
        if (!field) throw("missing field from pars");
        p_data = (double*)mxGetData(field);
        bearingRad = p_data[0];

        //  par
        field = mxGetField(prhs[2], 0, "erodeRadius");
        if (!field) throw("missing field from pars");
        p_data = (double*)mxGetData(field);
        erodeRadius = p_data[0];

        //  par
        field = mxGetField(prhs[2], 0, "radInv");
        if (!field) throw("missing field from pars");
        p_data = (double*)mxGetData(field);
        radInv = p_data[0];

        //  par
        field = mxGetField(prhs[2], 0, "hhThreshold");
        if (!field) throw("missing field from pars");
        p_data = (double*)mxGetData(field);
        hhThreshold = p_data[0];

        //  par
        field = mxGetField(prhs[2], 0, "erodeBtoW");
        if (!field) throw("missing field from pars");
        p_data = (double*)mxGetData(field);
        erodeBtoW = p_data[0];
        
        field = mxGetField(prhs[2], 0, "edgeDetectLevelNum");
        if (!field) throw("missing field from pars");
        p_data = (double*)mxGetData(field);
        edgeDetectLevelNum = p_data[0];
        

        ////////////////	PREAMBLE

		unsigned int xFile, yFile;

		//		500 - 561
		//	$$$$	mask for cutting out background (white)
		//char* maskFilename		= "050811-0005 mask.pgm";

		//	$$$$	name of file with positions, 
		//			output of the   WhiskerMark   app.
		//				posFilename[64] = "whiskermark/050811-0005 101.txt", 
		//char posFilename[64]	= "whiskermark/050811-0005 175 7.txt"; 
		//				posFilename[64] = "whiskermark/050811-0005 500 5.txt", 



		////////////////	LOAD MASK IMAGE AND CREATE WORKING MEMORY

		//unsigned char* dataMask = LoadPGM(maskFilename, xSize, ySize);
        unsigned char* dataMask = p_mask;

		unsigned char* dataOut = newBytes (imageBytes);
		unsigned char* dataBuf = newBytes (imageBytes);
		unsigned char* dataBufBlack = newBytes (imageBytes);
		unsigned char* dataBuf1 = newBytes (imageBytes);
		unsigned char* dataBuf2 = newBytes (imageBytes * 3);
		unsigned char* combiOut = newBytes ((xSize + HhHorSize) * ySize);

		unsigned int* Hough = newUnsignedInts (HhHorSize * HhVerSize);





		////////////////	A FEW PREPARATIONS

		//	put white border down side of combiOut
		for (int k = 0; k < ySize; k++)
			for (int l = xSize - 2; l < xSize + 10; l++)
				combiOut [(xSize + HhHorSize) * k + l] = 255;

        /*
		//	open position file (will be read during loop, below)
		FILE* posnFilePtr = fopen(posFilename, "r");
		if (posnFilePtr == NULL)
			throw "failed open position file";

		//	open output file
		FILE* txtFile = fopen("whiskerPos.txt", "w");
		if(txtFile == NULL)
			throw "failed open txtFile";

		//	open output file
		FILE* txtFileWide = fopen("whiskerPosWide.txt", "w");
		if(txtFileWide == NULL)
			throw "failed open txtFileWide";
         */



////////////////	MAIN LOOP

		//for (imNum = 180; imNum < 251; imNum += 1)

        vector<DOUBLE> out;
        vector<DOUBLE> outWide;

		//imNum = 123; //*mxGetPr(prhs[0]);
		{

			//	get position data from position file
            /*
			int frameNum = -1;
			while(true)
			{
				if (fscanf (posnFilePtr, "%d %g %g %g", &frameNum, &bearingRad, &centerX, &centerY) != 4)
					throw string("ran out of data in position file (perhaps frame ") + n2s(imNum) + " is not included?)";

				if (frameNum == imNum) break;
			}
             */

            //int frameNum = 123;
			//cout << "F" << imNum << ": " << bearingRad << ", " << centerX << ", " << centerY << endl;


            //	write to txtFile
			//fprintf (txtFile, "-1.0\t%d\t%f\n", frameNum, bearingRad);

			//	read input image
			//unsigned char* dataIn = LoadPGM(inFilename.c_str(), xSize, ySize);
            unsigned char* dataIn = p_img;

			//	prepare output images
			memset(dataOut, 255, imageBytes);
			memcpy(dataBuf, dataIn, imageBytes);

			//	copy into combined output
			for (int k = 0; k < ySize; k++)
			{
				for (int l = 0; l < xSize; l++)
				{
					combiOut [(xSize + HhHorSize) * k + l] = dataIn [(xSize) * k + l];
					dataBuf2 [((xSize) * k + l) * 3 + 0] = 
						dataBuf2 [((xSize) * k + l) * 3 + 1] = 
						dataBuf2 [((xSize) * k + l) * 3 + 2] = dataIn [(xSize) * k + l] / 2 + 128;
				}
			}



			//	draw white cross at centre of transform
			int zz = (xSize + HhHorSize) * floor(centerY) + floor(centerX);
			combiOut [zz] = 255;
			combiOut [zz - 1] = 255;
			combiOut [zz + 1] = 255;
			combiOut [zz - xSize - HhHorSize] = 255;
			combiOut [zz + xSize + HhHorSize] = 255;





            //  debug
            if (debug.on)
                memcpy(debug.input, dataBuf, ySize * xSize * sizeof(uchar));


	////////////////	ERODE BLACK TO WHITE

			//	pre-calculate "black" flag
			for (int b=0; b<imageBytes; b++)
				dataBufBlack[b] = dataIn[b] < erodeBtoW; // georges' original value here was 10 for clip 050811-0005, which has white level of about 120, so we're going to normalise white level to 200, so perhaps 18 (20) is consistent

			//	pre-calculate offsets
			const int o1 = -2 * xSize -2;
			const int o2 = -2 * xSize +0;
			const int o3 = -2 * xSize +2;
			const int o4 = +0 * xSize -2;
			const int o5 = +0 * xSize +0;
			const int o6 = +0 * xSize +2;
			const int o7 = +2 * xSize -2;
			const int o8 = +2 * xSize +0;
			const int o9 = +2 * xSize +2;

			//	erode black zones
			for (int k = erodeRadius; k < ySize - erodeRadius; k++)
			{
				for (int l = erodeRadius; l < xSize - erodeRadius; l++)
				{
					int i = l * xSize + k;

					if (
						dataBufBlack[i + o1] &&
						dataBufBlack[i + o2] &&
						dataBufBlack[i + o3] &&
						dataBufBlack[i + o4] &&
						dataBufBlack[i + o5] &&
						dataBufBlack[i + o6] &&
						dataBufBlack[i + o7] &&
						dataBufBlack[i + o8] &&
						dataBufBlack[i + o9]
					)
					{
						for (int kk = -erodeRadius; kk <= erodeRadius; kk++)
							for (int ll = -erodeRadius; ll <= erodeRadius; ll++)
								dataBuf[(l + ll) * xSize + k + kk] =  255;
					}
				}
			}




            //  debug
            if (debug.on)
                memcpy(debug.posterode, dataBuf, ySize * xSize * sizeof(uchar));



	////////////////	EDGE DETECTION

            //  parameter
            unsigned char edgeDetectLevel = edgeDetectLevelNum; // georges' original value here was 32

			//	blank out homogeneous zones
			for (int k = 1; k < ySize - 1; k++)
			{
				for (int l = 1; l < xSize - 1; l++)
				{
					int i = l * xSize + k;
					if (
						abs (dataIn[i - xSize - 1] - dataIn[i - xSize]) < edgeDetectLevel &&
						abs (dataIn[i - xSize] - dataIn[i - xSize + 1]) < edgeDetectLevel &&
						abs (dataIn[i - xSize + 1] - dataIn[i + 1]) < edgeDetectLevel &&
						abs (dataIn[i + 1] - dataIn[i + xSize + 1]) < edgeDetectLevel &&
						abs (dataIn[i + xSize + 1] - dataIn[i + xSize]) < edgeDetectLevel &&
						abs (dataIn[i + xSize] - dataIn[i + xSize - 1]) < edgeDetectLevel &&
						abs (dataIn[i + xSize - 1] - dataIn[i - 1]) < edgeDetectLevel &&
						abs (dataIn[i - 1] - dataIn[i - xSize - 1]) < edgeDetectLevel
						)
					{
						dataBuf[i - xSize - 1] = dataBuf[i - xSize] = dataBuf[i - xSize + 1] = 
							dataBuf[i - 1] = dataBuf[i] = dataBuf[i + 1] = 
							dataBuf[i + xSize - 1] = dataBuf[i + xSize] = dataBuf[i + xSize + 1] = 255;
					}
				}
			}




            //  debug
            if (debug.on)
                memcpy(debug.postedgedetect, dataBuf, ySize * xSize * sizeof(uchar));


			//	Erase according to mask
			for (int i = 0; i < imageBytes; i++)
				dataIn[i] = (dataBuf[i] < dataMask[i] ? dataMask[i] : dataBuf[i]);	

                
            //  debug
            if (debug.on)
                memcpy(debug.postmaskerase, dataIn, ySize * xSize * sizeof(uchar));

            //  debug
            if (debug.on)
            {
                for (int b=0; b<imageBytes; b++) dataBufBlack[b] = (dataBufBlack[b] ? 0 : 255);
                memcpy(debug.databufblack, dataBufBlack, ySize * xSize * sizeof(uchar));
            }


                
                

			//	Sobel
			for (int k = 1; k < ySize - 1; k++)
			{
				for (int l = 1; l < xSize - 1; l++)
				{
					int sobelBuf = 5 * dataIn[(l) * xSize + k] 
					- dataIn[(l - 1) * xSize + k] - dataIn[(l + 1) * xSize + k] 
					- dataIn[(l) * xSize + k - 1] - dataIn[(l) * xSize + k + 1];
					if (sobelBuf < 0) sobelBuf = 0;
					if (sobelBuf > 128) sobelBuf = 255;
					dataIn[(l) * xSize + k] = sobelBuf;
				}
			}
			


            //  debug
            if (debug.on)
                memcpy(debug.postsobel, dataIn, ySize * xSize * sizeof(uchar));


	////////////////	GEOMETRIC TRANSFORM

			/*
			//		GEOMETRIC IMAGE TRANSFORMATION
			struct	wPoint		inPt, outPt;

			for (i =  0; i < ySize ; i++) {
			for (j =  0; j < xSize ; j++) {
			inPt.x = j; inPt.y = i;
			outPt = urPunkt (inPt, centerX, centerY, bearingRad);
			Fill1 (dataBuf, dataBuf1, outPt.y, outPt.x, i, j, 1);

			}}
			*/

			//	TRANSFORM IMAGE (alternative)
			for (int y_dst =  0; y_dst < ySize ; y_dst++)
			{
				for (int x_dst =  0; x_dst < xSize ; x_dst++)
				{
					//	shift
					float y_abstract = ((float)y_dst) - ((float)ySize) / 2.0 + 0.01;
					float x_abstract = ((float)x_dst) - ((float)xSize) / 2.0 + 0.01;

					//	inversion
					float rad_squared = x_abstract * x_abstract + y_abstract * y_abstract;
					float corrFact = (radInv * radInv) / rad_squared;
					float x_src = x_abstract * corrFact + centerX;
					float y_src = y_abstract * corrFact + centerY;

					Fill1 (dataIn, dataBuf, y_src, x_src, y_dst, x_dst, 0);		//		still grayscale
					
					//dataBuf[y_dst * xSize + x_dst] = rad_squared;
					//		Fill1 (dataIn, dataBuf1, ic, jc, i, j, 1);		//		and make B/W
				}
			}


            //  debug
            if (debug.on)
                memcpy(debug.posttransform1, dataBuf, ySize * xSize * sizeof(uchar));


//			throw "stop";

			//	rect -> rad
			for (int i =  0; i < ySize ; i++)
			{
				for (int j =  0; j < xSize ; j++)
				{
					float ja = ((float)(i) / 2) * sin (((j * 2 * PI) / xSize)  + (bearingRad + PI / 2)) + xSize / 2;
					float ia = ((float)(i) / 2) * cos (((j * 2 * PI) / xSize)  + (bearingRad + PI / 2)) + ySize / 2; 

					Fill1 (dataBuf, dataBuf1, ia, ja, i, j, 1);
				}
			}		


            //  debug
            if (debug.on)
                memcpy(debug.posttransform2, dataBuf1, ySize * xSize * sizeof(uchar));


			//	copy into combined output
			for (int k = 0; k < ySize; k++)
				for (int l = 0; l < xSize; l++)
					combiOut [(xSize + HhHorSize) * k + l] = dataBuf1 [(xSize) * k + l];



	////////////////	HOUGH TRANSFORM

			//	Initialize the Hough array
			memset(Hough, 0, (HhHorSize * HhVerSize) * sizeof(unsigned int));
			//			for (int i = 0; i < (HhHorSize * HhVerSize); i++) Hough [i] = 0;

			//		Fill Hough array
			for (int y=0 ; y< ySize ; y++)
			{
				for (int x=0; x< xSize; x++)
				{
					if ( dataBuf1[y * xSize + x] < 130)
					{
						//			for (tau = 0 ; tau < HhHorSize ; tau ++){
						//	$$$$	.. or just fill in part used (for near vertical lines)
						for (int tau = (int)((0.5 - hhTauQuantile) * HhHorSize) - hhMaxRectSize; 
							tau < (int)((0.5 + hhTauQuantile) * HhHorSize) + hhMaxRectSize; 
							tau++)
						{
							int r = int(
                                    ((x) * cos(2 * PI * tau / HhHorSize) + (y) * sin(2 * PI * tau / HhHorSize)) 
								* HhVerSize / ySize + HhVerSize
                                    );

                            if (r > 0 && r < HhVerSize) Hough [r * HhHorSize + tau]++;
						}
					}
				}
			}

			// find strong maxima in Hough array

			// One side
			for (int r = hhMaxRectSize; r < HhVerSize / 2 - hhMaxRectSize; r++)	
			{
				for (int tau = (int)((0.5 - hhTauQuantile) * HhHorSize); tau < (int)((0.5 + hhTauAllowance) * HhHorSize); tau++)	
				{
					if (Hough[r * HhHorSize + tau] > hhThreshold)
					{
						// Check (2 * hhMaxRectSize + 1) * (2 * hhMaxRectSize + 1) neighborhood for max 
						int ifMax = 1;
						for (int i = -hhMaxRectSize; i <= hhMaxRectSize; i++)
						{
							int dr = r + i;
							for (int dt = (tau - hhMaxRectSize); dt <= (tau + hhMaxRectSize); dt++)
							{
								if ((dr != r || dt != tau) && (Hough[dr * HhHorSize + dt] > Hough[r * HhHorSize + tau]))
								{
									ifMax = 0;
									break;
								}
							}
						}

						// Process Hough array to create output 
						if (ifMax == 1) treatHoughMax (
                                out, outWide,
                                combiOut, dataBuf2, r, tau, centerX, centerY, bearingRad);		//, tangents
					}
				}
			}

			// Other side
			for (int r = HhVerSize / 2 + hhMaxRectSize; r < HhVerSize - hhMaxRectSize; r++)	
			{
				for (int tau = (int)((0.5 - hhTauAllowance) * HhHorSize); tau < (int)((0.5 + hhTauQuantile) * HhHorSize); tau++)	
				{
					if (Hough[r * HhHorSize + tau] > hhThreshold)
					{
						// Check (2 * hhMaxRectSize + 1) * (2 * hhMaxRectSize + 1) neighborhood for max 
						int ifMax = 1;
						for (int i = - hhMaxRectSize; i <= hhMaxRectSize; i++)
						{
							int dr = r + i;
							for (int dt = (tau - hhMaxRectSize); dt <= (tau + hhMaxRectSize); dt++)
							{
								if ((dr != r || dt != tau) && (Hough[dr * HhHorSize + dt] > Hough[r * HhHorSize + tau]))
								{
									ifMax = 0;
									break;
								}
							}
						}

						// local maximum found 
						if (ifMax == 1) treatHoughMax (
                                out, outWide,
                                combiOut, dataBuf2, r, tau, centerX, centerY, bearingRad);
					}
				}
			}

			//	find maximum of transformed data
			int hMax = 0;
			for (int i = 0; i < (HhHorSize * HhVerSize); i++)
			{
				if (Hough [i] > hMax)
					hMax = Hough [i];	
			}

            /*
			//	copy junk into dataOut
			for (int i = 0; i < (HhHorSize * HhVerSize); i++)
			{
                int junk = Hough[i];
				junk = (255 * junk) / hMax;
				if (junk < 0) junk = 0;
                if (junk > 255) junk = 255;
				dataOut[i] = junk;	
			}
             */

			//	copy Hh into combined output
			for (int k = 0; k < ySize; k++)
			{
				for (int l = 0; l < HhHorSize; l++)
				{
					if (k >= HhVerSize)
						combiOut [(xSize + HhHorSize) * k + l + xSize] = 64;
					else
						combiOut [(xSize + HhHorSize) * k + l + xSize] = Hough [(HhHorSize) * k + l];
				}
			}





	////////////////	FINALLY

            //  debug
            /*
            if (debug.on)
                memcpy(debug.final, dataBuf2, ySize * xSize * sizeof(uchar));
             */

            //  debug
            if (debug.on)
                memcpy(debug.combiout, combiOut, ySize * (xSize + HhHorSize) * sizeof(uchar));


			//	clean up
			//delete dataIn;
		}




		//qsort (tangents, (nextTangentIndex - 1), sizeof(struct tangentData), compar);

		/*
		for (int l = 0; l < nextTangentIndex; l++)
		{
			fprintf (txtFileWide, "%d\t%d\t%d\t%d\t%d\t%f\t%f\t%f\n", tangents[i].threadNo, tangents[i].frameNo, tangents[i].extrapol, 
				tangents[i].r, tangents[i].tau, tangents[i].x, tangents[i].y, tangents[i].angle);
		}
		*/



		////////////////	CLEAN UP

		//	free
		//delete dataMask;
		delete dataOut;
		delete dataBuf;
		delete dataBufBlack;
		delete dataBuf1;
		delete dataBuf2;
		delete combiOut;

        /*
		//	position file
		fclose (posnFilePtr);

		//	output files
		fclose (txtFile);
		fclose (txtFileWide);
         */

        //  generate output
        int N = out.size() / 3;
        if (outWide.size() != (N*8))
            throw "oh dear";

        plhs[0] = mxCreateDoubleMatrix(11, N, mxREAL);
        double* p = mxGetPr(plhs[0]);

        for (int n=0; n<N; n++)
        {
            double* p_out = &p[n * 11];
            double* p_outWide = &p[n * 11 + 3];
            memcpy(p_out, &out[n*3], 3*sizeof(double));
            memcpy(p_outWide, &outWide[n*8], 8*sizeof(double));
        }

	}

	catch(const string& e)
	{
		mexErrMsgTxt(e.c_str());
	}

	catch(const char* e)
	{
		mexErrMsgTxt(e);
	}

	catch(...)
	{
		mexErrMsgTxt("(...)");
	}
}






/************	compar		*************/
int compar (const tangentData *a, const tangentData *b)
{
	if (a->threadNo > b->threadNo) return (1);
	if (a->threadNo < b->threadNo) return (-1);
	if (a->frameNo > b->frameNo) return (1);
	if (a->frameNo < b->frameNo) return (-1);
	return (0);
}






/************	urPunkt		*************/
struct	wPoint urPunkt (struct	wPoint in, float centerX, float centerY, float bearingRad)
{
	struct	wPoint		out;
	float				radMm, corrFact, x, y, xx, yy;

	//													//		rect -> rad
	xx = (in.y / 2) * sin (((in.x * 2 * PI) / xSize)  + (bearingRad + PI / 2)) + xSize / 2;
	yy = (in.y / 2) * cos (((in.x * 2 * PI) / xSize)  + (bearingRad + PI / 2)) + ySize / 2;
	x = xx; y = yy;

	//													//		shift
	y = float(yy - ySize / 2 + centerY + 0.01);
	x = float(xx - xSize / 2 + centerX + 0.01);

	//													//		inversion
	radMm = (Square(centerX - x) + Square(centerY - y));
	corrFact = radInv * radInv / radMm;
	out.x = (x - centerX) * corrFact + centerX;
	out.y = (y - centerY) * corrFact + centerY;

	return (out);
}




/************	treatHoughMax		*************/
void treatHoughMax (vector<DOUBLE>& out, vector<DOUBLE>& outWide, unsigned char* combiOut, unsigned char* dataBuf2, int r, int tau, float centerX, float centerY, float bearingRad)		//		, struct tangentData tangents[]
{
	struct	wPoint		ptIn0, ptIn1, ptOut0, ptOut1;
	int					xx, yy, j, k, x, y, first = 1, gapLength = 0, BlkCounter = 0, i;	//	
	float				angle;

	float r_ = r - HhVerSize;
	float fac = ySize;
	float fac2 = fac / HhVerSize;
	float rr = r_ * fac2;
	float tauRadian = 2 * PI * tau / HhHorSize; 
	float sinTau = sin(tauRadian);
	float cosTau = cos(tauRadian);
    //struct * whDrawColour;
    //float PHI;
    //int red, blue, green;

	for (y = ySize - 1 ; y >= 0; y--)
	{
		x = (int) (sinTau * y + rr / cosTau);
		k = ((xSize + HhHorSize) * y + x);

		if ((combiOut [k] == 0) && (first == 1)) {
			ptIn0.x = sinTau * y + rr / cosTau;	
			ptIn0.y = y;
			ptIn1.x = sinTau * (y - 1) + rr / cosTau;	
			ptIn1.y = y - 1;

			gapLength = 0;
			first = 0;
		}

		if ((combiOut [k] == 0) && (first != 1)) {
			gapLength = 0;
			BlkCounter ++;
			if (BlkCounter > hhThreshold - 5) goto DoDraw;
		}

		if ((combiOut [k] == 255)) {
			gapLength ++;
			//	do not draw line
			if (gapLength > maxGapLength && first == 0)		return;
		}
	}		//		end y

	return;
	//		draw line for (r, tau);

DoDraw:
	{
		ptOut0 = urPunkt (ptIn0, centerX, centerY, bearingRad);
		ptOut1 = urPunkt (ptIn1, centerX, centerY, bearingRad);
		angle = atan2(ptOut1.y - ptOut0.y, ptOut1.x - ptOut0.x);

		//	fprintf (stderr, "  %d    %d      %d\n", tau, r, Hough[r * HhHorSize + tau]);
        out.push_back(ptOut0.x);
        out.push_back(ptOut0.y);
        out.push_back((angle - bearingRad) * 180 / PI);
		//fprintf (txtFile, "%f\t%f\t%f\n", ptOut0.x, ptOut0.y, (angle - bearingRad) * 180 / PI);

		if (nextTangentIndex == MAX_TANGENTS)
			throw "increase MAX_TANGENTS and recompile";

		tangents[nextTangentIndex].threadNo = 4000 - nextTangentIndex;
		tangents[nextTangentIndex].frameNo = -1; //imNum;
		tangents[nextTangentIndex].extrapol = 0;
		tangents[nextTangentIndex].r = r;
		tangents[nextTangentIndex].tau = tau;
		tangents[nextTangentIndex].x = ptOut0.x;
		tangents[nextTangentIndex].y = ptOut0.y;
		tangents[nextTangentIndex].angle = (angle - bearingRad) * 180 / PI;
		
		i = nextTangentIndex;

        outWide.push_back(tangents[i].threadNo);
        outWide.push_back(tangents[i].frameNo);
        outWide.push_back(tangents[i].extrapol);
        outWide.push_back(tangents[i].r);
        outWide.push_back(tangents[i].tau);
        outWide.push_back(tangents[i].x);
        outWide.push_back(tangents[i].y);
        outWide.push_back(tangents[i].angle);

		//fprintf (txtFileWide, "%d\t%d\t%d\t%d\t%d\t%f\t%f\t%f\n", tangents[i].threadNo, tangents[i].frameNo, tangents[i].extrapol, 
		//	tangents[i].r, tangents[i].tau, tangents[i].x, tangents[i].y, tangents[i].angle);
//     //   plhs[0] = mxCreateStructMatrix(i, 8, tangents[i]);
		nextTangentIndex++;
/*
       PHI=0.618;
          // draw line in original image
whDrawColour.red = (int)(cos((nextTangentIndex * PHI + 0) * 2 * PI) * 192);
whDrawColour.green = (int)(cos((nextTangentIndex * PHI + 0.333333) * 2 * PI) * 128);
whDrawColour.blue = (int)(cos((nextTangentIndex * PHI + 0.666666) * 2 * PI) * 192);

for (y = ySize - 1 ; y >= 0; y -= 2){
ptIn1.y = y;
ptIn1.x = (int) (sinTau * y + rr / cosTau);
k = ((xSize + HhHorSize) * ptIn1.y + ptIn1.x);
if (combiOut [k] == 0) {
ptOut1 = urPunkt (ptIn1, centerX, centerY, bearingRad);
xx = (int) (ptOut1.x);
yy = (int) (ptOut1.y);
if (xx >= 0 && xx < xSize && yy >= 0 && yy < ySize) {
dataBuf2[((xSize) * yy + xx) * 3 + 0] = whDrawColour.red;
dataBuf2[((xSize) * yy + xx) * 3 + 1] = whDrawColour.green;
dataBuf2[((xSize) * yy + xx) * 3 + 2] = whDrawColour.blue;
}
}}
 */
/*
		//	draw line in original image
		for (j = 0; j < 512; j++) {
			xx = (int) (ptOut0.x + j * cos(angle));
			yy = (int) (ptOut0.y + j * sin(angle));
			if (xx >= 0 && xx < xSize && yy >= 0 && yy < ySize)		dataBuf2[(xSize) * yy + xx] = 0;
		}

		//	draw line in inverted image
		for (y = ySize - 1 ; y >= 0; y--){
			x = (int) (sinTau * y + rr / cosTau);
			k = ((xSize + HhHorSize) * y + x);
			if ((k >= 0) && (k < (xSize + HhHorSize) * ySize))		combiOut [k] = 128;
		}
 */

		return;
	}
}






/************	Fill1	*************/
void Fill1 (unsigned char *dataIn, unsigned char *dataOut, float y_src, float x_src, int i, int j, int ifCut)
{
	int y_src_int = (int) floor (y_src);
	float y_src_frac = y_src - y_src_int;
	int x_src_int = (int) floor (x_src);
	float x_src_frac = x_src - x_src_int;
	int indx = (i * xSize + j);
	int indxx = (y_src_int * xSize + x_src_int);

	if (y_src_int >= 0 && x_src_int >= 0 && y_src_int < (ySize - 1) && x_src_int < (xSize - 1))
	{
		//	within src image
		//	take average over four nearby pixels to get estimate of src brightness at a fractional location
		//	and store it in dataOut at the dst location
		dataOut[indx] = (int) (	  (1 - y_src_frac) * (1 - x_src_frac) * (float)dataIn[indxx + 0]
			+ (1 - y_src_frac) * x_src_frac * (float)dataIn[indxx + 0 + 1] 
			+ y_src_frac * (1 - x_src_frac) * (float)dataIn[indxx + 0 + 1 * xSize]
			+ y_src_frac * x_src_frac * (float)dataIn[indxx + 0 + 1 * xSize + 1]	);

		if (ifCut)
		{
			if (dataOut[indx] < 128) dataOut[indx] = 0; else dataOut[indx] = 255;
		}
	}
	else
	{
		//	without src image (fill with grey)
		dataOut[indx] = 192;
	}

}




/************	Fill	*************/
void Fill (unsigned char *dataIn, unsigned char *dataOut, float ii, float jj, int i, int j)
{
	int			ibuf, jbuf, indx, indxx;
	float		ifrac, jfrac;

	ibuf = (int) floor (ii);		ifrac = ii - ibuf;
	jbuf = (int) floor (jj);		jfrac = jj - jbuf;
	indx = 3 * (i * xSize + j);
	indxx = 3 * (ibuf * xSize + jbuf);

	if (ibuf >= 0 && jbuf >= 0 && ibuf < ySize - 1 && jbuf < xSize - 1) {
		dataOut[indx + 0] = (int) ((1 - ifrac) * (1 - jfrac) * (float)dataIn[indxx + 0]
		+ (1 - ifrac) * jfrac * (float)dataIn[indxx + 0 + 3] 
		+ ifrac * (1 - jfrac) * (float)dataIn[indxx + 0 + 3 * xSize]
		+ ifrac * jfrac * (float)dataIn[indxx + 0 + 3 * xSize + 3]);

		dataOut[indx + 1] = (int) ((1 - ifrac) * (1 - jfrac) * (float)dataIn[indxx + 1]
		+ (1 - ifrac) * jfrac * (float)dataIn[indxx + 1 + 3] 
		+ ifrac * (1 - jfrac) * (float)dataIn[indxx + 1 + 3 * xSize]
		+ ifrac * jfrac * (float)dataIn[indxx + 1 + 3 * xSize + 3]);

		dataOut[indx + 2] = (int) ((1 - ifrac) * (1 - jfrac) * (float)dataIn[indxx + 2]
		+ (1 - ifrac) * jfrac * (float)dataIn[indxx + 2 + 3] 
		+ ifrac * (1 - jfrac) * (float)dataIn[indxx + 2 + 3 * xSize]
		+ ifrac * jfrac * (float)dataIn[indxx + 2 + 3 * xSize + 3]);

	}
	else {
		dataOut[indx + 0] = 0;
		dataOut[indx + 1] = 0;
		dataOut[indx + 2] = 255;
	}

}




/************	Square	*************/
float Square (float x)
{
	return (x * x);
}




//	LoadPGM
//	load greyscale image (P5)

unsigned char* LoadPGM(const char* filename, int xSize, int ySize)
{
	FILE* fid = fopen(filename, "rb");
	if (fid == NULL)
		throw string("LoadPGM() failed open file ") + filename;

	fseek (fid, 5, SEEK_SET);

	int xFile, yFile;
	fscanf (fid, "%d", &xFile);
	fscanf (fid, "%d", &yFile);
	if (xFile != xSize || yFile != ySize)
	{
		fclose(fid);
		throw string("LoadPGM() image wrong size in ") + filename;
	}

	int depth;
	fscanf (fid, "%d", &depth);
	if (depth != 255)
	{
		fclose(fid);
		throw string("LoadPGM() image wrong depth in ") + filename;
	}

	//	get \n
	fgetc(fid);

	unsigned char* buf = newBytes(xSize * ySize);

	//fseek (fid, 16, SEEK_SET);
	int nbCharsRead = fread (buf, 1, xSize * ySize, fid);
	if (nbCharsRead != (xSize * ySize))
	{
		fclose(fid);
		throw string("LoadPGM() file wrong size in ") + filename;
	}

	fclose(fid);

	return buf;
}



//	SavePGM
//	save greyscale image (P5)

void SavePGM (const char *filename, unsigned char *data, int dimX, int dimY)
{
	FILE *fid;
	//	int P,y; 	//	,x,z, ZSize = 1

	if (data == NULL)
		throw string("SavePGM() data was NULL ") + filename;

	fid = fopen(filename, "wb");

	if(fid == NULL)
		throw string("SavePGM() failed open file ") + filename;

	fprintf (fid, "P5\n");
	fprintf (fid, "#.\n");
	fprintf (fid, "%d %d\n255\n", dimX, dimY);

	fwrite (data, 1, dimX * dimY, fid);

	fclose (fid);
}




//	SavePGMC
//	save colour image (P6)

int SavePGMC (const char *Name, unsigned char *data, int dimX, int dimY)
{
FILE *fid;
//	int P,y; 	//	,x,z, ZSize = 1

if (data == NULL) 
return(-1);

fid = fopen(Name,"w");

if(fid == NULL) {
fprintf (stderr,"SavePGM: Unable to open '%s'\n",Name);
return (-1);
}

fprintf (fid, "P6\n");
//	fprintf (fid, "#.\n");
fprintf (fid, "%d %d\n255\n", dimX, dimY);


fwrite (data, 1, dimX * dimY * 3, fid);

fclose (fid);

return (0);
}
/*
*/

