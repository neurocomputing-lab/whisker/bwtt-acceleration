
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function contourLUT = createContourLUT(nextChunkData, snoutTrackingData)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% function measurements = collect1dMeasurements(iCurveSamplingVector,
%whiskerTreeLUTwithOrientations, nodesListWithHistAndMarker)
%
% Inputs:
%   - whiskerTreeLUTwithOrientations is a matrix, where the data are
%           [inCurve inMarker ::::: xofmarker, yofmarker, c, theta,
%           rho,angle, roundness, saliency]
% Outputs
%   - contourLUT are the point of the contur data with
%   whiskerTreeLUTwithOrientations data format
%
% TWO METHODS:
%   - given nextChunkData only uses the snout contur, given instead the 



if nargin < 2
    pp = nextChunkData.pp;

    X = nextChunkData.X;
else
    pts = flipud(snoutTrackingData.contour);
    pp = cscvn(pts);
    X = fliplr(snoutTrackingData.center);
end

pts = fnval(pp, linspace(pp.breaks(1), pp.breaks(end), 500));

[theta, rho] = cart2pol(pts(2,:)-X(2), pts(1,:)-X(1));
contourLUT = zeros(numel(rho),10);
contourLUT(:,1) = 0;    % in Curve
contourLUT(:,2) = 1:numel(rho); % inMarker
contourLUT(:,3) = pts(2,:)'; % xofmarker
contourLUT(:,4) = pts(1,:)'; % yofmarker
contourLUT(:,5) = 1;
contourLUT(:,6) = theta';
contourLUT(:,7) = rho';

    
