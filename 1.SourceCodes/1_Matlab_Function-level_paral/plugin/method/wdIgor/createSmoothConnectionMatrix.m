
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function smoothConnectivityMatrix = createSmoothConnectionMatrix(whiskerTreeLUTwithOrientations, nodesListWithHistAndMarker, figureOn, GRAYshape)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Input:
%
%
% Output:
%           - connectionNetork is a matrix of

smoothConnectivityMatrix = zeros(1000,5);
sIndex = 0;

[strokeIdx, measurementsLimits] = getStrokeIndexesFromNodesListWithHistAndMarker(whiskerTreeLUTwithOrientations, nodesListWithHistAndMarker);


completeWhiskerTreeLUTWithOrientations = [whiskerTreeLUTwithOrientations, strokeIdx];

filteredWhiskerTreeLUT = completeWhiskerTreeLUTWithOrientations(:,:);


t = completeWhiskerTreeLUTWithOrientations(:,6);
r = completeWhiskerTreeLUTWithOrientations(:,7);
%% 2a. plot whiskers
if figureOn
    figure(figureOn)
    hold off
    imshow(GRAYshape);
    hold on
    plot(filteredWhiskerTreeLUT(:,4), filteredWhiskerTreeLUT(:,3),'ro')
    hold on
    axis ij
end

%% 3. scan over strokes and detect tangents
order = 1;
N = 5;
C = N-1;
Nfwidx = 0:C;
Nbwidx = -C:0;
Delta = 3;

tangentEstimates = nan(size(filteredWhiskerTreeLUT(:,1)));
s = unique(filteredWhiskerTreeLUT(:,end));
for j=2:numel(s),
    strokeId = s(j);
    idx = filteredWhiskerTreeLUT(:,end) == strokeId;
    rStroke = r(idx);
    yStroke = filteredWhiskerTreeLUT(idx, 3);
    xStroke = filteredWhiskerTreeLUT(idx, 4);

    t= 0;

    if numel(xStroke) > 2*N-1,
        for i=1:numel(xStroke),
            if i < round(numel(xStroke)/2),
                py = polyfit(rStroke(i+Nfwidx), yStroke(i+Nfwidx), order);
                px = polyfit(rStroke(i+Nfwidx), xStroke(i+Nfwidx), order);
            else
                py = polyfit(rStroke(i+Nbwidx), yStroke(i+Nbwidx), order);
                px = polyfit(rStroke(i+Nbwidx), xStroke(i+Nbwidx), order);
            end
            %plot(polyval(px,rStroke(i)+[0:Delta]), polyval(py,rStroke(i)+[0:Delta]),'b');
            t(i) = atan2(py(1),px(1));
            if figureOn
            plot(polyval(px,rStroke(i))+[0:Delta]*cos(t(i)), polyval(py,rStroke(i))+[0:Delta]*sin(t(i)),'b')
            end
        end

    elseif numel(xStroke) > N,
        for i=1:numel(xStroke),
            if i < numel(xStroke)-C,
                py = polyfit(rStroke(i+Nfwidx), yStroke(i+Nfwidx), order);
                px = polyfit(rStroke(i+Nfwidx), xStroke(i+Nfwidx), order);
            elseif i >= N,
                py = polyfit(rStroke(i+Nbwidx), yStroke(i+Nbwidx), order);
                px = polyfit(rStroke(i+Nbwidx), xStroke(i+Nbwidx), order);
            else
                py = polyfit(rStroke(numel(xStroke)-C+Nfwidx), yStroke(numel(xStroke)-C+Nfwidx), order);
                px = polyfit(rStroke(numel(xStroke)-C+Nfwidx), xStroke(numel(xStroke)-C+Nfwidx), order);
            end
            %plot(polyval(px,rStroke(i)+[0:Delta]), polyval(py,rStroke(i)+[0:Delta]),'b');
            t(i) = atan2(py(1),px(1));
            if figureOn
            plot(polyval(px,rStroke(i))+[0:Delta]*cos(t(i)), polyval(py,rStroke(i))+[0:Delta]*sin(t(i)),'b')
            end
        end

    else
        for i=1:numel(xStroke),
            py = polyfit(rStroke, yStroke, order);
            px = polyfit(rStroke, xStroke, order);
            %plot(polyval(px,rStroke(i)+[0:Delta]), polyval(py,rStroke(i)+[0:Delta]),'b');
            t(i) = atan2(py(1),px(1));
            if figureOn
            plot(polyval(px,rStroke(i))+[0:Delta]*cos(t(i)), polyval(py,rStroke(i))+[0:Delta]*sin(t(i)),'b')
            end
        end
    end
    tangentEstimates(idx) = t;
end


%% 4. calculate inter-element distance
ptsax = filteredWhiskerTreeLUT(:,4)';
ptsay = filteredWhiskerTreeLUT(:,3)';
%
Sa = numel(ptsax);
xDiff = repmat(ptsax,[Sa 1]) - repmat(ptsax', [1 Sa]);
yDiff = repmat(ptsay,[Sa 1]) - repmat(ptsay', [1 Sa]);
% avoid sqrt for faster min searching
[thetaMatrix, rhoMatrix] = cart2pol(xDiff, yDiff);

%% 5. for every point in the stroke apply tensor field
distT = 20;
%%
for i=1:size(filteredWhiskerTreeLUT,1),
    if ~isnan(tangentEstimates(i)),
        % get elements in neighbourhood
        idx = rhoMatrix(i,:) < distT & rhoMatrix(i,:) > 0;
        localFilteredWhiskerTreeLUT = filteredWhiskerTreeLUT(idx,:);
        % vote only if there are elements outside the stroke
        pointStroke = filteredWhiskerTreeLUT(i, end);
        localStroke = localFilteredWhiskerTreeLUT(:,end);
        if sum(pointStroke - localStroke),
            %plot(filteredWhiskerTreeLUT(:,4), filteredWhiskerTreeLUT(:,3),'ro');
            %plot(localFilteredWhiskerTreeLUT(:,4), localFilteredWhiskerTreeLUT(:,3),'go');
            pointTangent = tangentEstimates(i);
            localTheta = thetaMatrix(i,idx);
            localDistances = rhoMatrix(i,idx);
            localTangent = tangentEstimates(idx);
            [outIdx, fieldValue] = getBestTensorVotingEstimates(pointTangent, pointStroke, localStroke, localTheta, localDistances, localTangent);
            for j=1:6,
                if ~isnan(outIdx(j))
                    inCurve = filteredWhiskerTreeLUT(i,1);
                    inMarker = filteredWhiskerTreeLUT(i,2);
                    outCurve = localFilteredWhiskerTreeLUT(outIdx(j),1);
                    outMarker = localFilteredWhiskerTreeLUT(outIdx(j),2);
                    sIndex = sIndex + 1;
                    smoothConnectivityMatrix(sIndex,:) = [inCurve inMarker outCurve outMarker 1];
                    sIndex = sIndex + 1;
                    smoothConnectivityMatrix(sIndex,:) = [outCurve outMarker inCurve inMarker  1];
                end
            end
            if figureOn
                plotVal = {'m','y','k','g','y','k'};
                for j=1:6,
                    if ~isnan(outIdx(j))
                        plot([filteredWhiskerTreeLUT(i,4), localFilteredWhiskerTreeLUT(outIdx(j),4)], [filteredWhiskerTreeLUT(i,3), ...
                            localFilteredWhiskerTreeLUT(outIdx(j),3)],...
                            plotVal{j},'linewidth',2);
                        plot(filteredWhiskerTreeLUT(i,4), filteredWhiskerTreeLUT(i,3),'gd');
                    end
                end
            end
        end
    end
end



smoothConnectivityMatrix = smoothConnectivityMatrix(1:sIndex,:);
