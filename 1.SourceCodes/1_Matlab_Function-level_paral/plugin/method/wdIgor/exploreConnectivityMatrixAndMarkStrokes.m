
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [strokesData, visitedNodes, nodesListWithHistAndMarker] = exploreConnectivityMatrixAndMarkStrokes(inCurve, inMarker, whiskerId, connectivityMatrix, whiskerTree, nodesListWithHistAndMarker,visitedNodes)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Inputs:   - inCurve, inMarker are the starting coordinates
%           - onnectivityMatrix [curveNin nodeNumberIdIn curveNout
%           nodeNumberIdOut distance]
%           - whiskerTree is a cell array'ORIENTEDRADIALDISTANCE' then the
%           output is [x,y,c,theta, rho]
%           - nodesListWithHistAndMarker is a nx4 matrix, with [inNode, outNode,
%           hist and marker point]
%           - visitedNodes is a nx1 array with flags set to 1 when visited
%
%
% Outputs:  - visitedNodes is a nx1 array with flags set to one
%           - strokesData are the stroke [inCurve,inMarker, outCurve,
%           outMarker],
%           - nodesListWithHistAndMarker
%
%           NOTE in order to avoid problems with the inStrokes, when working
%           on the connectivity map, we define
%           the inStroke as the inElementTable and the out stroke the last
%           element with double or single hist
%           NOTE: if the startpoint is already marked, it means that is and
%           endpoint, therefore we don't provide connectivity and stroke
%           data is zero.
%           NOTE2: if we have just a single node with hist=2, we set
%           strokeData to zero and we don't mark it with whiskerId
%<\outDOC>


inElementsTable = [inCurve, inMarker];
strokesData = zeros(1,4);
endStroke = zeros(1,2);


idxOFStartNode = ((nodesListWithHistAndMarker(:,1) == inElementsTable(1,1)) &...
    (nodesListWithHistAndMarker(:,2) == inElementsTable(1,2)));
isMarkedTheStartNode = nodesListWithHistAndMarker(idxOFStartNode,4);

if ~isMarkedTheStartNode
    strokesData(1:2) = nodesListWithHistAndMarker(idxOFStartNode,1:2);
    visitedNodes(idxOFStartNode) = 1;   % WE MARK !ST NODE AS VISITED
    [inElementsTable, nodesListWithHistAndMarker, visitedNodes, endStroke] = iterateWhiskerSearch...
        (inElementsTable, connectivityMatrix, visitedNodes, nodesListWithHistAndMarker, whiskerTree, whiskerId, endStroke);

    if endStroke(1)
        % mark only if we have a valid endstroke, othervise we got a single
        % element, so we have inCurve and outCurve is zero
        if ~isequal(strokesData(1:2),endStroke),
            nodesListWithHistAndMarker(idxOFStartNode,4) = whiskerId;
            strokesData(3:4) = endStroke;
        else
            % set to zero
            strokesData = zeros(1,4);
        end
    else
        % set to zero
        strokesData = zeros(1,4);
    end
end




%% iterate search function depth first search

function [inElementsTable, nodesListWithHistAndMarker, visitedNodes, endStroke] = iterateWhiskerSearch...
    (inElementsTable, connectivityMatrix, visitedNodes, nodesListWithHistAndMarker, whiskerTree, whiskerId, endStroke)


idxOfConnectedEdges = ((connectivityMatrix(:,3) == inElementsTable(1,1)) & (connectivityMatrix(:,4) == inElementsTable(1,2)));
connectedNodes= connectivityMatrix(idxOfConnectedEdges,[1 2]);
rhoStart = whiskerTree{inElementsTable(1,1)}(inElementsTable(1,2),5);
for i=1:size(connectedNodes,1),
    %<\inDOC
    %         figure(1)
    %         hold on
    %         outCurve = inElementsTable(1,1);
    %         outMarker = inElementsTable(1,2);
    %         inCurve = connectedNodes(i,1);
    %         inMarker =connectedNodes(i,2);
    %         x1 = whiskerTree{inCurve}(inMarker,1);
    %         y1 = whiskerTree{inCurve}(inMarker,2);
    %         x2 = whiskerTree{outCurve}(outMarker,1);
    %         y2 = whiskerTree{outCurve}(outMarker,2);
    %         line([y1 y2],[x1 x2],'Color','c')
    %         plot(y1,x1,'cd');
    %         plot(y2,x2,'c+');
    %         [x1 y1 x2 y2 inCurve inMarker outCurve outMarker]
    %<\outDOC>
    idxOFConnectedNode = ((nodesListWithHistAndMarker(:,1) == connectedNodes(i,1)) &...
        (nodesListWithHistAndMarker(:,2) == connectedNodes(i,2)));
    flagIsVisited = visitedNodes(idxOFConnectedNode);
    if ~flagIsVisited,
        % if still unvisitedNode, check its hist
        histOfThisNode = nodesListWithHistAndMarker(idxOFConnectedNode, 3);
        if histOfThisNode <= 2
            rhoPoint = whiskerTree{connectedNodes(i,1)}(connectedNodes(i,2),5);
            % add it to the nodes to visit table
            if (rhoPoint >= rhoStart)
                inElementsTable = [inElementsTable; connectedNodes(i,1:2)];
                % and mark it
                nodesListWithHistAndMarker(idxOFConnectedNode,4) = whiskerId;
                % save it as the last visited node
                endStroke = connectedNodes(i,1:2);
                visitedNodes(idxOFConnectedNode) = 1;
            end
        end
        % don't visit the node if it has the strange hist
    end
end
% extract the first element from the que
inElementsTable = inElementsTable(2:end,:);

if ~isempty(inElementsTable)
    [inElementsTable, nodesListWithHistAndMarker, visitedNodes, endStroke] = iterateWhiskerSearch...
        (inElementsTable, connectivityMatrix, visitedNodes, nodesListWithHistAndMarker, whiskerTree, whiskerId, endStroke);
end





