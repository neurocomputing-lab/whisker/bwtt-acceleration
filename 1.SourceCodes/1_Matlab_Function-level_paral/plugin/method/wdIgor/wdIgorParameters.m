
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


% function [pars, prevChunkData, chunkFrame, jumpFrame, flagGetDataFrom] = func(videoFilename, initialFrameToProcess)

function pars = wdIgorParameters()



% wdIgorInitialize.m
% syntax:
%   [wdParameters, wdPrevChunkData, wdChunkFrame, wdJumpFrame,
%               flagGetDataFromWT] = wdPerkonInitialize(filename, frameNum)
%
% loads parameters by gui or xml file and allocates algorithm parameters in
% wdParam and iterative algorithm data in previousChunkData
%
% Inputs:
%           - filename is the complete pathname of the movie film ending in
%           *.avi. If the user needs to load a configuration file, he will
%           take care of parsing the filename and modifying its parts in
%           order to get automatically the configuration file
%           - frameNumber is the startFrame given to the initialize
%           function by the general manager. It is used for loading the
%           original movie, makig a custom initialization GUI, loading chunks
%           off data etc..
%
% Outputs:
%           - wdParam is a data structure containing whisker detection
%           algorithm parameters.
%               Constant parameters:
%                       Constant{1} - number of elements considered for
%                       peak detetction
%                       Constant{2} - tensor voting threshold, touch only
%                       in case of emergency
%                       Constant{3} - not assigned
%                       Constant{4} - sigma, touch only if you are not
%                       satisfied with detetcted whiskers
%                       Constant{5} - theta range, used for filtering
%                       values of left/right whiskers
%                       Constant{6} - filtering scalar product in order to
%                       eliminate measurment that are less than
%                       Constant{7}.value
%                       Constant{7} - selects whiskers close to detected
%                       snout contour
%
%               Variable parameters:
%                       Variable{1} - you can play using differrent
%                       intermediate outputs of processing as input to the
%                       following stages
%                       Variable{2} - sampling step for finding points in
%                       polar coordinates - try first larger values in
%                       order to reduce the ammount of data to be processed
%                       Variable{3} - stick voting activation flag - set to
%                       on only if you have poor data
%                       Variable{4} - stick voting threshold is used for
%                       selecting output of stick voting - if you want to use
%                       this features set Variable{1} to 4
%                       Variable{5} - show debug steps, if 0 no debug
%                       figures, if 1 show debug figures
%                       Variable{6} - Variable{7} are thresholds used to
%                       select whisker peaks in the internal or external
%                       part of the ring in polar coordinates defined by
%                       Variable{8}
%           %
%           - previousChunkData is empty
%           - frameChunk is an integer data describing the number of frames
%           the general manager needs to load and feed to snout detection
%           algorithm
%           - frameJump is an integer data describing how many frames are
%           between one chunk and the other
%           - flagGetDataFromWT is a boolean variable. If true the general
%           manager merges previousFrame data from whisker tracking with the
%           previous frame data of the current algorithm
%
%
% NOTE: uint8 casting might in future be removed for 12 or 16 bit sensors
%
%*************************************************************************
%  Author: Igor Perkon    -   perkon@sissa.it




pars = {};


%% selected image
%original image - 'choice 1'
%q - 'choice 4'
%roundness - 'choice 3'
%saliency - 'choice 2'
par = [];
par.name = 'inputImage';
par.type = 'scalar';
par.help = 'Selects which internal filtering is used: 1 - original image (default), 2 - tensor voting (TV) image, 3 - roundness from TV, 4 - saliency from TV';
par.range = [1 4];
par.value = 1;
par.step = [1 1];
pars{end+1} = par;

%% sampling Step in create whisker tree
par = [];
par.name = 'samplingStep';
par.help = 'Sets the distance (in pixels) between consecutive head-centered circles where intensity peaks are sampled';
par.type = 'scalar';
par.range = [1 30];
par.value = 5;
par.step = [1 3];
pars{end+1} = par;

%% stick voting flag
par = [];
par.name = 'stickVotingActivationFlag';
par.help = 'Use the slowest stick voting procedure to enchance the lnear features';
par.value = 0;
par.type = 'flag';
pars{end+1} = par;

%% stick voting threshold - valid only if stickVotingActivationFlag is true
par = [];
par.name = 'stickVotingThreshold';
par.help = 'Sets the discrimation level when stick voting is active';
par.type = 'scalar';
par.range = [0 1];
par.value = 0.1;
par.step = [0.01 0.2];
par.precision = 3;
par.show = 'pars.stickVotingActivationFlag';
pars{end+1} = par;

%% histogrm equalization of difference image
par = [];
par.name = 'showDebug';
par.help = 'Show the detected intensity peaks: you should see red crosses only where whiskers are located. Use internalRingT and externalRingT to adjust';
par.value = 0;
par.type = 'flag';
pars{end+1} = par;

%% internal threshold for peak detection
par = [];
par.name = 'internalRingT';
par.type = 'scalar';
par.help = 'Only intensity peaks over this threshold are detected. Usually set higher than externalRingT to reduce the impact of common fur and short thin whiskers';
par.range = [1 128];
par.value = 50;
par.step = [1 10];
pars{end+1} = par;

%% internal threshold for peak detection
par = [];
par.name = 'externalRingT';
par.type = 'scalar';
par.help = 'Only intensity peaks over this threshold are detected. If too high no whisker tips, while if too low all the movie noise is detected';
par.range = [1 128];
par.value = 30;
par.step = [1 10];
pars{end+1} = par;

%% rho thresh - defines the rho boundary between internal and external ring
par = [];
par.name = 'boundaryInternalExternal';
par.label = 'radialRingPosition';
par.help = 'Radial distance from the head center creates two areas. Inside only intensity peaks over internalRingT are detected, externalRingT regulates outside';
par.type = 'scalar';
par.range = [1 400];
par.value = 100;
par.step = [1 10];
pars{end+1} = par;



%% CONSTANTS

%% local tokens (4 to 15)
par = [];
par.name = 'localTokens';
par.value = 6;
pars{end+1} = par;
	
%% bw binary threshold (0 to 1)
par = [];
par.name = 'tensorVotingThreshold';
par.value = 0.01;
pars{end+1} = par;
	
%% bw binary threshold (1 to 20)
par = [];
par.name = 'sigma';
par.value = 2;
pars{end+1} = par;

%% theta range selects the limits of whisker tree selection and speeds up
par = [];
par.name = 'thetaRange';
par.value = [-2*pi, 2*pi];
pars{end+1} = par;

%% scalarT for filtering spurious measurements
par = [];
par.name = 'scalarProductT';
par.value = 0.9;
pars{end+1} = par;

%% scalarT for filtering spurious measurements
par = [];
par.name = 'rhoT';
par.value = 20;
pars{end+1} = par;





