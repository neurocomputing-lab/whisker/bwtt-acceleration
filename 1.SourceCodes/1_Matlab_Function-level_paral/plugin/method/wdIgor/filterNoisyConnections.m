
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [connectivityMatrix2, deletedEdgeList] = filterNoisyConnections(connectivityMatrix, whiskerTree, whiskerTreeLUTwithOrientations, smallAngleT)

% Author: Perkon Igor    -    perkon@sissa.it

% <\inDOC>
% Function filterNoisyConnections filter connections that are too
%
% Inputs: connectivityMatrix
%           - connectivityMatrix [curveNin nodeNumberIdIn curveNout
%           nodeNumberIdOut distance]
%           - whiskerTree
%           -whiskerTreeLUTwithOrientations is a matrix, where the data are
%           [inCurve inMarker ::::: xofmarker, yofmarker, c, theta,
%           rho,angle, roundness, saliency]
%
% Outputs:
%           - connectivityMatrix is the updated connectivity matrix
%           - deletedEdgeList is the list of deleted edges with
%           [inCuve inMarker outCurve outMarker] format
%           
%
% For each point with more than one and less than 4 connections, the function sorts
% connections according thier angles. If the angle difference between two
% connection is less then DELTAdegree, we extract the coordinates of
% connected points and we connect to the closest element deleting the
% connections.
%
% MODIFIED on 15.05.2009 for any number of edges
%          on 20.05.2009 for using angle information in weighting distance
%          in 05.06.2009 adding the single end point detection with deleted
%          edges detection
%<\outDOC>




deletedEdgeList = zeros(10000,4);
deletedEdgeIndex = 0;


oldSize = inf;
while(size(connectivityMatrix,1) ~= oldSize)
    i=1;
    while i <= size(connectivityMatrix,1),

        edgeMatrix = connectivityMatrix(i,:);
        %if isequal(edgeMatrix(1:2),[7 7])
        %    pause(0.1)
        %end

        idx = (connectivityMatrix(:,1) == edgeMatrix(1)) & ...
            (connectivityMatrix(:,2) == edgeMatrix(2));
        numberOfEdges = sum(idx);
        if ~numberOfEdges,
            numberOfEdges = 1;
        end
        if numberOfEdges > 1 %&& numberOfEdges < 4,
            edgeMatrix = connectivityMatrix(idx,:);
            %x1 = whiskerTree{edgeMatrix(1,1)}(edgeMatrix(1,2),[1 2]);
            idx = (whiskerTreeLUTwithOrientations(:,1) == edgeMatrix(1,1)) & ...
                (whiskerTreeLUTwithOrientations(:,2) == edgeMatrix(1,2));
            x1 = whiskerTreeLUTwithOrientations(idx,[3 4]);
            localAngle = whiskerTreeLUTwithOrientations(idx, 8);
%             if isequal(x1,[196 252.5 ])
%                 pause(0.1)
%             end
            % we initialize to numberOfEdges
            y1 = zeros(numberOfEdges,2);
            refAngle = zeros(1,numberOfEdges);
            for j=1:numberOfEdges,
                y1(j,:) = whiskerTree{edgeMatrix(j,3)}(edgeMatrix(j,4),[1 2]);
                refAngle(j) = 180/pi*atan2([x1(2)-y1(j,2)],[x1(1)-y1(j,1)]);
            end

            % sort angles
            [sortedAngle, idx] = sort(refAngle);
            sortedY1 = y1(idx,:);
            sortedDistances = (x1(1)-sortedY1(:,1)).^2 + (x1(2)-sortedY1(:,2)).^2;
            sortedEdges = edgeMatrix(idx,:);
            realAngles = abs(sortedAngle - localAngle);
            realAngles(realAngles>180) = realAngles(realAngles>180)-180;
            realAngles(realAngles > 90) = 180 - realAngles(realAngles > 90);
            weightedDistances = sortedDistances.*((realAngles+1)'/180).^0.5;
            for j=1:numberOfEdges,
                if j< numberOfEdges,
                    flagDetectedTriangle = (sortedAngle(j+1) - sortedAngle(j)) < smallAngleT ||...
                        (sortedAngle(j+1) - sortedAngle(j)) > (360-smallAngleT);
                    if flagDetectedTriangle
                        % take the closest point
                        [val, pos] = min(weightedDistances(j:j+1));
                        if pos == 1,
                            edgeToDelete = sortedEdges(j+1,:);
                        else
                            edgeToDelete = sortedEdges(j,:);
                        end
                    end
                else
                    % modify index
                    flagDetectedTriangle =(sortedAngle(j) - sortedAngle(1)) < smallAngleT ||...
                        (sortedAngle(j) - sortedAngle(1)) > (360-smallAngleT);
                    if flagDetectedTriangle
                        % take the closest point
                        [val, pos] = min(weightedDistances([j,1]));
                        if pos == 1,
                            edgeToDelete = sortedEdges(1,:);
                        else
                            edgeToDelete = sortedEdges(j,:);
                        end
                    end
                end


                if flagDetectedTriangle
                    % put to zero connectivityMatrix edgetoDelete and its
                    % opposite

                    idx = (connectivityMatrix(:,1) == edgeToDelete(1)) & ...
                        (connectivityMatrix(:,2) == edgeToDelete(2)) &...
                        (connectivityMatrix(:,3) == edgeToDelete(3)) & ...
                        (connectivityMatrix(:,4) == edgeToDelete(4));
                    if sum(idx)
                        connectivityMatrix(idx,:) = nan(1,5);
                        deletedEdgeIndex = deletedEdgeIndex+1;
                        deletedEdgeList(deletedEdgeIndex,:) = edgeToDelete(1:4);
                    end
                    % opposite connection 
                    idx = (connectivityMatrix(:,1) == edgeToDelete(3)) & ...
                        (connectivityMatrix(:,2) == edgeToDelete(4)) &...
                        (connectivityMatrix(:,3) == edgeToDelete(1)) & ...
                        (connectivityMatrix(:,4) == edgeToDelete(2));
                    if sum(idx)
                        connectivityMatrix(idx,:) = nan(1,5);
                        deletedEdgeIndex = deletedEdgeIndex+1;
                        deletedEdgeList(deletedEdgeIndex,:) = edgeToDelete(1:4);
                    end
                end
            end

        end




        i = i + numberOfEdges;
    end

    oldSize = size(connectivityMatrix,1);
    connectivityMatrix =  connectivityMatrix(~isnan(connectivityMatrix(:,1)),:);
end


deletedEdgeList = deletedEdgeList(1:deletedEdgeIndex,:);
connectivityMatrix2 = [connectivityMatrix;connectivityMatrix([3 4 1 2 5],:)];


