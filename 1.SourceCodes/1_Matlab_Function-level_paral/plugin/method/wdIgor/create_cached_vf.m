
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

function [ out ] = create_cached_vf( sigma )
	ws = floor( ceil(sqrt(-log(0.01)*sigma^2)*2) / 2 )*2 + 1;  
    out = zeros(180,ws,ws,2,2);
    for i=1:180
        x = cos(pi/180*i);
        y = sin(pi/180*i);
        v = [x,y];
        Fk = create_stick_tensorfield(v,sigma);
        out(i,:,:,:,:) = Fk;
    end

end