
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Igor Perkon.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====




function [pars, prevChunkData, chunkFrame, jumpFrame, flagGetDataFrom] = func(videoFilename, initialFrameToProcess)



% syntax:
%       [sdparam, previousChunkData, frameChunk, frameJump]  =
%       sdMoritzsMazeInitialize(fullFilename)
%
% Inputs:
%           - fullFilename is an optional input indicating the configuration
%           file name
% Outputs:
%           - sdparam is a struct with the following parameters
%           *) sdparam.bwThreshold selects the inital threshold for whisker
%           detection
%           *) sdparam.bwMorphology is the threshold for setting the importance of
%           detected features, usually 5/255
%           *) sdparam.razorSize measures the width of detected snout mask
%           - previousChunkData are the data required for the detecor to be
%           initalized
%           - flagGetDataFromST flag is true if the detection method
%           requires data from snout tracker
%           - frameChunk is the number of frames required by the method to
%           get data from the general manager
%           - frameJump is the number of frames the general manager needs
%           to increment in order to derive new data
%*************************************************************************
%  Author: Perkon Igor    -    perkon@sissa.it





	
	path(path,'.\sdMoritzsMaze');
	
	% call parameter GUI with sliders and default values or read xml
	if nargin == 0,
		dirname = '.\*.sdc';
		[filename, pathname] = uigetfile(dirname, 'Pick a sdc file')
		fullFilename = fullfile(pathname, filename);
	end
	[bwThreshold, bwMorphology, razorSize] = textread(fullFilename);
	%bwThreshold = 0.1;
	%bwMorphology = 5/255;
	%razorSize = 10;
	
	sdparam = struct('bwThreshold', bwThreshold, 'bwMorphology', bwMorphology, 'razorSize', razorSize);
	
	pars = sdparam;
	




prevChunkData = [];
chunkFrame = 1;
jumpFrame = 1;
flagGetDataFrom = 0;














