
%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====

% CHANGELOG:
%
% 19/11/14: update to fix for Matlab 2014.


classdef stVideoSlidersGUI < handle
	
	properties
		
		h_axis
		h_avail
		h_range
		h_initial
		h_cur
		
	end
	
	properties (Constant = true)
		
		DISPLAY_HEIGHT = 4
		
	end
	
	methods

		function gui = stVideoSlidersGUI(h_axis)
			
			gui.h_axis = h_axis;

			% create parts
			cla(h_axis);

% 19/11/14
%
% OLD:
%			gui.h_avail = image(NaN, NaN, repmat(1, [2 2 3]), 'parent', h_axis);
%
% NEW:
			gui.h_avail = image( ...
				[1 1], ...
				[0 stVideoSlidersGUI.DISPLAY_HEIGHT], ...
				repmat(1, [2 2 3]), 'parent', h_axis);
% WHY:
% Old syntax no longer valid, requires valid ranges for arguments 1
% and 2. New arguments should be fine because both are updated (I
% assume) immediately on a call to updateFrameRange().
%
% 19/11/14

			hold(h_axis, 'on');
			gui.h_range = rectangle('parent', h_axis, ...
				'position', [0 -3 1 1], 'linestyle', 'none', 'FaceColor', [0.5 1 0.5]);
			gui.h_initial = plot(h_axis, NaN * [-1 -1], [1 3], 'r-', 'linewidth', 2);
			gui.h_cur = plot(h_axis, NaN * [-1 -1], [0 4], 'b-', 'linewidth', 1);

			% finalise axis
			set(h_axis, 'xtick', [], 'ytick', []);
			axis(h_axis, [-2 -1 -2 -1])
			
		end
		
		function updateFrameRange(gui, frameRange)

			set(gui.h_initial, 'xdata', frameRange.initial*[1 1]);
			w = frameRange.last - frameRange.first;
			if w
				set(gui.h_range, 'position', [frameRange.first 1 w 2]);
			else
				set(gui.h_range, 'position', [-100 1 1 1]);
			end

			% set axis limits
			axis(gui.h_axis, [0.5+[0 frameRange.available] [0 stVideoSlidersGUI.DISPLAY_HEIGHT]]);
			
			% update frames available
			set(gui.h_avail, ...
				'xdata', [1 frameRange.available], ...
				'ydata', [0 stVideoSlidersGUI.DISPLAY_HEIGHT]);
			
		end
		
		function updateCurrentFrame(gui, currentFrame)

			set(gui.h_cur, 'xdata', currentFrame * [1 1]);
			
		end

		function updateFramesAvailable(gui, framesAvailable)
		
			cdata = double(repmat(framesAvailable, [1 1 3]));
			cdata(:, :, 3) = 0;
			cdata = uint8(255 - cdata * 16);
			set(gui.h_avail, 'cdata', cdata);
			
		end
		
		function hh = getHandles(gui)
			
			hh = [gui.h_axis gui.h_avail gui.h_range gui.h_initial gui.h_cur];
			
		end
		
		function h = getAxis(gui)
			
			h = gui.h_axis;
			
		end
		
	end
	
end

