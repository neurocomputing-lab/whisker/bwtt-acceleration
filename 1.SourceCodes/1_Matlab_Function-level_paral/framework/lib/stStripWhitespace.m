
% s = stStripWhitespace(s)
%   if s is of the form "\tasd ", the whitespace is
%   removed and the bare string is returned.

%%%%====
%
%	This file is part of The BIOTACT Whisker Tracking Tool ("The BWTT").
%	Copyright (C) 2015 Ben Mitchinson.
%
%	The BWTT is free software: you can redistribute it and/or modify
%	it under the terms of the GNU General Public License as published by
%	the Free Software Foundation, either version 3 of the License, or
%	(at your option) any later version.
%
%	This program is distributed in the hope that it will be useful,
%	but WITHOUT ANY WARRANTY; without even the implied warranty of
%	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
%	GNU General Public License for more details.
%
%	You should have received a copy of the GNU General Public License
%	along with this program. If not, see <http://www.gnu.org/licenses/>.
%
%%%%====


function s = stStripWhitespace(s)

f = find(s ~= 9 & s ~= 32 & s~= 10 & s~= 13);

if isempty(f)
	s = '';
else
	s = s(min(f):max(f));
end

