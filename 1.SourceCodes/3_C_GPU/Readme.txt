This is a a GPU-accelerated version of C-based BWTT. 

The phase of the Polyfit in the algorithm was accelerated by introducing a cuda kernel parallelizing the process of Polyfit.