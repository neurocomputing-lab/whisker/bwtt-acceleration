#include "b.h"


extern "C" void polyfitCuda(unsigned char *lutFit,  int *sumCount, 
	int * iSource, int *iSink, int *lutX, 
	int *lutY, float * lutRho, unsigned int const pointCount, float const clusteringThreshold, 
	unsigned int const numSources, unsigned int const numSinks, unsigned long const lutSize,
	unsigned long const sumSize);


__global__ void polyfitKernel(unsigned char * const lutFit,  int * const sumCount, 
	const int * const iSource, const int * const iSink, const int * const lutX, 
	const int * const lutY, const float * const lutRho, 
	unsigned int const pointCount, float const clusteringThreshold, unsigned int const numSources,
	unsigned int const numSinks)
{
	int indexSource = blockIdx.x * blockDim.x + threadIdx.x;;
	int indexSink = blockIdx.y * blockDim.y + threadIdx.y;
	if(indexSource >= numSources || indexSink >= numSinks){
		return;
	}

	int so = iSource[indexSource] - 1;
	int si = iSink[indexSink] - 1;

	double px = (lutX[si] - lutX[so]) / (lutRho[si] - lutRho[so]);
	double bx = lutX[so] - px*lutRho[so];

	double py = (lutY[si] - lutY[so]) / (lutRho[si] - lutRho[so]);
	double by = lutY[so] - py*lutRho[so];

	int base = indexSource + indexSink*numSources;
	int sum = 0;
	//double mean = 0;
	//double std = 0;

	for(int i = 0; i < pointCount; i++){
		double errorx = px*lutRho[i]+bx-lutX[i];
		double errory = py*lutRho[i]+by-lutY[i];
		double err = errorx*errorx + errory*errory;
		if(err<clusteringThreshold && lutRho[i]<lutRho[si] && lutRho[i]>lutRho[so]){
			lutFit[base*pointCount + i] = 1;
			//select[sum] = i;
			sum++;
			//mean += lutIns[i]; 
		}
	}

	sumCount[base] = sum;
	/*
	if(sum!=0){
		//mean = mean/sum;
	}

	for(int i = 0; i < sum; i++){
		lutFit[base*pointCount + select[i]] = sum;
		//std += pow(lutIns[select[i]] - mean, 2);
	}

	if(sum > 1){
		//std = sqrtf(std/(sum-1));
	}
	else
		//std = 0;

		*/
}



void polyfitCuda(unsigned char *lutFit,  int *sumCount, 
	int * iSource, int *iSink, int *lutX, 
	int *lutY, float * lutRho, unsigned int const pointCount, float const clusteringThreshold, 
	unsigned int const numSources, unsigned int const numSinks, unsigned long const lutSize,
	unsigned long const sumSize){

	
	const int BS = 16;
    	const dim3 blockSize(BS, BS);
	const dim3 gridSize((numSources - 1 / BS) + 1, (numSinks - 1 / BS) + 1); // Define Block Size



	int *d_iSource, *d_iSink, *d_sumCount;
	int *d_lutX, *d_lutY;
	float *d_lutRho;
	unsigned char *d_lutFit;

	unsigned long sizeSo = numSources * sizeof(int);
	unsigned long sizeSi = numSinks * sizeof(int);
	unsigned long sizeLutC = pointCount * sizeof(int);
	unsigned long sizeLutR = pointCount * sizeof(float);


	cudaMalloc((void **) &d_iSource, sizeSo);
	cudaMalloc((void **) &d_iSink, sizeSi);
	cudaMalloc((void **) &d_lutX, sizeLutC);
	cudaMalloc((void **) &d_lutY, sizeLutC);
	cudaMalloc((void **) &d_lutRho, sizeLutR);

	cudaMalloc((void **) &d_lutFit, lutSize);
	cudaMalloc((void **) &d_sumCount, sumSize);

	cudaMemcpy(d_iSource, iSource, sizeSo, cudaMemcpyHostToDevice);
	cudaMemcpy(d_iSink, iSink, sizeSi, cudaMemcpyHostToDevice);
	cudaMemcpy(d_lutX, lutX, sizeLutC, cudaMemcpyHostToDevice);
	cudaMemcpy(d_lutY, lutY, sizeLutC, cudaMemcpyHostToDevice);
	cudaMemcpy(d_lutRho, lutRho, sizeLutR, cudaMemcpyHostToDevice);

	polyfitKernel<<<gridSize, blockSize>>>(d_lutFit, d_sumCount, d_iSource, d_iSink, d_lutX, d_lutY, d_lutRho,
		pointCount, clusteringThreshold, numSources, numSinks);
	cudaDeviceSynchronize();

	cudaMemcpy(lutFit, d_lutFit, lutSize, cudaMemcpyDeviceToHost);
	cudaMemcpy(sumCount, d_sumCount, sumSize, cudaMemcpyDeviceToHost);

	cudaFree(d_iSource);
	cudaFree(d_iSink);
	cudaFree(d_lutX);
	cudaFree(d_lutY);
	cudaFree(d_lutRho);
	cudaFree(d_lutFit); 
	cudaFree(d_sumCount);  
	
}



