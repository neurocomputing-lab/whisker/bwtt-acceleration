/**
* @file wdIgorMeanAngle.h
* @Funtions for Extracting Whisker Segments
* @author Yang Ma, Erasmus MC
*/

/*
% This function attempts to find the snout contour using a pre-defined contour template
% Inputs:
%           -(snoutCenterX, snoutCenterY) is the user selected snout center
%           -(noseTipX, noseTipY) is the user selected nose tip
%           -sdContour is a vector of points extracted from previous section
% Outputs:
%           -snoutContour is the estiamted contour of snout
*/

#include <opencv/cv.h>
#include <opencv/highgui.h>
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "createWhiskerTree.h"


void wdIgorMeanAngleProcess(IplImage *greyFrame, IplImage *RingMask, int cols, int rows, int pixs,
	int snoutCenterX, int snoutCenterY, IplImage *distanceMask, FILE *file1);
void polyfit(uchar *lutFit, int * sumCount, int *iSource, int *iSink,
	int numSources, int numSinks);
void QuickSumSort(unsigned long left, unsigned long right, int *sumCount, unsigned long * finalIndex);

int sizeBytesInt = 24000;
int sizeBytesDouble = 48000;


void wdIgorMeanAngleProcess(IplImage *greyFrame, IplImage *RingMask, int cols, int rows, int pixs,
	int snoutCenterX, int snoutCenterY, IplImage *distanceMask, FILE *file1) {

	// Parameter Setting
	int peakT = 40;
	int samplingStep = 1;
	float thetaT = 1;
	float secThresh = thetaT;
	float bandWidth = 8;
	int nptsSharedPaths = 8; //cMassT

	// Get Wisker image in rings
	uchar *frameData;
	frameData = (uchar *)greyFrame->imageData;

	//FILE *file3 = fopen("filename", "w");

	uchar *RingMaskData;
	RingMaskData = (uchar *)RingMask->imageData;
	for (int i = 0; i < pixs; i++) {
		//fprintf(file3, "%d\n", frameData[i]);
		frameData[i] = frameData[i] * RingMaskData[i];
	}
	//fclose(file3);



	// Double the image size
	IplImage *duoImage = cvCreateImage(
		cvSize(2*cols, 2*rows), IPL_DEPTH_8U, 1);
	cvResize(greyFrame, duoImage, CV_INTER_CUBIC);
	snoutCenterX *= 2;
	snoutCenterY *= 2;

	// STEP1 --> set parameters and CREATE WHIKSER TREE
	float trBinary = 1/255.0f;
	if(peakT - 10 > 1){
		trBinary = (peakT - 10)/255.0f;
	}
	//float trBinary = max(peakT - 10, 1)/255.0f;
	int trInnerRing = peakT;
	int trOuterRing = 0;
	int rhoThresh = 1000;
	int potentialDensityArray = samplingStep;

	createWhiskerTree(duoImage, potentialDensityArray, snoutCenterX, snoutCenterY,
		trBinary, 2*cols, 2*rows);

	// STEP2 --> FIND SOURCES AND SINKS
	float *disData = (float *)distanceMask->imageData;
	int idxSinks[1000], idxSources[1000];
	int sinkCount = 0, sourceCount = 0;
	for (int i = 0; i < tokensIndex; i++) {
		float temp = disData[selectedWTree[i].X / 2 + selectedWTree[i].Y*cols / 2];
		if (temp < secThresh) {
			idxSinks[sinkCount++] = i;
		}
		if (temp > bandWidth - secThresh) {
			idxSources[sourceCount++] = i;
		}
	}

	// STEP3 --> POLYFIT
	int clusteringThreshold = 0;

	//uchar *lutFit;
	int *sumCount;
	unsigned long limit = (unsigned long)sinkCount * (unsigned long)sourceCount
		* (unsigned long)tokensIndex;
	unsigned long size1 = (unsigned long)sinkCount * (unsigned long)sourceCount *
		(unsigned long)sizeof(int);
	unsigned long sizelong = (unsigned long)sinkCount * (unsigned long)sourceCount *
			(unsigned long)sizeof(unsigned long);
	unsigned long size2 = limit * (unsigned long)sizeof(uchar);

	sumCount = (int*)malloc(size1);
	//lutFit = (uchar*)malloc(size2);

	//memory allocated using malloc
	if (sumCount == NULL)
	{
		printf("Error! memory not allocated.");
		exit(0);
	}




	uint64_t *xMax = malloc(sizeBytesDouble);
	uint64_t *yMax = malloc(sizeBytesDouble);
	double *rhoMax = malloc(sizeBytesDouble);

	for(int i = 0; i < tokensIndex; i++){
		xMax[i] = selectedWTree[i].X;
		yMax[i] = selectedWTree[i].Y;
		rhoMax[i] = selectedWTree[i].rho;
	}

	uint32_t *soMax = malloc(sizelong);
	uint32_t *siMax = malloc(sizelong);
	//uint32_t *selectToken = malloc(limit* (unsigned long)sizeof(uint32_t));
	uint16_t *lutFit = malloc(limit* (unsigned long)sizeof(uint16_t));

	for(int i = 0; i < sinkCount; i++){
		//siMax[i] = idxSinks[i];
		int tempbase = i*sourceCount;
		for(int j = 0 ; j < sourceCount; j++){
			soMax[tempbase + j] = idxSources[j];
			siMax[tempbase + j] = idxSinks[i];
			/*
			int base2 = (tempbase + j)*tokensIndex;
			for(int k = 0 ; k < tokensIndex; k++){
				selectToken[base2 + k] = k;
			}
			*/
		}
	}


	bwtt(sinkCount, sourceCount, tokensIndex, siMax, soMax,
			lutFit, rhoMax, xMax, yMax, rhoMax, xMax, yMax);

	FILE *ft;
	ft = fopen("test", "w");

	//polyfit(lutFit, sumCount, idxSources, idxSinks,
		//sourceCount, sinkCount);

	for(int i = 0; i < sinkCount; i++){
			//siMax[i] = idxSinks[i];
		int tempbase = i*sourceCount;
		for(int j = 0 ; j < sourceCount; j++){
			int tempCount = 0;
			int base2 = (tempbase + j)*tokensIndex;
				for(int k = 0 ; k < tokensIndex; k++){
					if(lutFit[base2 + k] == 1){
						tempCount++;
					}
			}

			sumCount[tempbase + j] = tempCount;
			//fprintf(ft, "%d\n", tempCount);
		}

	}

	fclose(ft);


	// STEP 4 --> Sort & Select
	unsigned long *finalIndex;
	finalIndex = (unsigned long*)malloc(sizelong);
	unsigned long totalOptions = (unsigned long)sinkCount * (unsigned long)sourceCount;
	for (unsigned long i = 0; i < totalOptions; i++) {
		finalIndex[i] = i;
	}


	QuickSumSort(0, totalOptions - 1, sumCount, finalIndex);


	unsigned long resultIndex[100];
	int rstCount = 0;


	for (unsigned long i = 0; i < totalOptions; i++) {
		int tempsum = 0;
		unsigned long tempbase = (unsigned long)finalIndex[i] * (unsigned long)tokensIndex;
		for (int j = 0; j < tokensIndex; j++) {
			if (lutFit[tempbase + j] == 1) {
				tempsum++;
			}
		}

		if (tempsum >= nptsSharedPaths) {
			resultIndex[rstCount++] = finalIndex[i];
			for (int j = 0; j < tokensIndex; j++) {
				if (lutFit[tempbase + j] == 1) {
					for (unsigned long k = j; k < limit; k += tokensIndex) {
						lutFit[k] = 0;
					}
				}
			}
		}
	}


	//float *disData = (float *)distanceMask->imageData;
	for (int i = 0; i < rstCount; i++) {
		int tempso = resultIndex[i] % sourceCount;
		int tempsi = resultIndex[i] / sourceCount;
		fprintf(file1, "%d\t%d\t%d\t%d\n",
			selectedWTree[idxSources[tempso]].Y / 2, selectedWTree[idxSources[tempso]].X / 2,
			selectedWTree[idxSinks[tempsi]].Y / 2, selectedWTree[idxSinks[tempsi]].X / 2);
	}



	free(lutFit);
	free(sumCount);
	free(finalIndex);
	free(xMax);
	free(yMax);
	free(soMax);
	free(siMax);
	cvReleaseImage(&duoImage);

}

void polyfit(uint8_t *lutFit, int * sumCount, int *iSource, int *iSink,
	 int numSources, int numSinks) {

	float clusteringThreshold = 1.2;

	for (int i = 0; i < numSources; i++) {
		for (int j = 0; j < numSinks; j++) {
			//unsigned int select[1000];
			unsigned int so = iSource[i];
			unsigned int si = iSink[j];
			unsigned long base = i + j*numSources;

			double px = (selectedWTree[si].X - selectedWTree[so].X) /
				(selectedWTree[si].rho - selectedWTree[so].rho);
			double bx = selectedWTree[so].X - px*selectedWTree[so].rho;

			double py = (selectedWTree[si].Y - selectedWTree[so].Y) /
				(selectedWTree[si].rho - selectedWTree[so].rho);
			double by = selectedWTree[so].Y - py*selectedWTree[so].rho;

			sumCount[base] = 0;

			for (int k = 0; k < tokensIndex; k++) {
				double errorx = px*selectedWTree[k].rho + bx - selectedWTree[k].X;
				double errory = py*selectedWTree[k].rho + by - selectedWTree[k].Y;
				double err = errorx*errorx + errory*errory;
				if (err < clusteringThreshold && selectedWTree[k].rho < selectedWTree[si].rho
					&& selectedWTree[k].rho>selectedWTree[so].rho) {
					unsigned long temp = (unsigned long)base*(unsigned long)tokensIndex + k;
					lutFit[temp] = 1;
					sumCount[base]++;
				}
			}

		}
	}

}

void QuickSumSort(unsigned long left, unsigned long right, int *sumCount, unsigned long * finalIndex) {

	unsigned long i = left, j = right;
	int key = sumCount[left];
	unsigned long tempidx = finalIndex[left];

	if (left >= right) {
		return;
	}

	while (i < j) {

		while (i<j && sumCount[j] < key)
			j--;
		finalIndex[i] = finalIndex[j];
		sumCount[i++] = sumCount[j];

		while (i<j && sumCount[i]  > key)
			i++;
		if (i < j) {
			finalIndex[j] = finalIndex[i];
			sumCount[j--] = sumCount[i];
		}
	}
	finalIndex[i] = tempidx;
	sumCount[i] = key;
	QuickSumSort(left, i - 1, sumCount, finalIndex);
	QuickSumSort(i + 1, right, sumCount, finalIndex);
}
